package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;
import com.jal.auto.BCInvoiceData;
import com.jal.auto.BCMyProfile;


public class InvoiceData extends TestBase {

	@Test(priority = 1)
	public void InvoiceData_ContractedAgent() throws InterruptedException {
		try{	
		    BCInvoiceData InvoiceData = new BCInvoiceData();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			InvoiceData.NavigatetoInvoiceData(logger);
			InvoiceData.ClickContractedAgentPDF(logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}
	}

@Test(priority = 2)
public void InvoiceData_NonContractedAgent() throws InterruptedException {
	try{	
	    BCInvoiceData InvoiceData = new BCInvoiceData();
	    BCMyProfile Profile = new BCMyProfile();
		Profile.login(data,logger);
		Thread.sleep(2000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
		Thread.sleep(2000);
		InvoiceData.NavigatetoInvoiceData(logger);
		InvoiceData.ClickNonContractedAgentPDF(logger);
	}catch(Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
}
}

@Test(priority = 3)
public void InvoiceData_JPSAgent() throws InterruptedException {
	try{	
	    BCInvoiceData InvoiceData = new BCInvoiceData();
	    BCMyProfile Profile = new BCMyProfile();
		Profile.login(data,logger);
		Thread.sleep(2000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
		Thread.sleep(2000);
		InvoiceData.NavigatetoInvoiceData(logger);
		InvoiceData.ClickJPSAgentPDF(logger);
	}catch(Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
}}
}