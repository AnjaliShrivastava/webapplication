package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;
import com.jal.auto.BCFlightStatusandSchedule;
import com.jal.auto.BCMyProfile;

public class FlightStatusandSchedule extends TestBase {

	@Test(priority = 1)
	public void FligtStatus_TodayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightStatusToday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 3)
	public void FligtStatus_YesterdayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightStatusYesterday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 5)
	public void FligtStatus_TomorrowDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightStatusTomorrow(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 7)
	public void FligtStatus_StatusNegativeScenario() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightStatusNegativescenario(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 2)
	public void FligtSchedule_TodayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightScheduleToday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 4)
	public void FligtSchedule_YesterdayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightScheduleYesterday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 6)
	public void FligtSchedule_TomorrowDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.NavigatetoFlightstatusandSchedule(logger);
			FlightStatusandSchedule.FlightScheduleTomorrow(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	} 

	// ********************** Dashboard *************//

	@Test(priority = 1)
	public void DashboardFligtStatus_TodayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightStatusToday(data, logger);
			

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 3)
	public void DashboardFligtStatus_YesterdayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightStatusYesterday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 5)
	public void DashboardFligtStatus_TomorrowDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightStatusTomorrow(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 7)
	public void DashboardFligtStatus_StatusNegativeScenario() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightStatusNegativescenario(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority = 2)
	public void DashboardFligtSchedule_TodayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightScheduleToday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 4)
	public void DashboardFligtSchedule_YesterdayDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightScheduleYesterday(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 6)
	public void DashboardFligtSchedule_TomorrowDepartureDate() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardFlightScheduleTomorrow(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 8)
	public void DashboardFligtStatus_ScheduleNegativeScenario() throws InterruptedException {
		try {
			BCFlightStatusandSchedule FlightStatusandSchedule = new BCFlightStatusandSchedule();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data, logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			FlightStatusandSchedule.DashboardScheduleNegativeScenario(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
}
