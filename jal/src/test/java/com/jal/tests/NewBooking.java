package com.jal.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.jal.auto.BCLogin;
import com.jal.auto.BCMyProfile;
import com.jal.auto.BCNewBooking;
import com.jal.auto.BCShipmentList;

public class NewBooking extends TestBase {
	
	
	@BeforeClass
	public void beforeClass() {
		/*System.out.println(this.getClass());
		System.out.println(this.getClass().);
		Method[] method = this.getClass().getMethods();		
		int TM = method.length ;
		*/
	 logger = extent.startTest("JAL_Normal Booking");
	}
	

	@Test()
	
	public void performancetest() throws InterruptedException {

	
		BCShipmentList shipment = new BCShipmentList();
		BCNewBooking booking = new BCNewBooking();
		BCMyProfile Profile = new BCMyProfile();
		
		Profile.login(data,logger);	
		shipment.NavigatetoShipmentList(logger);
		booking.newBookingNavigation(logger);
		
		booking.EnterFlightDetails(data,logger);
		booking.EnterShipmentDetailsforLooseBulk(data,logger);
			
		booking.SearchFlight(data,logger);
		booking.SelectFlight(data,logger);
		
		booking.enterNewShipperDetails(data,logger);
		booking.enterNewConsigneeDetails(data,logger);
		booking.enterShipmentDetails(data,logger);
		booking.continueBooking(data, logger);
		booking.paymentINFO(data,logger);
		booking.BookingDoneVerification(logger);
		booking.getBookingDetails(logger);		

	} 

	//@Test
	public void CreateNewBooking_byAgentforULDUsingExistingShipperAndConsinee() throws InterruptedException {

		BCMyProfile Profile = new BCMyProfile();			
		BCNewBooking booking = new BCNewBooking();
		BCShipmentList shipment = new BCShipmentList();
		Profile.login(data,logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		shipment.NavigatetoShipmentList(logger);
		booking.newBookingNavigation(logger);
		booking.EnterFlightDetails(data,logger);
		booking.EnterShipmentDetailsforULDBUP(data,logger);
		booking.SearchFlight(data,logger);
		booking.SelectFlight(data,logger);
		booking.existingShipperDetails(data,logger);
		booking.existingConsigneeDetails(data,logger);
		booking.enterShipmentDetails(data,logger);
		booking.continueBooking(data, logger);
		booking.paymentINFO(data,logger);
		booking.BookingDoneVerification(logger);
		booking.getBookingDetails(logger);		

	}

	//@Test
	public void CancelBooking_NewBooking() throws InterruptedException {

		BCMyProfile Profile = new BCMyProfile();			
		BCNewBooking booking = new BCNewBooking();
		BCShipmentList shipment = new BCShipmentList();
		Profile.login(data,logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		shipment.NavigatetoShipmentList(logger);
		booking.newBookingNavigation(logger);
		booking.EnterFlightDetails(data,logger);
		booking.EnterShipmentDetailsforULDBUP(data,logger);
		booking.SearchFlight(data,logger);
		booking.SelectFlight(data,logger);
		booking.existingShipperDetails(data,logger);
		booking.existingConsigneeDetails(data,logger);
		booking.enterShipmentDetails(data,logger);
		booking.continueBooking(data, logger);
		booking.paymentINFO(data,logger);
		booking.BookingDoneVerification(logger);
		booking.getBookingDetails(logger);
		booking.cancelCreatedBooking(data, logger);
		booking.cancelBookingDetails(logger);

	}


	//@Test
	public void CancelBooking_ExistingBooking() throws InterruptedException {


		BCMyProfile Profile = new BCMyProfile();			
		BCNewBooking booking = new BCNewBooking();
		BCShipmentList shipment = new BCShipmentList();
		Profile.login(data,logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		shipment.NavigatetoShipmentList(logger);
		booking.searchAWB(data, logger);
		shipment.EyeIcon(logger);
		booking.cancelCreatedBooking(data, logger);
		booking.cancelBookingDetails(logger);



	}
}