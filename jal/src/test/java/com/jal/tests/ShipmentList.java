package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

//import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import com.jal.auto.BCLogin;
import com.jal.auto.BCShipmentList;

public class ShipmentList extends TestBase {
	
	
	
	
		
	

	//@Test(invocationCount = 2, threadPoolSize = 2)
	@Test
	
	public void Shipment_BasicSearch() throws InterruptedException, IOException{
				
		BCShipmentList shipmentlist = new BCShipmentList();
		BCLogin login = new BCLogin();
		/* long startTime4 = System.currentTimeMillis();*/
		login.login(logger);
		/*long endTime4 = System.currentTimeMillis();
        long totalTime4 = endTime4 - startTime4;
        System.out.println("Total Page Load Time: " + totalTime4 + "milliseconds");	*/
		shipmentlist.NavigatetoShipmentList(logger );
        shipmentlist.Search(data , logger );
		
	
	}
		
		
	public void DataSorting_ByDropdownValues() throws InterruptedException {
		
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);			
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.calanderSearch(data, logger);
			shipmentlist.sortByAWB(data ,logger );			
			
			
			shipmentlist.sortByBUP(data , logger);
			
		
			shipmentlist.sortByFlight(data, logger);
			
			shipmentlist.sortByDepart(data , logger);
			
			shipmentlist.sortByAllocCode(data, logger);
			
			shipmentlist.sortByAllocReleaseTime(data , logger);
			
			shipmentlist.sortByBookingCloseTime(data , logger);
		
		}
		
		
	public void DataSorting_AsscendingOrder() throws InterruptedException {
	
		BCShipmentList shipmentlist = new BCShipmentList();
		BCLogin login = new BCLogin();
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);			
		shipmentlist.NavigatetoShipmentList(logger );
		shipmentlist.calanderSearch(data, logger);
		shipmentlist.sortByAWBAss(data ,logger );			
	
	
	}
	
	
		
		public void Shipment_AdvanceSearcforBookedShipment() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);			
			shipmentlist.NavigatetoShipmentList(logger );
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			shipmentlist.advanceSearch(data , logger);
			
			
		}
		
		public void Shipment_negativeScenario() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);			
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.InvalidSearch(data , logger );
		}
		
		//@Test
		public void Shipment_7daysfromToday() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );			
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.calanderSearch(data, logger);
		}
		public void Shipment_closeSearchBy() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.SearchClose(data , logger );
		}
		
		public void Shipment_SortBasedOnOriginDes() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.sortByorigin(data, logger);
			shipmentlist.sortByDestination(data, logger);
		}
		
		public void ShipmentList_NewBookingNavigation() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.EyeIcon(logger);
			shipmentlist.ShipmentListTab(logger);
			Thread.sleep(2000);
			shipmentlist.EditIcon(logger);
			
		}
		
		public void ShipmentList_TrackAWBNavigation() throws InterruptedException{
			BCShipmentList shipmentlist = new BCShipmentList();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			shipmentlist.NavigatetoShipmentList(logger );
			shipmentlist.TrackAndTrace(logger);
			
		}
		
		
		}	
