package com.jal.tests;
import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;
import com.jal.auto.BCAllotmentUtilization;
import com.jal.auto.BCMyProfile;


public class AllotmentUtilization extends TestBase {

 @Test(priority = 1)
	public void AllotmentUtilization_DifferentDateSearch() throws InterruptedException {
		try{	
			BCAllotmentUtilization AllotmentUtilization = new BCAllotmentUtilization();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			AllotmentUtilization.NavigatetoAllotmentUtilization(logger);
			AllotmentUtilization.DifferentDatesearch(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}
	} 
	@Test(priority = 2)
	public void AllotmentUtilization_CurrentDateSearch() throws InterruptedException {
		try{	
			BCAllotmentUtilization AllotmentUtilization = new BCAllotmentUtilization();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			AllotmentUtilization.NavigatetoAllotmentUtilization(logger);
			AllotmentUtilization.CurrentDatesearch(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}
	} 
	@Test(priority = 3)
	public void AllotmentUtilization_CurrentDateNegativeSearch() throws InterruptedException {
		try{	
			BCAllotmentUtilization AllotmentUtilization = new BCAllotmentUtilization();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			AllotmentUtilization.NavigatetoAllotmentUtilization(logger);
			AllotmentUtilization.NegativeDatesearch(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}
	}
}

