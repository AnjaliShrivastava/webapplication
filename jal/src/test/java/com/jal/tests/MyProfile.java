package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.jal.pages.usersandrolesObjects;
import com.relevantcodes.extentreports.LogStatus;
import java.util.Random;
import org.openqa.selenium.WebDriver;
import com.jal.auto.BCLogin;
import com.jal.auto.BCMyProfile;
import com.jal.auto.BCUsersAndRoles;

public class MyProfile extends TestBase {

	WebDriver driver;
	Random r = new Random();
    int rNum = r.nextInt(999);
	//@Test(priority = 0)
	public void FirstTimelogin_SetPasswordConsignee() throws InterruptedException {
        
		try {
			BCMyProfile Profile = new BCMyProfile();
			BCLogin login = new BCLogin();
			BCUsersAndRoles user = new BCUsersAndRoles();			
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			user.navigateToUsersAndRoles(logger);
			user.CreateConsigneeUser(data,rNum,logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			String [] text = Details.trim().split(" ");
			login.logout(logger);
			Profile.Relogin(data, text[text.length-1] , rNum,logger);
			Profile.FirstTimelogin_SetPassword1(data,rNum, logger);
		} catch (Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());

		}

	}
	
	@Test
	public void FirstTimelogin_SetPasswordAdmin() throws InterruptedException {
        /*Random r = new Random();
        int rNum = r.nextInt(100);*/
		try {
			BCMyProfile Profile = new BCMyProfile();
			BCLogin login = new BCLogin();
			BCUsersAndRoles user = new BCUsersAndRoles();			
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			user.navigateToUsersAndRoles(logger);
			user.CreateAdminUser(data ,rNum, logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			String [] text = Details.trim().split(" ");
			login.logout(logger);
			Profile.Relogin(data, text[text.length-1] , rNum,logger);
			Profile.FirstTimelogin_SetPassword1(data,rNum, logger);
		} catch (Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());

		}

	}
	
	
	//@Test
	public void FirstTimelogin_SetPasswordStaff() throws InterruptedException {
       /* Random r = new Random();
        int rNum = r.nextInt(100)*/;
		try {
			BCMyProfile Profile = new BCMyProfile();
			BCLogin login = new BCLogin();
			BCUsersAndRoles user = new BCUsersAndRoles();			
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			user.navigateToUsersAndRoles(logger);
			user.CreateStaffUser(data ,rNum, logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			String [] text = Details.trim().split(" ");
			login.logout(logger);
			Profile.Relogin(data, text[text.length-1] , rNum,logger);
			Profile.FirstTimelogin_SetPassword1(data,rNum, logger);
		} catch (Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());

		}

	}

	

	// ************** AGENT PROFILE ******************* //

		////@Test(priority = 1)
		public void AgentWithoutPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				// Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AgentWithoutPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 1)
		public void AgentChangePassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				// Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AgentChangePassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		////@Test(priority = 1)
		public void AgentIncorrectPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AgentIncorrectPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		////@Test(priority = 1)
		public void AgentSecondTimePasswordChange() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AgentSecondTimePasswordChange(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		// ************** ADMIN PROFILE ******************* //



		//@Test(priority = 2)
		public void AdminChangePassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AdminChangePassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 2)
		public void AdminIncorrectPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AdminIncorrectPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		//@Test(priority = 2)
		public void AdminSecondTimePasswordChange() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.AdminSecondTimePasswordChange(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		// ************** CONSIGNEE PROFILE ******************* //

		//@Test(priority = 3)
		public void ConsigneeWithoutPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.ConsigneeWithoutPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 3)
		public void ConsigneeChangePassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.ConsigneeChangePassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 3)
		public void ConsigneeIncorrectPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.ConsigneeIncorrectPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		//@Test(priority = 3)
		public void ConsigneeSecondTimePasswordChange() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.ConsigneeSecondTimePasswordChange(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		// ************** STAFF PROFILE ******************* //

		//@Test(priority = 4)
		public void StaffWithoutPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.StaffWithoutPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 4)
		public void StaffChangePassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.StaffChangePassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}

		}

		//@Test(priority = 4)
		public void StaffIncorrectPassword() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				Profile.navigateToMyProfile(data, logger);
				Profile.StaffIncorrectPassword(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}

		//@Test(priority = 4)
		public void StaffSecondTimePasswordChange() throws InterruptedException {

			try {
				BCMyProfile Profile = new BCMyProfile();
				Profile.login(data, logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
				Thread.sleep(5000);
				//Profile.FirstTimelogin_SetPassword(data, logger);
				Profile.navigateToMyProfile(data, logger);
				Profile.StaffSecondTimePasswordChange(data, logger);

			} catch (Exception e) {

				logger.log(LogStatus.FAIL, e.getMessage());

			}
		}
	

	
	//@Test
	public void ForgotPassword_SubmitDetails() throws InterruptedException {
		try {
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);		
			Profile.ForgotPassewordNavigation(logger);
			Profile.ForgotPassewordDetails(data, logger);
			Profile.SaveForgotPassewordDetails(logger);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());}
	}
	
	//@Test
	public void ForgotPassword_ResetDetails() throws InterruptedException {
		try {
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);		
			Profile.ForgotPassewordNavigation(logger);
			Profile.ForgotPassewordDetails(data, logger);
			Profile.ResetForgotPassewordDetails(logger);
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());}
	}
	
	
	
	
	
}
