package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;



import org.openqa.selenium.WebDriver;

import com.jal.auto.BCLogin;
import com.jal.auto.BCTrackAWBandMail;

public class TrackAWBandMail extends TestBase {

	WebDriver driver;

	@Test
	public void TrackAWB_BasicSearch() throws InterruptedException {
		try {
			BCTrackAWBandMail TrackAWBandMail = new BCTrackAWBandMail();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(5000);
			TrackAWBandMail.NavigatetoTrackAWBandMail(logger);
			TrackAWBandMail.TrackAWBTab(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test
	public void TrackAWB_AdvancedSearch() throws InterruptedException {
		try {
			BCTrackAWBandMail TrackAWBandMail = new BCTrackAWBandMail();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(5000);
			TrackAWBandMail.NavigatetoTrackAWBandMail(logger);
			TrackAWBandMail.TrackAWBAdvancesearch(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test
	public void TrackMail_BasicSearch() throws InterruptedException {
		try {
			BCTrackAWBandMail TrackAWBandMail = new BCTrackAWBandMail();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(5000);
			TrackAWBandMail.NavigatetoTrackAWBandMail(logger);
			//TrackAWBandMail.Search(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	@Test
	public void TrackMail_AdvancedSearch() throws InterruptedException {
		try {
			BCTrackAWBandMail TrackAWBandMail = new BCTrackAWBandMail();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(5000);
			TrackAWBandMail.NavigatetoTrackAWBandMail(logger);
			//TrackAWBandMail.Search(data, logger);

		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
}
