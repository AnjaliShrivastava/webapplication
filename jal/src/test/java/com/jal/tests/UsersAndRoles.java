package com.jal.tests;

import java.util.Random;

import org.testng.annotations.Test;
import com.jal.auto.BCLogin;
import com.jal.auto.BCUsersAndRoles;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;

public class UsersAndRoles extends TestBase {


	Random random = new Random();
	int num = random.nextInt(400);

	public  final int rNum = num ;


	@Test (priority=0)
	public void CreateRole_NewRole(){
		try {

			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToCreateRoles(logger);
			role.CreateRolewithDefaultPermission(data , rNum, logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test 
	public void CreateRole_NewRoleWithSpecificPermission(){
		try {
			Random random = new Random();
			int num = random.nextInt(400);
			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToCreateRoles(logger);
			role.CreateRolewithspecifPermission(data , num, logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}


	@Test
	public void CreateRole_ResetValues() throws InterruptedException{
		try {
			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToCreateRoles(logger);
			role.ResetValues(data, logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test
	public void CreateRole_cancelRole(){
		try {
			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToCreateRoles(logger);
			role.CancelRole(data, logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test(priority=3)
	public void ExistingRole_EditRole(){
		try {

			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);
			role.searchRolewithExistingRole(data , rNum , logger);
			role.editRoleSection(logger);
			role.editRole(data ,rNum , logger);
			role.SaveRole(data ,rNum, logger);			

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

	}


	@Test(priority=1)
	public void ExistingRole_ResetEditRole(){
		try {

			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);
			role.searchRolewithExistingRole(data , rNum, logger);
			role.editRoleSection(logger);
			role.editRole(data ,rNum, logger);
			role.ResetRole(data ,rNum , logger);			

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	@Test(priority=2)

	public void ExistingRole_CancelResetRole(){
		try {
			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);
			role.searchRolewithExistingRole(data , rNum, logger);
			role.editRoleSection(logger);
			role.editRole(data ,rNum, logger);
			role.CancelRole( logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}



	@Test(priority=4)
	public void ExistingRole_DeleteRole(){
		try {

			BCUsersAndRoles role = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);
			role.searchRole(data ,rNum, logger);
			role.DeleteRole(logger);


		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test
	public void Role_NegativeValidation(){
		try {

			BCUsersAndRoles role = new BCUsersAndRoles();	
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			role.navigateToUsersAndRoles(logger);
			role.navigateToCreateRoles(logger);
			role.CreateRoleValidation(data, logger);
			pageRefresh(GeneralObjects.MenuUsersAndRoles);			
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);
			role.searchRole(data, logger);
			role.DeleteRoleValidation(logger);
			pageRefresh(GeneralObjects.MenuUsersAndRoles);
			role.navigateToUsersAndRoles(logger);
			role.navigateToExistingRoles(logger);			
			role.searchRole(data, logger);
			role.editRoleSection(logger);
			role.EditRoleValidation(data , logger);


		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	} 


	@Test 
	public void CreateUser_NewAgentUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToUsersAndRoles(logger);
			user.CreateAgentUser(data,rNum ,logger);
			//user.searchCreatedUsers(data,rNum,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}



	@Test 
	public void CreateUser_NewConsigneeUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToUsersAndRoles(logger);
			user.CreateConsigneeUser(data,rNum,logger);
			//user.searchCreatedUsers(data,rNum,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	@Test 
	public void CreateUser_AdminUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();		
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			user.navigateToUsersAndRoles(logger);
			user.CreateAdminUser(data, rNum , logger);
			//user.searchCreatedUsers(data,rNum,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	@Test 
	public void CreateUser_StaffUser(){
		try {
			Random random = new Random();
			int num = random.nextInt(400);
			BCUsersAndRoles user = new BCUsersAndRoles();		
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToUsersAndRoles(logger);
			user.CreateStaffUser(data,num ,logger);
			//user.searchCreatedUsers(data,rNum,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	} 



	@Test 
	public void ExistingUsers_UserSortingAsscending() {
		try {
			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();	
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.ValidateSortUsersbyPortalID(data,logger);
			user.ValidateSortUsersbyUsername(data,logger);
			user.ValidateSortUsersbyFirstname(data,logger);
			user.ValidateSortUsersbyLastname(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	@Test 
	public void ExistingUsers_UserSortingDescending() {
		try {
			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();	
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			user.navigateToExistingUsers(logger);
			user.ValidateSortUsersDesc(data, logger);
			
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test 
	public void ExistingUser_SearchActiveUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersActive(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}
	@Test 
	public void ExistingUser_SearchInActiveUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersInactive(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test 
	public void ExistingUser_SearchLockedUser(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersLocked(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	@Test 
	public void ExistingUser_SearchByName(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersName(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}


	@Test
	public void ExistingUsers_EditUsers(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersActive(data,logger);
			user.editUser(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	} 

	@Test
	public void ExistingUsers_DeleteUsers(){
		try {

			BCUsersAndRoles user = new BCUsersAndRoles();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			user.navigateToExistingUsers(logger);
			user.searchUsersActive(data,logger);
			user.deleteUserData(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}





}
