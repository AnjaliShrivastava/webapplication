package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import com.jal.auto.BCLogin;
import com.jal.auto.BCManageNews;

public class ManageNews extends TestBase {
	
	WebDriver driver2;
	

	
	@Test
	public void CreateNews_NewNews(){
		try {
			
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		news.CreateNews(data , logger);
		
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	
	@Test
	public void CreateNews_ResetValues() throws InterruptedException{
		try {
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		news.Resetvalues(data, logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
		
	}
	
	@Test
	public void CreateNews_cancelNews(){
		try {
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
	
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);
		news.CloseCreateNewspage(data , logger);
	}catch(Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
		
	}
	
	@Test
	public void ExistingNews_EditNews(){
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		try {
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			news.EditNews(data , logger);
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}
	
	
	@Test
	public void ExistingNews_DeleteNews(){
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		try {
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			news.DeleteNews(logger);
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		} }	
	
	
	@Test
	public void ExistingNews_SearchByInputActive(){
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		try {
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			news.SearchNewsbyinputField(data, logger);
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}	
	@Test
	public void ExistingNews_SearchByInputInActive(){
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();
		try {
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			news.SearchNewsbyinputFieldInactive(data, logger);
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}	
	
	
	@Test
	public void ExistingNews_SortNewsbyArea() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
		try {
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);
			news.ValidateSortNewsbyArea(logger);
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
		
	}
	
	
	@Test
	public void ExistingNews_SortNewsbyStation() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
			try {
				login.login(logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
				Thread.sleep(5000);
				news.ValidateSortNewsbystation(logger);
			} catch (InterruptedException e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			} }

	
	@Test
	public void ExistingNews_SortNewsbyNewsID() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
			try {
				login.login(logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
				Thread.sleep(5000);
				news.ValidateSortNewsbyNewsID(logger);
			} catch (InterruptedException e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			} }
	@Test
	public void ExistingNews_SortNewsbyStartDate() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
			try {
				login.login(logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
				Thread.sleep(5000);
				news.ValidateSortNewsbystartDate(logger);
			} catch (InterruptedException e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			} }
	@Test
	public void ExistingNews_SortNewsbyEndDate() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
			try {
				login.login(logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
				Thread.sleep(5000);
				news.ValidateSortNewsbyEndDate(logger);
			} catch (InterruptedException e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			} }
	
	@Test
	public void ExistingNews_SortbyAssecending() {
		
		BCManageNews news = new BCManageNews();
		BCLogin login = new BCLogin();		
			try {
				login.login(logger);
				Thread.sleep(5000);
				elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
				Thread.sleep(5000);
				news.ValidateSortnewsASSC(logger);
			} catch (InterruptedException e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			} }
	
	
	

}
