package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;



import com.jal.auto.BCLogin;
import com.jal.auto.BCPortalSetting;

public class PortalSetting extends TestBase {
	

	@Test(priority = 3)
	public void PortalSettings_EditDetails() throws InterruptedException{
		try {
			
			BCPortalSetting settings = new BCPortalSetting();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			settings.navigateToPortalSettings(logger);
			settings.EditPortalSettings(logger);
			settings.enterDetails(data, logger);
			//settings.enterSHCDetails(data, logger);
			settings.SaveData(logger);
			settings.VerifyTextValues(data, logger);
			}catch(Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}
	
	@Test(priority = 2)
	public void PortalSettings_CancelEditDetails() throws InterruptedException{
		try {
			
			BCPortalSetting settings = new BCPortalSetting();
			BCLogin login = new BCLogin();
			login.login(logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
			Thread.sleep(5000);	
			settings.navigateToPortalSettings(logger);
			settings.EditPortalSettings(logger);
			settings.CancelData(logger);
			
			}catch(Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}
	

	@Test(priority = 1)
public void PortalSettings_ResetEditDetails() throws InterruptedException{
	try {
		
		BCPortalSetting settings = new BCPortalSetting();
		BCLogin login = new BCLogin();
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);	
		settings.navigateToPortalSettings(logger);
		settings.EditPortalSettings(logger);
		settings.enterDetails(data, logger);
		//settings.enterSHCDetails(data, logger);
		settings.resetData(logger);
		
		// Yet to implement ////
		
		//settings.VerifyresettedTextValues(data, logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

@Test(priority = 0)
public void PortalSettings_NegativeScenarios() throws InterruptedException{
	try {
		
		BCPortalSetting settings = new BCPortalSetting();
		BCLogin login = new BCLogin();
		login.login(logger);
		Thread.sleep(5000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1 ,GeneralObjects.NoButton , logger );
		Thread.sleep(5000);	
		settings.navigateToPortalSettings(logger);
		settings.EditPortalSettings(logger);
		settings.enterDetails(data, logger);
		settings.SaveNegativeData(logger);
		//settings.VerifyTextValues(data, logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}



}


