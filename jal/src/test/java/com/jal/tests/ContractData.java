package com.jal.tests;

import org.testng.annotations.Test;

import com.jal.auto.BCContractData;
import com.jal.auto.BCMyProfile;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;

public class ContractData extends TestBase {
	
	@Test(priority = 1)
	public void ContractData_NoneditableShortName() throws InterruptedException {
		try{	
			BCContractData ContractData = new BCContractData();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			ContractData.NavigatetoContractData(logger);
			ContractData.DifferentdateSearchNonEditableShotName(data,logger);
			ContractData.DifferentdateAdvanceSearch(data,logger);
			ContractData.DifferentdateSortByOrgin(data,logger);
			ContractData.DifferentdateSortByDestination(data,logger);
			ContractData.DifferentdateSortBy1stFlight(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}

}
	/*@Test(priority = 3)
public void ContractData_EditableShortName() throws InterruptedException {
	try{	
		BCContractData ContractData = new BCContractData();
	    BCMyProfile Profile = new BCMyProfile();
		Profile.login(data,logger);
		Thread.sleep(2000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
		Thread.sleep(2000);
		ContractData.NavigatetoContractData(logger);
		ContractData.DifferentdateSearchEditableShortName(data,logger);
		ContractData.DifferentdateAdvanceSearch(data,logger);
		ContractData.DifferentdateSortByOrgin(data,logger);
		ContractData.DifferentdateSortByDestination(data,logger);
		ContractData.DifferentdateSortBy1stFlight(data,logger);
	}catch(Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
}
	}
	@Test(priority = 4)
	public void ContractData_DropdownSelectShortName() throws InterruptedException {
		try{	
			BCContractData ContractData = new BCContractData();
		    BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(2000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(2000);
			ContractData.NavigatetoContractData(logger);
			ContractData.DifferentdateSearchDropdownShortName(data,logger);
			ContractData.DifferentdateAdvanceSearch(data,logger);
			ContractData.DifferentdateSortByOrgin(data,logger);
			ContractData.DifferentdateSortByDestination(data,logger);
			ContractData.DifferentdateSortBy1stFlight(data,logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
	}

} */
	@Test(priority = 2)
public void ContractData_NegativeScenario() throws InterruptedException {
	try{	
		BCContractData ContractData = new BCContractData();
	    BCMyProfile Profile = new BCMyProfile();
		Profile.login(data,logger);
		Thread.sleep(2000);
		elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
		Thread.sleep(2000);
		ContractData.NavigatetoContractData(logger);
		ContractData.NegativeDatesearch(data,logger);
		
	}catch(Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
}
}
}