package com.jal.tests;

import java.util.List;

import org.testng.TestNG;
import org.testng.collections.Lists;

import com.jal.auto.MyTransformer;

public class APP  {

	public static void main(String[] args) {
		
	TestNG testng  = new TestNG();
		List<String> suites = Lists.newArrayList();
	suites.add("./testng.xml");
      testng.setTestSuites(suites);
      testng.setAnnotationTransformer(new MyTransformer());
        testng.run();
		
	}

}
