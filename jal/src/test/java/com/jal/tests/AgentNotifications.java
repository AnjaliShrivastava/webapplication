package com.jal.tests;

import org.testng.annotations.Test;
import com.jal.auto.TestBase;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.LogStatus;


import org.openqa.selenium.WebDriver;

import com.jal.auto.BCAgentNotifications;
import com.jal.auto.BCMyProfile;

public class AgentNotifications extends TestBase {

	WebDriver driver;

	@Test(priority = 1)
	public void AgentNotification_Search() throws InterruptedException {
		try {
			BCAgentNotifications AgentNotifications = new BCAgentNotifications();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);

			Thread.sleep(3000);
			AgentNotifications.navigateToMyProfile(data,logger);
			AgentNotifications.NotificationsTab(logger);
			AgentNotifications.SearchClose(data, logger);

		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 2)
	public void AgentNotification_InvalidSearch() throws InterruptedException {
		try {
			BCAgentNotifications AgentNotifications = new BCAgentNotifications();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(3000);
			AgentNotifications.navigateToMyProfile(data,logger);
			AgentNotifications.NotificationsTab(logger);
			AgentNotifications.InvalidSearch(data, logger);

		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 3)
	public void DataSorting_ByDropdownValues() throws InterruptedException {
		try {
			BCAgentNotifications AgentNotifications = new BCAgentNotifications();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(3000);
			AgentNotifications.navigateToMyProfile(data,logger);
			AgentNotifications.NotificationsTab(logger);
			AgentNotifications.sortByAWB(data, logger);
			AgentNotifications.sortByShortName(data, logger);
			AgentNotifications.sortByDateTime(data, logger);

		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	@Test(priority = 4)
	public void DataSorting_AscendingByDropdownValues() throws InterruptedException {
		try {
			BCAgentNotifications AgentNotifications = new BCAgentNotifications();
			BCMyProfile Profile = new BCMyProfile();
			Profile.login(data,logger);
			Thread.sleep(5000);
			elementAvailbility(GeneralObjects.PwdExpiryMSG1, GeneralObjects.NoButton, logger);
			Thread.sleep(3000);
			AgentNotifications.NavigateNotificationUsingBell(logger);
			AgentNotifications.AscendingsortByAWB(data, logger);
			AgentNotifications.AscendingsortByShortName(data, logger);
			AgentNotifications.AscendingsortByDateTime(data, logger);

		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
}

