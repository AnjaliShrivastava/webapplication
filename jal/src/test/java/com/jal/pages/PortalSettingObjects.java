package com.jal.pages;

import com.jal.auto.TestBase;

public class PortalSettingObjects extends TestBase {

    public static final String headertitle = "//h1[contains(text(),'"+TestBase.Gettextvalue("portalsetting")+"')]";
    public static final String editIcon = "//img[@class='editIcon']";
    public static final String BookingWindow = "//input[@name='bookingWindowConfig']";
    public static final String SmallShipment = "//input[@name='minShipmentWeightConfig']";
    public static final String RetentionDays = "//input[@name='rententionDaysConfig']";
    public static final String LD2= "//input[@name='LDB2Config']";
    public static final String LD3= "//input[@name='LDB3Config']";
    public static final String Pallet = "//input[@name='palletConfig']";
    public static final String Login = "//input[@name='maximumAttemptsConfig']";
    
    public static final String ExpireIn = "//input[@name='expireInConfig']";
    public static final String MinimumLength ="//input[@name='minimumLenghtConfig']";
    public static final String ExpiryAlert ="//input[@name='expiryAlertsConfig']";
    public static final String AircraftSHCTitle ="//div[contains(text(),'"+TestBase.Gettextvalue("aircraftshc")+"')]";
    public static final String DeliveryATTitle ="(//div[contains(text(),'"+TestBase.Gettextvalue("deliveryat")+"')])[1]";
    public static final String DeliverySHCTextField ="//input[@name='newDeliveryCodemodal']";
    public static final String DeliveryAtTextField ="//input[@name='newDeliveryNamemodal']";
    public static final String AircraftTextField ="//input[@name='newAircraftTypemodal']";
    public static final String AircraftSHCTextField ="//input[@name='newAircraftShcCodemodal']";
    public static final String AirCraftPlusButton ="(//img[@src='/jalportal/0.0.0-300e93a6/assets/Images/plus_icon2.svg'])[1]";
    public static final String DeliveryAtPlusButton ="//div[@class='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-md-6 col-12 portalsettings-card-background mobileULDFormatting']//div[3]//div[1]//div[2]/div/div[2]";
    
    public static final String SuccessMessage ="//span[contains(text(),'"+TestBase.Gettextvalue("configsave")+"')]";
    public static final String cancelButton  = "//button[contains(text(),'"+TestBase.Gettextvalue("cancel")+"')]";
    public static final String resetButton="//button[contains(text(),'"+TestBase.Gettextvalue("reset")+"')]";
    public static final String errorChar   ="//div[contains(text(),'Invalid Character')]";
    
    
    ///    **************  Verified Fields ************************   ///
    
    public static final String BWValue  ="//div[@class='portal-header-content col-xxl-4 col-xl-12 col-lg-12 col-md-12 col-12']";
    public static final String weightValue  ="//div[@class='portal-header-content col-xxl-4 col-xl-12 col-lg-12 col-md-12  col-12']";
    public static final String RDValue  ="//div[@class='portal-header-content col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-12']";
    public static final String LD2Value  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-4'])[1]";
    public static final String LD3Value  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-4'])[2]";
    public static final String PalletValue  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-4'])[3]";
    public static final String MAValue  ="//div[@class='portal-header-content col-xxl-12 col-xl-12 col-lg-12 col-md-12  col-12']";
    public static final String EIValue  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4  col-4'])[1]";
    public static final String MLValue  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4  col-4'])[2]";
    public static final String EAValue  ="(//div[@class='portal-header-content col-xxl-4 col-xl-4 col-lg-4 col-md-4  col-4'])[3]";
    
    		
    
    
}
