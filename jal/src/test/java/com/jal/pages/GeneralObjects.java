package com.jal.pages;

import com.jal.auto.TestBase;

public class GeneralObjects extends TestBase {
	public static final String txtUsername = "//*[@id='form-name']";
	public static final String txtPassword = "//*[@id='form-password']";
	public static final String btnLogin = "//button[contains(text(),'"+TestBase.Gettextvalue("login")+"')]";
	public static final String btnSubmit ="(//button[@type='submit'])[1]";
	public static final String btnClose = "//button[contains(text(),'Close')]";
	public static final String portalID ="//input[@id ='login_username' and @required='required']";
	public static final String portalPassword ="//input[@id='login_password' and @required='required']";
	public static final String buttonSignin ="//button[@id='login_signIn']";
	
	public static final String PwdExpiryMSG1JA ="//div[contains(text(),'"+TestBase.Gettextvalue("passwordExpiryAlert")+"')]";
	public static final String PwdExpiryMSG1 ="//div[contains(text(),'Your password will expire in')]";
	
	public static final String PwdExpiryMSG2 ="//div[contains(text(),'"+TestBase.Gettextvalue("doyouwanttochangepassword")+"')]";
	
	//public static final String LogginButton ="//button[contains(text(),'Login')]";
	public static final String LogginButton ="//button[contains(text(),'"+TestBase.Gettextvalue("login")+"')]";
	
	//******************** MENU ********************************* //
	
	public static final String MenuUsersAndRoles="//a[@routerlink='./usersandroles']";
	public static final String ShipmentList="//a[@routerlink='./shipment-list']";
	public static final String MenuManageNews="//a[@routerlink='./news']";	
	public static final String MenuPortalSettings="//a[@routerlink='./portalsetting']";
	public static final String MenuProofList="//a[@routerlink='./prooflist']";
	public static final String MenuTrackAWBandMail="//a[@routerlink='./trackandtrace']";
	public static final String MenuInvoiceData = "//a[@routerlink='./invoice-data']";
	public static final String MenuAllotmentUtilization = "//a[@routerlink='./allotment']";
	public static final String MenuContractData = "//a[@routerlink='./contract-data']";
	public static final String Menuflightstatusandschedule	="//a[@routerlink='./statusandschedule']";
	
	
	
	
	
	
	public static final String cancelButton  ="//button[text()='CANCEL']";
	public static final String resetButton  ="//button[text()='"+TestBase.Gettextvalue("reset")+"']";
	public static final String createButton  ="//button[text()='"+TestBase.Gettextvalue("create")+"']";
	public static final String SaveButton = "//button[text()='SAVE']";
	public static final String searchbutton="//button[text()='"+TestBase.Gettextvalue("Search")+"']";
	
	public static final String Welcome = "//span[contains(text(),'Welcome')]";
	public static final String logout = "//a[contains(text(),'Logout')]";
	
	public static final String NoButtonJa ="//button[contains(text(),' "+TestBase.Gettextvalue("no")+"')]";
	public static final String NoButton ="//button[contains(text(),'No')]";
	public static final String YesButton ="//button[contains(text(),'"+TestBase.Gettextvalue("yes")+"')]";
	
	
	public static final String loginerror ="//div[@ id='infoMessage' and text()='An invalid or expired token was used to log into the application. Please try again.']";
	public static final String signinErrorPage="//span[contains(text(),'"+TestBase.Gettextvalue("login")+"')]";
	
	
	public static final String oAuthError="//h1[contains(text(),'OAuth Error')]";
	
	
	
	public static final String password ="//input[@id='passwordId']";
	public static final String passwordDev ="(//input[@id='passwordId'])[2]";
	public static final String confirmpassword ="//input[@id='confirmPasswordId']";
	public static final String confirmpasswordDev ="(//input[@id='confirmPasswordId'])[2]";
	public static final String successmessage="(//span[contains(text(),'Your password has been changed')])[2]";
	public static final String returntologin="(//span[contains(text(),'BACK TO LOGIN')])[2]";
	
	
	public static final String returntologinpage ="";
	
	public static final String firstTimeLogintitle = "//h2//span[contains(text(),'SET NEW PASSWORD')]";
	public static final String firstTimeLogintitleDev ="(//span[contains(text(),'SET NEW PASSWORD')])[2]";
	
	public static final String btnSUBMIT="//button[@name='submitButton']";
	public static final String btnSUBMITDev="(//button[@name='submitButton'])[2]";
	public static final String BurgerMenu ="//i[@class='fa fa-bars']";
	
	
	public static final String loginbutton = "(//button[@id='login_signIn'])[1]";

	public static final String calendardateview = "(//table[@class='picker__table'])/tbody";
	public static final String Calendarmonthdrpdown = "(//select[contains(@class,'picker__select--month')])";
	public static final String Calendaryeardrpdown = "(//select[contains(@class,'picker__select--year')])";

	// span[contains(text(),'Password has been reset successfully. Re-login with
	// new credentials')]

	
	
	
	
	//span[contains(text(),'Password has been reset successfully. Re-login with new credentials')]
}





