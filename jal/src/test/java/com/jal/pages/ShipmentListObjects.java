package com.jal.pages;

import com.jal.auto.TestBase;

public class ShipmentListObjects extends TestBase {
	
	public static final String headertitle = "//h1[contains(text(),'"+TestBase.Gettextvalue("mngbookingtitle")+"')]"; 
	public static final String dropdownvalue="//ul[@class='select-dropdown']/li";
	
	//*************  Search **************** //
	
	public static final String btnSearch = "//*[@id='searchIcon']";
	public static final String txtAWBprefix = "//*[@name='searchAwbPrefix']";
	public static final String txtAWBserial = "//*[@name='searchAwbSerial']";
	public static final String AWBtext="//div[@class='awb-contntt']";
	
	
	public static final String calanderIcon ="//mdb-date-picker[@name='mydate']";
	public static final String MaincalanderObject ="(//table[@class='picker__table'])[1]/tbody";
	public static final String searchTitle ="//div[@class='searchResultContainer']/span[contains(text(),'"+TestBase.Gettextvalue("yousearchedfor")+"')]";
	public static final String FlightDateTitle ="//span[contains(text()',"+TestBase.Gettextvalue("flightdate")+"')]"; 
	//public static final String FlightDateTitle ="//span[@class='searchDisplay']/span[text()=' Flight Date: ']";
	public static final String todaysList="//button[@class='btn btn-outline-danger todayslistbtn']";
	
	public static final String  RowContainer ="//div[@class='row lazyLoadingcontainer confirmed']";
	public static final String  invalidAWBMessage ="//div[contains(text(),' Invalid AWB Serial ')]";
	
	//*************  Sorting values **************** //
	
	public static final String SortByDropdown = "(//span[@class='mdl-selectfield'])[1]";
	public static final String SortedAWBText ="//div[@class='awb-contntt']";
	public static final String SortedBUPText ="//div[@class='col-xl-1 d-none d-xl-block col-xxl-3 bup-content ']";
	public static final String SortedFlightText ="//div[1]/div[2]/div[1]/div[@class='col-xl-1 col-lg-2 col-md-2 col-3 col-xxl-2 code-content ng-star-inserted']";
	public static final String SortedDepartText ="//div/div[2]/div[1]/div[@class='col-xl-2 col-lg-2 d-none col-xxl-1 d-lg-block depart_blk ng-star-inserted']";
	public static final String SortedAllocCodeText ="//div/div[2]/div[1]/div[@class='col-xl-1 col-lg-1 d-lg-block d-none potraitDisplay col-xxl-1 aloctnCode']";
	public static final String SortedBookingCloseText ="//div[@class='col-xl-2 col-lg-2 col-md-3 col-3 bcl col-xxl-1']";
	public static final String SortedBookingCloseText2 ="//div[@class='col-xl-2 col-lg-2 col-md-3 col-3 bcl col-xxl-1 ng-star-inserted']";
	public static final String SortedAllocReleaseText="//div/div[2]/div[1]/div[@class='col-xl-2 col-lg-2 col-md-3 d-none d-sm-block col-xxl-1 alcretime ng-star-inserted']";
	public static final String SortedOriginDes="//div/div[2]/div[1]/div//div[@class=' col-12 d-none d-sm-block col-xxl-12 col-xl-12 flight-content']";
	
	public static final String assecding="//img[@class='logo sort-up']";
	
	//*************  Advance Search **************** //
	
	public static final String advanchSearchIcon ="//span[@id='advSrchToggle']";
	public static final String originDropdown="//mdb-select[@name='searchOrigin']";
	public static final String destinationDropdown="//mdb-select[@name='searchDestination']";
	public static final String searchCalanderIcon ="//mdb-date-picker[@name='sfldate']";
	public static final String depTimeDropdown="//mdb-select[@name='sortbyDeparturetime']";
	public static final String bookingTypeDropdown ="//mdb-select[@name='searchBookingTypeSearch']";
	public static final String bookingBookingStatus ="//mdb-select[@name='searchBookingStatus']";
	public static final String departedShipmentCheckbox="//div//input[@id='checkbox1']";
	public static final String flightNumberInput="//span//input[@name='searchFlightNoSerial']";
	public static final String searchcalanderObject ="(//table[@class='picker__table'])[2]/tbody";
	public static final String ShipmentListRow ="//div[@class='row lazyLoadingcontainer confirmed']";
	
	
	/// New Booking 
	
	public static final String tabNewBooking ="//a[contains(text(),'New Booking')]";
	public static final String tabShipmentList="//a[contains(text(),'Shipment List')]";
	public static final String viewBookingIcon="(//div[@class='col-xl-4 col-lg-4 col-md-4 edit-icon handCursor imgPad'])[1]";
	public static final String editBookingIcon="(//div[@class='col-xl-4 col-lg-4 col-md-4 edit-icon handCursor imgPad'])[2]";


	/// track&trace
	public static final String track="(//div[@class='col-xl-4 col-lg-4 col-md-4 handCursor imgPad'])[1]";
	public static final String headertitleTrack = "//h1[contains(text(),'"+TestBase.Gettextvalue("trackawbandmailtitle")+"')]"; 
	public static final String closeBtn ="(//button[contains(text(),'Close')])[2]";


}
