package com.jal.pages;

import com.jal.auto.TestBase;

public class FlightStatusandScheduleObjects extends TestBase {

    public static final String headertitle = " //h1[contains(text(),'Flight Status and Schedule')]";
    public static final String headerflight="//mdb-select[@name='flightSelection']";
    public static final String drpdownselectflight="//ul[@class='select-dropdown']/li";
    public static final String headerairline="//mdb-select[@name='airlinename ']";
    public static final String drpdownselectAirlines="//ul[@class='select-dropdown']/li";
    public static final String inputflightnumber="//input[@name='searchFlightNoSerial']";
    public static final String departuredate="//mdb-select[@name='dateSelection']";
    public static final String drpdowndeparturedate="//ul[@class='select-dropdown']/li";
    public static final String btnsubmit="//button[@type='submit']";
    public static final String btnsearch="//img[@class='logo intermediate-bar-logo']";
    public static final String btnclose="(//button[@type='button'])[1]";
    
    public static final String departureairport="//mdb-select[@name='locationname']";
    public static final String drpdowndepartureairport="//ul[@class='select-dropdown']/li";
    public static final String arrivalairport="//mdb-select[@name='arrivalname']";
    public static final String drpdownarrivalairport="//ul[@class='select-dropdown']/li";
    
    public static final String statusflightfielderrormessage="//div[contains(text(),' Please enter valid value.')]";

    
    //******************* Dashboard **********//
    public static final String Dashboardpopupmessage="//div[@class='modal-content delRole']";
    public static final String Nobutton="(//button[contains(text(),' No')])[2]";
    public static final String Yesbutton="(//button[contains(text(),' Yes')])[2]";
    
    public static final String Dashboarddepartureairport="//mdb-select[@name='deplocValue']";
    public static final String Dashboarddrpdowndepartureairport="//ul[@class='select-dropdown']/li";
    public static final String Dashboardarrivalairport="//mdb-select[@name='arrlocValue']";
    public static final String Dashboarddrpdownarrivalairport="//ul[@class='select-dropdown']/li";
    public static final String Dasboarddeparturedate="(//mdb-select[@name='dateValue'])[1]";
    public static final String Dashboarddrpdowndeparturedate="//ul[@class='select-dropdown']/li";
    public static final String Dashboardbtnsearch="(//button[@type='submit'])[1]";
    public static final String Dashboarddepartureairporterrormessage="(//div[contains(text(),' Please enter valid value.')])[1]";
    public static final String Dashboardarrivalairporterrormessage="(//div[contains(text(),' Please enter valid value.')])[2]";
    
    
    public static final String Dashboardairline="(//mdb-select[@name='carrierCode'])[1]";
    public static final String DashboarddrpdownselectAirlines="//ul[@class='select-dropdown']/li";
    public static final String Dashboardinputflightnumber="(//input[@name='flightnumber'])[1]";
    public static final String Dashboardstatusdeparturedate="(//mdb-select[@name='dateValue'])[2]";
    public static final String Dashboardstatusdrpdowndeparturedate="//ul[@class='select-dropdown']/li";
    public static final String Dashboardstatusbtnsearch="(//button[@type='submit'])[2]";
    public static final String Dashboardstatusflightfielderrormessage="(//div[contains(text(),'Please enter valid value')])[3]";
    
   
}