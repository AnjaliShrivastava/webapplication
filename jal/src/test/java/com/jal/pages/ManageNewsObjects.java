package com.jal.pages;


import com.jal.auto.TestBase;

public class ManageNewsObjects extends TestBase {
	
	public static final String headertitle = "//h1[contains(text(),'Manage News')]";
	
	// **************   Create News
	
	
	
	//public static final String headingManageNews="//span/h1[contains(text(),'Manage News')]";
	public static final String tabExistingNews ="//a[contains(text(),'Existing News')]";
	public static final String tabCreateNews = "//a[contains(text(),'Create News')]";
	public static final String labelArea="//div[@class='dropdowns']/span[contains(text(),'Area')]";
	public static final String labelStation="//div[@class='dropdowns']/span[contains(text(),'Station')]";
	public static final String titleNews ="//div[@class='create-news-header row']/div/div/div[text() = 'News']";
	public static final String StartDateField  ="//mdb-date-picker[@name='createNewsStartDateValue']";
	public static final String EndDateField  ="//mdb-date-picker[@name='createNewsenddate']";
	public static final String InputTextField  ="//div[@class='ql-container ql-snow']//div[1]";
	public static final String cancelButton  ="//button[text()='CANCEL']";
	public static final String resetButton  ="//button[text()='RESET']";
	public static final String createButton  ="//button[text()='CREATE']";
	public static final String areadropdownfield="(//div[@class='dropdowns'])//mdb-select[@name='createNewsAreaValue']";
	public static final String stationdropdownfield="(//div[@class='dropdowns'])//mdb-select[@name='createNewsStation']";
	
	public static final String startDatecalanderObject= "(//table[@class='picker__table'])[3]/tbody";
    public static final String EndDatecalanderObject= "(//table[@class='picker__table'])[4]/tbody";
	public static final String dateIcon="(//div[@class='datepickers']//img[@class='shipmenticon imgCal_create'])[1]";	
	public static final String CreateNewsMessage="//span[contains(text(),'Your news has been saved and updated')]";
	public static final String EditEndDateField ="//mdb-date-picker[@name='mydate']";
	public static final String EditEndDatecalanderObject ="(//table[@class='picker__table'])[4]/tbody";
	
	//  *********     Existing News **************** //
	
	
	public static final String deleteIcon ="(//img[@class='deleteIcon icon handCursor d-block'])[1]";
	public static final String editIcon ="(//img[@class='editIcon icon handCursor d-block'])[1]";
	public static final String YesButton ="(//button[text()='Yes'])[1]";
	public static final String NoButton ="(//button[text()='No'])[1]";
	public static final String DeleteMessage ="//span[contains(text(),'Deleted Successfully')]";
	public static final String SaveButton ="//button[text()='SAVE']";	
	public static final String editNewsMessage ="//span[contains(text(),'Your news has been saved and updated')]";
	
	
	
	// *******   Advance Search ************** //
	public static final String InActiveRow  ="//div[@class='champ-card pos_rel inActive']";
	public static final String ActiveRow  ="//div[@class='champ-card pos_rel prflActive']";
	public static final String advanceSerachIcon  ="//*[@id='popUpIcon']";
	public static final String searchAreaDropdown  ="//div[contains(text(),'Select')][1]";
	public static final String searchStationDropdown  ="//div[contains(text(),'Select')][1]";
	public static final String statusAreaDropdown  ="//div[contains(text(),'Select')][1]";
	public static final String SubmitButton="//button[contains(text(),'SUBMIT')]";
	public static final String searchStartDate = "//mdb-date-picker[@name='searchStartDate']";
	public static final String searchEndDate = "//mdb-date-picker[@name='searchEndDate']";
	public static final String searchStartDatecalanderObject ="(//table[@class='picker__table'])[1]/tbody";
	public static final String searchEndtDatecalanderObject ="(//table[@class='picker__table'])[1]/tbody";
	public static final String AreaTextContainer ="//div[@class='col-xl-3 col-lg-3 col-md-3 col-7 area_cntnt']";
	public static final String StationTextContainer="//div[@class='col-xl-3 col-lg-3 col-md-3 col-7 station_cntnt']";
	public static final String startDateContainer ="//div[@class='col-xl-6 col-lg-6 col-md-6 col-7 st_date_cntnt']";
	public static final String EndDateContainer ="//div[@class='col-xl-6 col-lg-6 col-md-6 col-7 end_date_cntnt']";
	
	
	//*************  Sorting values **************** //
	
	public static final String sortByDropDown="//div[@class='sortby']//mdb-select";
	public static final String SortedAreaText= "//div[@class='col-xl-3 col-lg-3 col-md-3 col-7 area_cntnt']";
	public static final String SortedStationText ="//div[@class='col-xl-3 col-lg-3 col-md-3 col-7 station_cntnt']";
	public static final String SortedNewsIDText ="//div[@class='col-xl-4 col-lg-4 col-md-4 col-6 newsid_cntnt']";
	public static final String SortedNewsIDTextAssc="//div[@class='col-xl-4 col-lg-4 col-md-4 col-6 newsid_cntnt ng-star-inserted']";
	public static final String SortedstartDateText ="//div[@class='col-xl-6 col-lg-6 col-md-6 col-7 st_date_cntnt ng-star-inserted']";
	public static final String SortedEndDateText ="//div[@class='col-xl-6 col-lg-6 col-md-6 col-7 end_date_cntnt ng-star-inserted']"; // Not mandatory to use 
	public static final String AsscendingButton ="//span//img[@class='logo intermediate-bar-logo sort-up']";
	public static final String DsscendingButton ="//span//img[@class='logo intermediate-bar-logo sort-down']";
	public static final String dropdownvalue="//ul[@class='select-dropdown']/li";
	public static final String assecendingButton="//div[@class='col-xl-3 col-lg-3 col-md-3 col-3 sort']/span/img[1]";
	
	public static final String SortedActivenews  = "//div[@class='champ-card pos_rel prflActive']" ;
	public static final String SortedInActivenews  = "//div[@class='champ-card pos_rel inActive']" ;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
