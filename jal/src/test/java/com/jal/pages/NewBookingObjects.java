package com.jal.pages;

import com.jal.auto.TestBase;

public class NewBookingObjects extends TestBase {


	// ************* (1) Search Flight ********************** //

	public static final String searchFlightTextTab = "//div[contains(text(),'SEARCH FLIGHT ')]";
	public static final String labelOrigin = "//label[contains(text(),'Origin')]";
	public static final String labelDestination = "//label[contains(text(),'Destination')]";
	public static final String labelflightDate = "//span[contains(text(),'Flight Date')]";
	public static final String DateSelectField = "//input[@placeholder='Select Date']";
	public static final String DestinationselectField = "(//input[@type='search'])[2]";
	public static final String originSelectField = "(//input[@type='search'])[1]";
	public static final String PlusoneDayToggle ="(//div[@class='col-xl-3 col-4'])[1]";
	public static final String JPRToggle ="(//div[@class='col-xl-3 col-4'])[3]";
	public static final String DirectToggle ="(//div[@class='col-xl-3 col-4'])[2]";
	public static final String CalenderObject="//table[@class='picker__table']/tbody" ;


	// ************    Shipment Information *******************   //

	public static final String ShipmentListTab = "//div[contains(text(),' Shipment Information')]";
	public static final String radioULDBUP  ="//label[@for='uld-bup']";
	public static final String radioLooseBulk ="//label[@for='loose']";
	public static final String resetButton ="//button[contains(text(),'RESET')]";
	public static final String searchButton="(//button[contains(text(),'Search')])[1]";
	
	//****************** ULD/BUP ***********
	public static final String LD3 ="//input[@id='uldLD3']";
	public static final String LD2 ="//input[@id='uldLD2']";
	public static final String ULDPallet="//input[@id='uldPallet']";
	
	
    //****************** Losse Bulk ***********
	
	public static final String weightField="//input[@id='shipmentWeight']";
	public static final String dryIceField ="//input[@name='shipmentinfoDryIceWeightText']";
	public static final String overSizeToggle ="(//span[@class='lever'])[4]";
	public static final String AVIToggle ="(//span[@class='lever'])[5]";
	public static final String DGRToggle = "(//span[@class='lever'])[6]";
	public static final String commodityField ="//input[@id='shipmentinfoCommodityInput']";
	public static final String SHCField="//input[@id='shcInfoCodeInput']";
	public static final String SHCPlusIcon ="//img[@class='shcPlus pointer']";
	public static final String VolumeInput="//input[@id='weightwithVolumeInput']";
	
	
	// ************* (2) Select Flight ********************** //
	
	public static final String SelectButton ="(//button[contains(text(),'Select')])[2]";
	
	
	
	
	
	// ************* (3) AWB Details *******************************
	
	// Shipper Details
	
	public static final String shipperAccordian ="//span[contains(text(),'Shipper')]";
	public static final String radioUseAgent ="//label[@for='agtshipper']";
	public static final String radioUseExistingShipper ="//label[@for='exShipper']";	
	public static final String radioNewShipper ="//label[@for='newShipper']";
	public static final String ShortNameShipper ="//input[@id='exInputShipperNameInput']";
	
	
	// New Shipper
	
	public static final String newShipperName ="//input[@id='newInputShipperName']" ;
	public static final String newShipperPhNo ="//input[@id='newInputShipperPhone']" ;
	public static final String newShipperAddress ="//input[@id='newInputShipperAddress']";
	
	// consignee Details 
	
	//Use Existing Consignee
	public static final String newConsigneeRadioButton ="//label[@for='newConsignee']";
	public static final String consignneAccordian ="//span[contains(text(),'Consignee')]";
	public static final String radioExConsignee ="//label[@for='exConsignee']";
	public static final String ShortName = "//input[@id='exInputConsigneeNameInput']";
	public static final String  consigneeSerachButton ="(//button[contains(text(),'Search')])[2]";
	
	// New Consignee 
	
	public static final String newConsigneeName ="//input[@id='newInputConsigneeName']" ;
	public static final String newConsigneePhNo ="//input[@id='newInputConsigneePhone']" ;
	public static final String newConsigneeAddress ="//input[@id='newInputConsigneeAddress']";
	
	// Shipment Details 
	public static final String shipmentAccordian ="//span[contains(text(),'Shipment Details')]";
	public static final String pieces ="//input[@id='pieces']" ;
	public static final String ChargesDropDown="//mdb-select[@name='charges']";
	public static final String valueOfCarriage ="//input[@id='valueCarriage']";
	public static final String natureOfGoods ="//input[@id='natureOfGoodsInput']";
	public static final String remarks  ="//input[@id='chargesInput']" ;
	public static final String deliveryRemarks ="//input[@id='deliveryAtRemark']";
	public static final String deliveryAtDropdown="(//mdb-select[@placeholder='Select'])[1]";
	
	public static final String cancelBookingButton ="//button[contains(text(),'Cancel Booking')]";
	public static final String continueButton ="//button[contains(text(),'continue')]";
	
	
	/// Payment ////////////
	
	public static final String message1 ="//div[contains(text(),'At this moment of time we are unable to calculate the payment information. You can always contact JAL')]";
	public static final String message2 ="//div[contains(text(),'If OK, please accept the T&C and proceed with your booking')]";
	public static final String noButton ="(//button[contains(text(),'No')])[2]";
	public static final String yesButton ="(//button[contains(text(),'Yes')])[2]";
	
	public static final String TAndC="//div[@class='switch  mdb-color-switch primary-switch termsCheck']";
	public static final String createBookingButton ="//button[contains(text(),'create booking')]";
	public static final String BookinCreationMessage="//div[contains(text(),'Your booking will be created.')]";
	public static final String bookingConfirmationYes ="(//a[contains(text(),'Yes')])[3]";
	public static final String bookingConfirmationNo ="(//a[contains(text(),'No')])[3]";
	
	public static final String bookingCancellationMessage="//div[contains(text(),'Are you sure you want to cancel booking')]";
	
	
	public static final String printLabelButton ="//label[contains(text(),'PRINT LABELS')]";
	public static final String newBookingButton ="//label[contains(text(),'NEW BOOKING')]";
	public static final String editBookingButton="//label[contains(text(),'EDIT BOOKING')]" ;
	public static final String CancelBookingButton="//label[contains(text(),'CANCEL BOOKING')]";
	
	public static final String NewAWBSection="//div[@class='col-lg-12 col-md-12 col-xs-12 col-12 text-center txtfill']";
	public static final String originSection="//span[@class='b-l-1 txtfill']" ;
	public static final String desSection ="//div[@class='col-xxl-2 col-xl-2 col-md-2 col-lg-2 col-2 fltcodeFont']";
	
	
	// ********* Cancel Booking Button *******************
	public static final String cancelBookingMessage ="//div[contains(text(),'Do you wish to cancel Booking')]";
	public static final String cancelYesButton ="(//button[contains(text(),'Yes')])[1]";
	public static final String FlightDetailsSection = "//div[@class='flightcontent']";
	public static final String BookingStatus ="(//div[@class='col-xxl-2 col-xl-2 col-md-2 col-lg-2 txtvalue'])[5]";
	
	public static final String DryICE ="//button[contains(text(),'info (DRY ICE)')]";
	public static final String ICEPieces ="//input[@formcontrolname='addDIPiece']";
	public static final String ICEWGT="//input[@formcontrolname='addDIWeightPerPiece']";
	public static final String PLUSIcon ="(//button[@class='icon-button pl-0'])[1]";
	public static final String DoneBtn ="//button[contains(text(),'DONE')]";
	public static final String leverOpt="//span[@class='lever']";
	public static final String YesStatus="(//a[contains(text(),'Yes')])[2]";

}
