package com.jal.pages;

import com.jal.auto.TestBase;

public class ContractDataObjects extends TestBase {

	public static final String headertitle = " //h1[contains(text(),'Contract Data')]";
	public static final String selectdatecalendar="//mdb-date-picker[@name='validDateSearchData']";
	public static final String origin="//mdb-select[@name='orgn']";
	public static final String origindrpdown="//ul[@class='select-dropdown']/li";
	public static final String zone="//mdb-select[@name='orgnList']";
	public static final String zonedrpdown="//ul[@class='select-dropdown']/li";
	public static final String downloadexcel="(//img[@class='logo_refresh intermediate-bar-logo handCursor'])[1]";

	public static final String inputallotmentcode="//input[@name='alotmntCode']";
	public static final String btnsubmit="//button[@type='submit']";
	public static final String btnclose="(//button[@type='button'])[5]";
	public static final String btnsearch="//img[@class='logo intermediate-bar-logo']";
	public static final String clickremark="(//div[@class='row remarksClose'])[1]";
	public static final String clickXicon="(//div[@class='row remarksClose'])[1]";
	public static final String clicksortby="//div[@class='col-xxl-4 col-xl-4 col-lg-3 col-md-5 col-5 selecting']";
	public static final String sortbydrpdown="//ul[@class='select-dropdown']/li";
	public static final String datefielderrormessage="//div[contains(text(),'Date required')]";
	public static final String Allotmentcodeerrormessage="//div[contains(text(),'Invalid Character')]";
	public static final String originerrormessage="//div[contains(text(),'Either Origin or Zone required')]";
	public static final String remarktextfield="//input[@class='inputText remarkInput']";
	public static final String origindestinationtext="//div[@class='col-xxl-4 col-xl-4 col-lg-4 col-md-2 cardContent']";
	public static final String Flighttext=" //div[@class='col-xxl-3 col-xl-3 col-lg-3 col-md-2 cardContent']";
    public static final String calendarviewstartdate ="(//div[@class='picker__frame picker__box'])[1]";
    public static final String startdateclosebutton ="(//button[@class='picker__button--close'])[1]";
}
