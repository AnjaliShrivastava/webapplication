package com.jal.pages;

import com.jal.auto.TestBase;

public class AllotmentUtilizationObjects extends TestBase {

    public static final String headertitle = " //h1[contains(text(),'Allotment Utilization')]";
    public static final String btnSearch = "//img[@class='logo intermediate-bar-logo']"; 
    public static final String btnSearchview="img[contains(@src,'search.svg')]";
    public static final String Searchstartdate ="//mdb-date-picker[@name='fromDatePeriod']";
    public static final String calendarviewstartdate ="(//div[@class='picker__frame picker__box'])[1]";
    public static final String calendarviewEnddate ="(//div[@class='picker__frame picker__box'])[2]";
    public static final String Searchenddate  ="//mdb-date-picker[@name='toDatePeriod']";
    public static final String buttonsubmit ="//button[@type='submit']";
    public static final String clickPDF  ="//img[contains(@src,'pdf.svg')]";
    
    public static final String flightnumbersuffix  ="(//input[@name='searchFlightNoSerial'])[1]";
    public static final String flightnumbererrormessage ="(//div[contains(text(),' Please enter valid value.')])[1]";
    public static final String daysofweeks ="//label[@for='alldays']";
    public static final String daysofweekserrormessage  ="//div[contains(text(),'Please select “ALL” or specific days.')]";
    public static final String Calendarheader="(//div[@class='picker__frame picker__box'])[1]";
    public static final String originfield="//mdb-select[@name='orgn']";
    public static final String origindrpdown ="//ul[@class='select-dropdown']/li";
    public static final String originerrormessage ="//div[contains(text(),' Origin required ')]";
    public static final String Fromdateerrormessage="//div[contains(text(),'Please select the From Date')]";
    public static final String Todateerrormessage ="//div[contains(text(),'Please select the To Date')]";
    
    public static final String startDatecalanderObject= "(//div[@class='picker__frame picker__box'])[1]";
    public static final String startdateclosebutton ="(//button[@class='picker__button--close'])[1]";
    public static final String enddateclosebutton="(//button[@class='picker__button--close'])[2]";
    public static final String enddate  ="(//mdb-date-picker[@name='toDatePeriod'])[2]";
    public static final String EndDatecalanderObject= "(//table[@class='picker__table'])[4]/tbody";
    
   
    
}
