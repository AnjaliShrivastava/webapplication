package com.jal.pages;



import com.jal.auto.TestBase;

public class usersandrolesObjects extends TestBase {
	
	public static final String headertitle = "//h1[contains(text(),'Users and Roles')]";
	
	// **************   TABS AND HEADINGS  *******************   //

	public static final String mainTabRoles ="//a[contains(text(),'Roles ')]";	
	public static final String mainTabUsers ="//a[contains(text(),' Users ')]";
	public static final String tabCreateRole = "//a[contains(text(),'Create Role')]";
	public static final String tabExistingRoles = "//a[contains(text(),'Existing Roles')]";
	public static final String tabCreateUser = "//a[contains(text(),'Create User')]";
	public static final String tabExistingUsers = "//a[contains(text(),'Existing Users')]";

	// **************   CREATE ROLES  *******************   //

	public static final String inputRoleName ="//input[@name='createRoleType']";
	public static final String inputDescription="//textarea[@name='createRolesContent']";
	public static final String labelPermission="//span[contains(text(),'PERMISSIONS')]";
	public static final String successMessage="//span[contains(text(),'Your data has been saved and updated')]";
	public static final String permission="(//span[@class='lever'])[3]";
	
	// **************   EXISTING ROLES  *******************   //
	
	public static final String labelSearchRole ="//label[contains(text(),'Search Role:')]";
	public static final String inputSearchRoleType="//input[@name='searchRoleType']";	
	public static final String seachRoleIcon="//span[@class='searchimage searchroleImage']";
	public static final String labelRoleName ="//div[contains(text(),'ROLE NAME')]";
	public static final String labelDescription ="//div[contains(text(),'DESCRIPTION ')]";
	public static final String roleNameContainer="//div[@class='col-xxl-4 col-xl-3 col-lg-3 col-md-4 col-8 roleNameCntnt']";
	public static final String roleDescriptionContainer="//div[@class='col-xxl-7 col-xl-8 col-lg-7 col-md-6 d-lg-block d-md-block d-xl-block d-none desc']";
	public static final String roleEditIcon="//a[@class='collapsed card-link mano d-block']";
	public static final String roledeletIcon ="//*[@id='Existing_Role']/div[2]/div/div[3]/div/div/div/div/div/div/div/div[3]/div/div[2]/a/img[1]";
	public static final String errorMessageNoRole1st=" //div[contains(text(),'We could not find what you are looking for')]";
	public static final String errorMessageNoRole2nd =" //div[contains(text(),'You can change the search criteria or correct the data entered')]";
	public static final String errorMessageExistingRole=" //span[contains(text(),'Role Name already Exists! Please try with different one')]";
	
	public static final String inputEditRoleName ="//input[@name='editRoleName']";
	public static final String inputEditDescription="//textarea[@name='editRoleDescription']";
	public static final String deleteYesButton = "//*[@id='Existing_Role']/div[2]/div/div[3]/div/div/div/div/div/div/div/div[4]/div/div/div[2]/button[2]";
	
	public static final String DeletesuccessMessage=" //span[contains(text(),'Role has been deleted')]";
	public static final String errorMsgRoleWithUser=" //span[contains(text(),'Cannot delete the role as it has been linked to 1 or more users. Please de-link the role from the users to proceed with delete')]";
	
	public static final String errorMsgEditRole="//div[contains(text(), 'This role has been linked to 1 or more users. Are you sure you want to save the updates?')]";
	
	public static final String rolecloseIcon ="//*[@id='Existing_Role']/div[2]/div/div[3]/div/div/div/div/div/div[1]/div/div[3]/div/div[4]/img";
	
	// **************   CREATE USERS     *******************   //
	public static final String labelusertype="//div[@class='col-6 col-md-12']/label[contains(text(),'User Type ')]";
	public static final String userTypefield="(//div[@class='col-6 col-md-12'])//mdb-select[@name='createselect']";
	public static final String dropdownvalue="//ul[@class='select-dropdown']/li";
	public static final String TextField="//div[@class='col-6 col-md-12 short ng-star-inserted']/input[1]";
	public static final String buttoncheck="//button[contains(text(),'CHECK')]";
	public static final String portalId="//input[@name='newportalId']";
	public static final String roledropdownvalue="//mdb-select[@name='agentConsginee']";
	
	
	public static final String clickprooflistAll ="//label[@for=\"defaultGroupExample1\"]";
	public static final String clickprooflistlimited ="//label[@for=\"defaultGroupExample2\"]";
	public static final String prooflisttextfield="//div[@class='col-xxl-5 col-xl-7 col-lg-6 col-md-7 col-6 create_btn limitBtn ng-star-inserted']/span/input[1]";
	
	public static final String agentcreatebutton	="(//button[contains(text(),'CREATE')])[2]";
	public static final String agentcancelbutton="//div[@class='row btns_usr_dtls hidding-xxl label_header ng-star-inserted']/div/button[text()='CANCEL']";
	public static final String consigneecreatebutton ="(//button[contains(text(),'CREATE')])[1]";
	  
	public static final String buttoncopymessage="//button[contains(text(),'Copy Message')]";
	public static final String texttoastmessage="//div[@class='col-xl-11 col-lg-11 col-md-10 col-10 successPadd']";
	
	public static final String employeeId="//input[@name='empId']";
	public static final String firstname="//input[@name='staffFirstName']";
	public static final String lastname="//input[@name='staffLastName']";
	public static final String department="//input[@name='staffDep']";
	public static final String telephone="//input[@name='staffTelephone']";
	public static final String portalIdofExistinguser="//input[@name='staffPortalId']";
	public static final String rolesdropdown="//mdb-select[@name='staffresetRole']";
	public static final String staffCreatebutton="(//button[contains(text(),'CREATE')])[1]";
	
	//******************************* SORTBY *****************************//
	public static final String usersortBydropdown="//span[@class='mdl-selectfield sortByDropdown']/mdb-select[@class='sortSelection ng-untouched ng-valid ng-dirty']";
	public static final String sortedportalIDtext="//div[@class='col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-5 portalUsername']";
	public static final String sortedusertypetext="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 d-md-block d-none Staff']";
	public static final String sortedfirstnametext="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 col-4 fName']";
	public static final String sortedlastnametext="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 d-md-block d-none lnameCntnt']";	

//*************************** SEARCH USERS ******************************//
	public static final String searchusericon="//a[@id='popModal']";
	public static final String searchuserportalID="//input[@name='searchId']";
	public static final String searchusertypedropdown="//div[contains(text(),'User Type')]/parent::div[1]/div[1]";
	public static final String searchstatusdropdown="//div[contains(text(),'User Type')]/parent::div[1]/div[2]";
	public static final String searchuserfirstname="//input[@name='searchfirstName']";
	public static final String searchuserlastname="//input[@name='searchlastName']";
	public static final String searchbuttonsubmit="(//div[@class='col-xxl-5 col-xl-5 col-lg-6 col-md-6 col-6 submitButton'])//button[1]";
	public static final String searchbuttonclose="//button[text()='CLOSE']";
	
	public static final String portalIdcontainer="//div[@class='col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-5 portalUsername']";
	public static final String usertypecontainer="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 d-md-block d-none Staff']";
	public static final String firstnamecontainer="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 col-4 fName']";
	public static final String lastnamecontainer="//div[@class='col-xxl-2 col-xl-2 col-lg-2 col-md-2 d-md-block d-none lnameCntnt']";
	
//**************************** ADVANCE EDIT USER **********************//

	public static final String userediticon="//a[@class='collapsed card-link d-block']";
	public static final String userdeleteicon="(//img[@src='/jalportal/0.0.0-75d64008/assets/Images/delete.svg'])[1]";
//******************************** EDIT USERS **************************//
	public static final String edituserportalstatusdropdown="(//div[@class='col-xl-12 col-lg-12 col-md-12 col-6 portalstatus colour'])//mdb-select[@name='agentPortalStatus']";
	public static final String edituserroledropdown="(//div[@class='col-xl-12 col-lg-12 col-md-12 col-6 rolestatus colour'])//mdb-select[@name='agentRole']";
	public static final String editprooflistAll ="//div[@class='col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-6 margnBtm']//div";
	public static final String editprooflistlimitedbutton="//div[@class='col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-6 limitedPl margnBtm']//div";
	public static final String editprooflistlimitedtext="//div[@class='col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-12 proofTxt ng-star-inserted']/input[1]";

	public static final String SubmitButton ="//*[contains(text(),'SUBMIT')]";
	
	
	
	public static final String IDPasswoRDTEXT="//div[@class='col-xl-11 col-lg-11 col-md-10 col-10 successPadd']";
	
	public static final String UserCardActive ="//div[@class='cardmain cc userCard']";
	public static final String UserCardInActive ="//div[@class='cardmain cc inactiveuserCard']";
	public static final String UserCardLockedUser="//div[@class='cardmain cc lockeduserCard']";
	
	
	
	//Edit Users 
	
	public static final String editUserIcon="(//a[@class='collapsed card-link d-block'])[1]";
	public static final String editPortalStatus="(//mdb-select[@name='agentPortalStatus'])[1]";
	public static final String editAgentRole="(//mdb-select[@name='agentRole'])[1]";
	public static final String editlimitedProofList="//textarea[@name='existlimitedContent']";
	public static final String deletUserIcon="(//a[@class='d-block'])[1]";
	public static final String deletUserMessage="//span[contains(text(),' User has been deleted successfully')]";
	public static final String downSortArrow ="//span[@class='search']/span[4]/img";
	
	
}


	
	
	
	
	
	
	


