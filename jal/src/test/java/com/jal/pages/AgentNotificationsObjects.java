package com.jal.pages;

import com.jal.auto.TestBase;

public class AgentNotificationsObjects extends TestBase {

    public static final String headertitle = " //h1[contains(text(),'Flight Status and Schedule')]";
    public static final String tabnotification = "//span[contains(text(),'Notifications')]";
    public static final String sortbydrpdownvalue = "//div[@class='col-xl-8 col-lg-8 col-md-12 col-12']";
    public static final String dropdownvalues = "//ul[@class='select-dropdown']/li";
    public static final String ascendingvalues = "//div[@class='col-xl-3 col-lg-3 col-md-3 col-4 sort']/span/img[1]";
    public static final String btnsearchicon = "//*[@id='popUpIcon']";
    public static final String txtawbprefix= "//*[@name='awbPrefix']";
    public static final String txtawbsuffix = "//*[@name='awbPrefixSerial']";
    public static final String shortnamevalues = "//*[@name='shortName']";
    
    
    public static final String dateField = "//mdb-date-picker[@name='sfldate']";
    public static final String startdatecalendarObjects  ="(//div[@class='picker__frame picker__box'])[1]";
    public static final String btnsubmit="//*[contains(text(),'SUBMIT')]";
    public static final String invalidAWBmessage="//div[contains(text(),'Invalid AWB Prefix')]";
    public static final String invalidShortNamemessage="//div[contains(text(),'Invalid Short Name')]";
    public static final String sortedAWBText ="//div[@class='col-xl-3 col-lg-3 col-md-3 col-8 awb-text']";
    public static final String sortedShortname ="//div[@class='col-xl-3 col-lg-3 col-md-3 col-8 shrtname-text']";
    public static final String sortedDatetext ="//div[@class='col-xl-3 col-lg-3 col-md-3 col-8 date-text']";
    public static final String sortedTimetext="//div[@class='col-xl-3 col-lg-3 col-md-3 col-8 time-cntnt']";
    public static final String sortedmessagetext ="//div[@class='col-xl-5 col-lg-5 col-md-5 d-none d-sm-block notification-block']";
    public static final String myprofile = "//a[contains(text(),'My Profile')]";
    public static final String welcome="//button[@class='menuDropDown btn']/span[1]" ;
    public static final String profiledrpdown ="(//div[@class='dropdown-menu dropdown-primary details-dropdown fadeInDropdown'])";
	public static final String logout = "//a[contains(text(),'Logout')]";
	public static final String notificationBell="//i[@class='fa fa-lg fa-bell-o notification-Icon']";
}

