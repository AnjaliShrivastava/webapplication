package com.jal.pages;

import com.jal.auto.TestBase;

public class TrackAWBandMailObjects extends TestBase {

	public static final String headertitle = " //h1[contains(text(),'Track AWB and Mail')]";

	public static final String dropdownselect = "(//div[@class=' col-md-2 col-lg-2 col-xl-1  widthVary'])//mdb-select[@name='awbmailText']";
	public static final String drpdownvalues = "//ul[@class='select-dropdown']/li";
	public static final String txtAWBseries1 = "//*[@name='airwayBillSerial1']";
	public static final String txtAWBseries2 = "//*[@name='airwayBillSerial2']";
	public static final String txtAWBseries3 = "//*[@name='airwayBillSerial3']";
	public static final String txtAWBseries4 = "//*[@name='airwayBillSerial4']";
	public static final String txtAWBseries5 = "//*[@name='airwayBillSerial5']";
	public static final String advancesearch = "(//img[@class='logo intermediate-bar-logo'])[1]";
	public static final String btnsubmit = "(//button[contains(text(),'Search')])";
	public static final String trackAWBAndMAILSec ="//div[@class ='col-md-12']/ul";
}
