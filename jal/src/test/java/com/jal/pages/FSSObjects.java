package com.jal.pages;

import com.jal.auto.TestBase;

public class FSSObjects extends TestBase {

    public static final String headertitle = " //h1[contains(text(),'Flight Status and Schedule')]";
    public static final String selectdrpdownflight = "//div[contains(text(),' Status ')]";
    public static final String drpdownstatus = "//span[text()='Status']";
    public static final String airlines = "//div[contains(text(),'JAL')]";
    public static final String flightnumber = "//input[@name='flightnumber']";
    public static final String departuredate = "//div[contains(text(),'Today')]";
    public static final String drdwYesterday = "//span[text()='Yesterday']";
    public static final String drdwTomorrow = "//span[text()='Tomorrow']";
    public static final String expview = "//img[contains(@src,'search.svg')]";
    
    public static final String drpdownschedule = "//span[text()='Schedule']";

    
}
