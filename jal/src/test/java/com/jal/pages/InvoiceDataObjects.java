package com.jal.pages;

import com.jal.auto.TestBase;

public class InvoiceDataObjects extends TestBase {

	public static final String headertitle = "//h1[contains(text(),'Invoice Data')]";
	public static final String clickPDF ="//div[@class='view overlay mainPdf']";
	public static final String loginbutton="(//button[@id='login_signIn'])[1]";
    
}
