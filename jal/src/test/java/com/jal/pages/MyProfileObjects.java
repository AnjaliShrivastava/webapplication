package com.jal.pages;

import com.jal.auto.TestBase;

public class MyProfileObjects extends TestBase {

	public static final String headertitle = " //h1[contains(text(),'My Profile')]";

	// ************** TABS AND HEADINGS ******************* //
	public static final String tabmanage = "//a[contains(text(),'Manage')]";

	// ************** WELCOME PROFILE ******************* //
	
	public static final String myprofile = "(//a[@routerlink='./myprofile'])[2]";
	public static final String logout = "//a[contains(text(),'Logout')]";
	public static final String welcomebutton="//span[contains(text(),'Welcome')]" ;
	

	public static final String editicon = "//div[@class='col-xl-5 col-lg-5 col-md-5 col-2 edit-profile-icon']/img";
	public static final String allbutton = "//label[@for='radio100']";
	public static final String limitedbutton = "//label[@for='radio101']";
	public static final String limitedtxtfield = "//div[@class='col-xl-10 col-lg-10 col-md-10 col-11 md-form prooflist-group']/input[1]";

	public static final String successtoastmsg = "//div[contains(text(),' Your proof list group has been saved and updated!')]";
	public static final String canceltoastmsg = "//div[contains(text(),'Your proof list group update has been cancelled ')]";

	// ************** PASSWROD CHANGE ******************* //
	public static final String btnchangepassword = "(//button[contains(text(),'CHANGE PASSWORD')])[1]";
	public static final String newpassword = "//span[contains(text(),'New Password')]/parent::div[1]/input";
	public static final String confirmpassword = "//span[contains(text(),'New Password')]/parent::div/following-sibling::div[1]/input";
	public static final String buttonprofilesubmit = "//button[contains(text(),'SUBMIT')]";
	public static final String btncancel = "//button[contains(text(),'CANCEL')]";

	// ************** TOAST MESSAGES ******************* //
	public static final String passwordsizemessage = "//div[contains(text(),' Password should have 8 minimum characters and should have at least 1 Uppercase letter, 1 Number and 1 Special character No Space is allowed ')]";
	public static final String passwordsuccessmessage = "//div[contains(text(),' Your data has been saved and updated ')]";
	public static final String passwordwarningmessage = "//div[contains(text(),'Password cannot be changed more than once in a day')]";
	public static final String passwordIncorrectmessage = "//*[@id='nav-tabContent']/div[2]";
	public static final String emptyPasswordMessage="//div[contains(text(),'minimum characters and should have at least 1 Uppercase letter, 1 Number and 1 Special character No Space is allowed')]";
	public static final String passwordSeccessMessage ="//div[contains(text(),' Your data has been saved and updated')]";
	
	
	
	
	
	
	
	
	
	public static final String forgotPasswordLink = "(//a[@id='login_forgotPassword'])[1]";
	
	public static final String forgotPasswordTitle ="(//span[contains(text(),'FORGOT PASSWORD')])[2]";
	
	public static final String forgotPasswordPortalID ="(//input[@id ='login_username' and @required='required'])[2]" ;
	
	public static final String forgotPasswordName ="//input[@id ='name' and @required='required']";
	
	public static final String forgotPasswordEmail = "//input[@id ='email' and @required='required']";
	
	public static final String forgotPasswordPhone ="(//input[@id ='phone'])[2]" ;
	
	public static final String forgotPasswordSubmit="(//button[@name='submitButton'])[2]" ;
	
	public static final String forgotPasswordReset="(//button[@name='resetButton'])[2]" ;
	
	public static final String forgotPasswordBackToLoginButton="(//button[@name='backButton'])[2]" ;
	
	
	public static final String forgotPasswordSuccessMessage ="(//span[contains(text(),'Your request has been forwarded to the administrator, they will get back to you shortly')])[2]";



}
    

