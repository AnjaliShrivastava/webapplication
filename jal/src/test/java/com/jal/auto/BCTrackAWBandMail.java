package com.jal.auto;

import java.util.Map;

import com.jal.pages.GeneralObjects;
import com.jal.pages.ShipmentListObjects;
import com.jal.pages.TrackAWBandMailObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCTrackAWBandMail extends TestBase {
	
	public void NavigatetoTrackAWBandMail(ExtentTest logger ) {
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuTrackAWBandMail , "is displayed on the page" , logger );
			click(GeneralObjects.MenuTrackAWBandMail , logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	
	public void TrackAWBTab(Map<String, String> data, ExtentTest logger) throws InterruptedException {
				try {

					//dropDownSelection(TrackAWBandMailObjects.dropdownselect, TrackAWBandMailObjects.drpdownvalues,"AWB", logger);
					//click(TrackAWBandMailObjects.drpdownvalues,logger);
					ClickSendText(TrackAWBandMailObjects.txtAWBseries1, data.get("AWBSerial"), logger);
					ClickSendText(TrackAWBandMailObjects.txtAWBseries2, data.get("AWBSerial1"), logger);
					ClickSendText(TrackAWBandMailObjects.txtAWBseries3, data.get("AWBSerial2"), logger);
					ClickSendText(TrackAWBandMailObjects.txtAWBseries4, data.get("AWBSerial3"), logger);
					ClickSendText(TrackAWBandMailObjects.txtAWBseries5, data.get("AWBSerial4"), logger);
					click(TrackAWBandMailObjects.btnsubmit, logger);
					logger.log(LogStatus.PASS, "User able to click TrackAWB");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click TrackAWB");

				}		
	}
	
	public void TrackAWBAdvancesearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try {

			//dropDownSelection(TrackAWBandMailObjects.dropdownselect, TrackAWBandMailObjects.drpdownvalue,TestBase.Gettextvalue("TrackAWB"), logger);
			//click(TrackAWBandMailObjects.drpdownvalues,logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries1, data.get("AWBPrefix"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries2, data.get("AWBSerial"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries3, data.get("ShortName"), logger);
			SubmitButton(ShipmentListObjects.btnSearch , logger);
			ClickSendText(TrackAWBandMailObjects.txtAWBseries4, data.get("AWBSerial3"), logger);
			ClickSendText(TrackAWBandMailObjects.txtAWBseries5, data.get("AWBSerial4"), logger);
			click(TrackAWBandMailObjects.btnsubmit, logger);
			//click(TrackAWBandMailObjects.advancesearch, logger);
			
			//click(TrackAWBandMailObjects.btnsubmit, logger);
			logger.log(LogStatus.PASS, "User able to click TrackAWB");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click TrackAWB");

		}		
}
	
	public void TrackMail_BasicSearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try {

			dropDownSelection(TrackAWBandMailObjects.dropdownselect, TrackAWBandMailObjects.drpdownvalues,"Mail", logger);
			//click(TrackAWBandMailObjects.drpdownvalues,logger);
			ClickSendText(TrackAWBandMailObjects.txtAWBseries1, data.get("AWBSerial"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries2, data.get("AWBSerial1"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries3, data.get("AWBSerial2"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries4, data.get("AWBSerial3"), logger);
			//ClickSendText(TrackAWBandMailObjects.txtAWBseries5, data.get("AWBSerial4"), logger);
			click(TrackAWBandMailObjects.btnsubmit, logger);
			logger.log(LogStatus.PASS, "User able to click TrackAWB");
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click TrackAWB");

		}		
}

public void TrackMail_AdvancedSearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
try {

	//dropDownSelection(TrackAWBandMailObjects.dropdownselect, TrackAWBandMailObjects.drpdownvalue,TestBase.Gettextvalue("TrackAWB"), logger);
	//click(TrackAWBandMailObjects.drpdownvalues,logger);
	//ClickSendText(TrackAWBandMailObjects.txtAWBseries1, data.get("AWBPrefix"), logger);
	//ClickSendText(TrackAWBandMailObjects.txtAWBseries2, data.get("AWBSerial"), logger);
	//ClickSendText(TrackAWBandMailObjects.txtAWBseries3, data.get("ShortName"), logger);
	SubmitButton(ShipmentListObjects.btnSearch , logger);
	ClickSendText(TrackAWBandMailObjects.txtAWBseries4, data.get("AWBSerial1"), logger);
	//ClickSendText(TrackAWBandMailObjects.txtAWBseries5, data.get("AWBSerial4"), logger);
	click(TrackAWBandMailObjects.btnsubmit, logger);
	//click(TrackAWBandMailObjects.advancesearch, logger);
	
	//click(TrackAWBandMailObjects.btnsubmit, logger);
	logger.log(LogStatus.PASS, "User able to click TrackAWB");
} catch (Exception e) {
	logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click TrackAWB");

}		
}}
