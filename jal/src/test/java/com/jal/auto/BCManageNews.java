package com.jal.auto;
import java.util.Date;
import java.util.Map;

import com.jal.pages.GeneralObjects;
import com.jal.pages.ManageNewsObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCManageNews extends TestBase {

	public BCManageNews() {

		
	}
	Date d = new Date();

	@SuppressWarnings("deprecation")
	int todayDate = d.getDate();
	String tDate = Integer.toString(todayDate);
	int tomDate = todayDate + 1 ;
	String tomoDate = Integer.toString(tomDate);


	public void CreateNews(Map<String, String> data , ExtentTest logger) throws InterruptedException { 
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);		 
			click(ManageNewsObjects.tabCreateNews, logger);
			dropDownSelection(ManageNewsObjects.areadropdownfield,ManageNewsObjects.dropdownvalue , data.get("Area"), logger);

			dropDownSelection(ManageNewsObjects.stationdropdownfield,ManageNewsObjects.dropdownvalue ,data.get("Station"), logger );

			selectCalanderDate(ManageNewsObjects.StartDateField ,ManageNewsObjects.startDatecalanderObject ,tDate , logger );
			selectCalanderDate(ManageNewsObjects.EndDateField ,ManageNewsObjects.EndDatecalanderObject , tomoDate, logger);
			ClickSendText(ManageNewsObjects.InputTextField,data.get("Text"), logger);
			click(ManageNewsObjects.createButton , logger);
			verifyElementDisplayed(ManageNewsObjects.CreateNewsMessage , "News has been created successfully" , logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}


	public void Resetvalues(Map<String, String> data , ExtentTest logger) throws InterruptedException { 
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			click(ManageNewsObjects.tabCreateNews, logger);
			dropDownSelection(ManageNewsObjects.areadropdownfield,ManageNewsObjects.dropdownvalue , data.get("Area"), logger );
			dropDownSelection(ManageNewsObjects.stationdropdownfield,ManageNewsObjects.dropdownvalue , data.get("Station"), logger );
			selectCalanderDate(ManageNewsObjects.StartDateField ,ManageNewsObjects.startDatecalanderObject ,tDate , logger );
			selectCalanderDate(ManageNewsObjects.EndDateField ,ManageNewsObjects.EndDatecalanderObject , tomoDate, logger);
			ClickSendText(ManageNewsObjects.InputTextField,data.get("Text") ,logger );
			Thread.sleep(3000);
			click(ManageNewsObjects.resetButton , logger);
			Thread.sleep(1000);
			verifyElementcontainsText(ManageNewsObjects.areadropdownfield, "Select"	 , "Entered value is resetted" ,logger); 
			verifyElementcontainsText(ManageNewsObjects.stationdropdownfield, "All" , "Entered value is resetted",logger);
			verifyElementText(ManageNewsObjects.StartDateField, "" , "Entered value is resetted",logger);
			verifyElementText(ManageNewsObjects.EndDateField, "" , "Entered value is resetted", logger);
			verifyElementText(ManageNewsObjects.InputTextField, "" , "Entered value is resetted", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}



	}


	public void CloseCreateNewspage(Map<String, String> data , ExtentTest logger) throws InterruptedException { 
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			click(ManageNewsObjects.tabCreateNews , logger);
			dropDownSelection(ManageNewsObjects.areadropdownfield,ManageNewsObjects.dropdownvalue , data.get("Area"), logger );
			dropDownSelection(ManageNewsObjects.stationdropdownfield,ManageNewsObjects.dropdownvalue , data.get("Station") , logger);
			Thread.sleep(1000);
			click(ManageNewsObjects.cancelButton , logger);
			Thread.sleep(1000);
			verifyElementSelected(ManageNewsObjects.tabExistingNews ,"is selected and Create News page has been closed now" , logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void SearchNewsbyinputField(Map<String, String> data ,ExtentTest logger) throws InterruptedException {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			Thread.sleep(1000);
			String [] text = TestBase.arrayElementText(ManageNewsObjects.ActiveRow, logger);
			Thread.sleep(1000);
			click(ManageNewsObjects.advanceSerachIcon, logger);
			Thread.sleep(1000);    	

			dropDownSelection(ManageNewsObjects.searchAreaDropdown,ManageNewsObjects.dropdownvalue , text[1] ,logger);
			dropDownSelection(ManageNewsObjects.searchStationDropdown,ManageNewsObjects.dropdownvalue , text[2], logger );
			dropDownSelection(ManageNewsObjects.statusAreaDropdown,ManageNewsObjects.dropdownvalue , "Active", logger);  	
			click(ManageNewsObjects.SubmitButton, logger); 

			verifyElementinContainer(ManageNewsObjects.AreaTextContainer,text[1], logger);
			verifyElementinContainer(ManageNewsObjects.StationTextContainer,text[2], logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void SearchNewsbyinputFieldInactive(Map<String, String> data ,ExtentTest logger) throws InterruptedException {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			Thread.sleep(3000);
			String [] text = TestBase.arrayElementText(ManageNewsObjects.InActiveRow, logger);
			Thread.sleep(1000);
			click(ManageNewsObjects.advanceSerachIcon, logger);
			Thread.sleep(1000);    	

			dropDownSelection(ManageNewsObjects.searchAreaDropdown,ManageNewsObjects.dropdownvalue , text[1] ,logger);
			dropDownSelection(ManageNewsObjects.searchStationDropdown,ManageNewsObjects.dropdownvalue , text[2], logger );
			dropDownSelection(ManageNewsObjects.statusAreaDropdown,ManageNewsObjects.dropdownvalue , "Inactive", logger);  	
			click(ManageNewsObjects.SubmitButton, logger); 

			verifyElementinContainer(ManageNewsObjects.AreaTextContainer,text[1], logger);
			verifyElementinContainer(ManageNewsObjects.StationTextContainer,text[2], logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}





	public void SearchNewsbyDate(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			click(ManageNewsObjects.advanceSerachIcon,logger);
			selectCalanderDate(ManageNewsObjects.StartDateField ,ManageNewsObjects.startDatecalanderObject ,data.get("StartDate"),logger );
			selectCalanderDate(ManageNewsObjects.EndDateField ,ManageNewsObjects.EndDatecalanderObject , data.get("EndDate"), logger);
			click(ManageNewsObjects.SubmitButton, logger);  
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortNewsbyArea(ExtentTest logger) throws InterruptedException { 
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Area" , logger);

			verifySortedElementsDecendingOrder(ManageNewsObjects.SortedAreaText, " :is sorted based on area" , logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortNewsbystation(ExtentTest logger) throws InterruptedException { 
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Station" , logger );	
			verifySortedElementsDecendingOrder(ManageNewsObjects.SortedStationText, " :is sorted based on Station", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void ValidateSortNewsbyNewsID(ExtentTest logger) throws InterruptedException { 
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);

			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "News ID", logger );

			verifySortedElementsDecendingOrder(ManageNewsObjects.SortedNewsIDText, " :is sorted based on NewsID",logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortNewsbystartDate(ExtentTest logger) throws InterruptedException { 
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Start Date" , logger);

			verifySortedElementsDecendingOrder(ManageNewsObjects.SortedstartDateText, " :is sorted based on startDate", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortNewsbyEndDate(ExtentTest logger) throws InterruptedException { 
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "End Date" , logger);

			verifySortedElementsDecendingOrder(ManageNewsObjects.SortedEndDateText, " :is sorted based on startDate", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortnewsASSC(ExtentTest logger) {
		try{
			
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "End Date" , logger);
			Thread.sleep(2000);
			click(ManageNewsObjects.assecendingButton, logger);
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedEndDateText, " :is sorted based on EndDate in assecending order " , logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Start Date" , logger);
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedstartDateText, " :is sorted based on Start Date in assecending order " , logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "News ID", logger );
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedNewsIDTextAssc, " :is sorted based on NewsID in assecending order",logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Station" , logger );	
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedStationText, " :is sorted based on Station in assecending order", logger);
			dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Area" , logger);
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedAreaText, " :is sorted based on area in assecending order" , logger);
			/*dropDownSelection(ManageNewsObjects.sortByDropDown,ManageNewsObjects.dropdownvalue , "Status" , logger);
			verifySortedElementsAssendingOrder(ManageNewsObjects.SortedActivenews, " :is sorted based on Status in assecending order" , logger);
	*/	
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	


	public void DeleteNews(ExtentTest logger) throws InterruptedException {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews , logger);
			click(ManageNewsObjects.tabExistingNews, logger);
			click(ManageNewsObjects.deleteIcon, logger);
			click(ManageNewsObjects.YesButton, logger);
			verifyElementDisplayed(ManageNewsObjects.DeleteMessage , "News has been Deleted and removed from the List", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void EditNews(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuManageNews , "is displayed on the page" , logger );
			click(GeneralObjects.MenuManageNews, logger);
			click(ManageNewsObjects.tabExistingNews, logger);
			click(ManageNewsObjects.editIcon, logger);
			selectCalanderDate(ManageNewsObjects.EditEndDateField ,ManageNewsObjects.EditEndDatecalanderObject ,tomoDate , logger );
			ClickSendText(ManageNewsObjects.InputTextField, data.get("Text") , logger );
			click(ManageNewsObjects.SaveButton , logger);
			verifyElementDisplayed(ManageNewsObjects.editNewsMessage , "News has been updated successfully", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}



}