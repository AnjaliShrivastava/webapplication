package com.jal.auto;


import com.jal.pages.GeneralObjects;
import com.jal.pages.InvoiceDataObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class BCInvoiceData extends TestBase {
	
	public void NavigatetoInvoiceData(ExtentTest logger ) {
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuInvoiceData , "is displayed on the page" , logger );
			click(GeneralObjects.MenuInvoiceData , logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	
	public void ClickContractedAgentPDF(ExtentTest logger ) {
				try {
					click(InvoiceDataObjects.clickPDF,logger);
					//verifyElementDisplayed(GeneralObjects.MenuInvoiceData , "is displayed on the page" , logger );
					logger.log(LogStatus.PASS, "User able to click InvoiceData");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click InvoiceData");

				}
	}
	

	public void ClickNonContractedAgentPDF(ExtentTest logger ) {
				try {
					click(InvoiceDataObjects.clickPDF,logger);
					//verifyElementDisplayed(GeneralObjects.MenuInvoiceData , "is displayed on the page" , logger );
					logger.log(LogStatus.PASS, "User able to click InvoiceData");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click InvoiceData");

				}
	}
	

	public void ClickJPSAgentPDF(ExtentTest logger ) {
				try {
					click(InvoiceDataObjects.clickPDF,logger);
					//verifyElementDisplayed(GeneralObjects.MenuInvoiceData , "is displayed on the page" , logger );
					logger.log(LogStatus.PASS, "User able to click InvoiceData");
				} catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage(), "User is unable to click InvoiceData");

				}
	}
	

}    