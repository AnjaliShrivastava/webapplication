package com.jal.auto;

import java.util.Map;

import com.jal.pages.GeneralObjects;
import com.jal.pages.PortalSettingObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCPortalSetting extends TestBase {

	public void navigateToPortalSettings(ExtentTest logger) {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuPortalSettings , "is displayed on the page" , logger );
			click(GeneralObjects.MenuPortalSettings , logger );
			verifyElementDisplayed(PortalSettingObjects.headertitle , "Redirected to Portal Settings Screen", logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void EditPortalSettings(ExtentTest logger) {
		try {
			click(PortalSettingObjects.editIcon,  logger );
			verifyElementDisplayed(GeneralObjects.SaveButton , "Edit Section is appered on the screen", logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void enterDetails(Map<String, String> data , ExtentTest logger) {
		try {

			ClickSendText(PortalSettingObjects.BookingWindow,data.get("BookingWin") , logger);
			ClickSendText(PortalSettingObjects.SmallShipment,data.get("ShipmentWgt") , logger);
			ClickSendText(PortalSettingObjects.RetentionDays,data.get("RetentionDay") , logger);
			ClickSendText(PortalSettingObjects.LD2,data.get("LD2") , logger);
			ClickSendText(PortalSettingObjects.LD3,data.get("LD3") , logger);
			ClickSendText(PortalSettingObjects.Pallet,data.get("Pallet") , logger);
			ClickSendText(PortalSettingObjects.Login,data.get("MaxAttempt") , logger);
			ClickSendText(PortalSettingObjects.ExpireIn,data.get("ExpireDays") , logger);
			ClickSendText(PortalSettingObjects.MinimumLength,data.get("MinLength") , logger);
			ClickSendText(PortalSettingObjects.ExpiryAlert,data.get("ExpiryAlert") , logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}

	public void VerifyTextValues(Map<String, String> data,ExtentTest logger) {
		try {
			verifyElementcontainsText(PortalSettingObjects.BWValue, data.get("BookingWin"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.weightValue,data.get("ShipmentWgt"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.RDValue,data.get("RetentionDay"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.LD2Value,data.get("LD2"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.LD3Value,data.get("LD3"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.PalletValue,data.get("Pallet"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.MAValue,data.get("MaxAttempt"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.EIValue,data.get("ExpireDays"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.MLValue,data.get("MinLength"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.EAValue,data.get("ExpiryAlert"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.AircraftTextField,data.get("AIRCRAFT"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.AircraftSHCTextField,data.get("SHC"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.DeliverySHCTextField,data.get("SHC"), " is displayed in the field" ,logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}
	
	public void VerifyresettedTextValues(Map<String, String> data,ExtentTest logger) {
		try {
			verifyElementcontainsText(PortalSettingObjects.BWValue, data.get("BookingWin"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.weightValue,data.get("ShipmentWgt"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.RDValue,data.get("RetentionDay"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.LD2Value,data.get("LD2"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.LD3Value,data.get("LD3"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.PalletValue,data.get("Pallet"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.MAValue,data.get("MaxAttempt"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.EIValue,data.get("ExpireDays"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.MLValue,data.get("MinLength"), " is displayed in the field" ,logger);
			verifyElementcontainsText(PortalSettingObjects.EAValue,data.get("ExpiryAlert"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.AircraftTextField,data.get("AIRCRAFT"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.AircraftSHCTextField,data.get("SHC"), " is displayed in the field" ,logger);
			//verifyElementcontainsText(PortalSettingObjects.DeliverySHCTextField,data.get("SHC"), " is displayed in the field" ,logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}
	

	public void enterSHCDetails(Map<String, String> data , ExtentTest logger) {
		try {
			ClickSendText(PortalSettingObjects.DeliverySHCTextField,data.get("SHC") , logger);
			click(PortalSettingObjects.DeliveryAtPlusButton , logger );

			//ClickSendText(PortalSettingObjects.AircraftTextField,data.get("AIRCRAFT") , logger);
			//ClickSendText(PortalSettingObjects.AircraftSHCTextField,data.get("SHC") , logger);
			click(PortalSettingObjects.AirCraftPlusButton , logger );

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}
	public void SaveData(ExtentTest logger) {
		try {
			/*JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,-500)");*/
			click(GeneralObjects.SaveButton , logger );
		 verifyElementDisplayed(PortalSettingObjects.SuccessMessage , " Message is dispalyed on the screen", logger);	
			logger.log(LogStatus.PASS, PortalSettingObjects.SuccessMessage);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}
	
	public void resetData(ExtentTest logger) {
		try {
			/*JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,-500)");*/
			click(PortalSettingObjects.resetButton , logger );
			logger.log(LogStatus.PASS, "Data resetted");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}
	
	
	
	public void CancelData(ExtentTest logger) {
		try {
			
			click(PortalSettingObjects.cancelButton , logger );
			verifyElementDisplayed(PortalSettingObjects.editIcon , " Portal settings editing is been canceled", logger);	
			logger.log(LogStatus.PASS, " Portal settings editing is been canceled");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}

	
	public void SaveNegativeData(ExtentTest logger) {
		try {
			
			click(GeneralObjects.SaveButton , logger );
			verifyElementDisplayed(PortalSettingObjects.errorChar , " Portal settings editing is been canceled", logger);	
			logger.log(LogStatus.PASS, " Portal settings editing is been canceled");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());
		}
	}





}