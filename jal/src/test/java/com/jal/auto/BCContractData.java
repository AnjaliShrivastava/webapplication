package com.jal.auto;

import java.util.Map;

import com.jal.pages.ContractDataObjects;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class BCContractData extends TestBase {

		public void NavigatetoContractData(ExtentTest logger ) {
			try{
				click(GeneralObjects.BurgerMenu , logger);
				verifyElementDisplayed(GeneralObjects.MenuContractData , "is displayed on the page" , logger );
				click(GeneralObjects.MenuContractData , logger);
			}catch (Exception e) {
				logger.log(LogStatus.ERROR, e.getMessage());
			}
		}
		
			public void DifferentdateSearchNonEditableShotName(Map<String, String> data, ExtentTest logger) throws InterruptedException {
					try {
						selectCalenderMonth(ContractDataObjects.selectdatecalendar , data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("1"), logger );
						//selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate ,data.get("Year"),data.get("EndDateMonth"),data.get("EndDate"),("2"), logger );
						dropDownSelection(ContractDataObjects.origin,ContractDataObjects.origindrpdown , data.get("Origin"),logger);
						click(ContractDataObjects.btnsubmit , logger);
						//click(ContractDataObjects.btnsearch , logger);
						click(ContractDataObjects.clickremark , logger);
						verifyElementDisplayed(ContractDataObjects.remarktextfield , "is displayed on the page" , logger );
						//click(ContractDataObjects.clickXicon , logger);
						click(ContractDataObjects.downloadexcel , logger);
						
					}catch (Exception e) {
						logger.log(LogStatus.FAIL, e.getMessage());
					}

				}
			public void DifferentdateAdvanceSearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
				try {
					
					click(ContractDataObjects.btnsearch , logger);
					dropDownSelection(ContractDataObjects.origin,ContractDataObjects.origindrpdown , data.get("Origin"),logger);
					click(ContractDataObjects.btnsubmit , logger);
					//verifyElementDisplayed(ContractDataObjects.remarktextfield , "is displayed on the page" , logger );
					click(ContractDataObjects.downloadexcel , logger);
					dropDownSelection(ContractDataObjects.clicksortby,ContractDataObjects.sortbydrpdown ,data.get("SortOrigin"), logger);
					//click(ContractDataObjects.btnsubmit , logger);
					click(ContractDataObjects.downloadexcel , logger);
				}catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage());
				}
}
			
      public void DifferentdateSortByOrgin(Map<String, String> data , ExtentTest logger)throws InterruptedException  {
	     try{
		
		dropDownSelection(ContractDataObjects.clicksortby,ContractDataObjects.sortbydrpdown ,data.get("SortOrigin"), logger);
		System.out.println(TestBase.Gettextvalue("SortOrigin"));
		verifySortedElementsDecendingOrder(ContractDataObjects.origindestinationtext, " :is sorted based on Origin" , logger);
	}catch (Exception e) {
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, e.getMessage());
	}
	     }
      public void DifferentdateSortByDestination(Map<String, String> data , ExtentTest logger)throws InterruptedException  {
 	     try{
 		
 		dropDownSelection(ContractDataObjects.clicksortby,ContractDataObjects.sortbydrpdown ,data.get("SortDestination"), logger);
 		System.out.println(TestBase.Gettextvalue("SortDestination"));
 		verifySortedElementsDecendingOrder(ContractDataObjects.origindestinationtext, " :is sorted based on Destination" , logger);
 	}catch (Exception e) {
 		System.out.println(e.getMessage());
 		logger.log(LogStatus.FAIL, e.getMessage());
 	}
 	     }
        public void DifferentdateSortBy1stFlight(Map<String, String> data , ExtentTest logger)throws InterruptedException  {
  	     try{
  			
  			dropDownSelection(ContractDataObjects.clicksortby,ContractDataObjects.sortbydrpdown ,data.get("Sort1stFlight"), logger);
  			System.out.println(TestBase.Gettextvalue("Sort1stFlight"));
  			verifySortedElementsDecendingOrder(ContractDataObjects.Flighttext, " :is sorted based on 1stFlight" , logger);
  		}catch (Exception e) {
  			System.out.println(e.getMessage());
  			logger.log(LogStatus.FAIL, e.getMessage());
  		}
  		     }
  	      
public void DifferentdateSearchEditableShortName(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try {
		
		
		selectCalenderMonth(ContractDataObjects.selectdatecalendar , data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("1"), logger );
		//selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate ,data.get("Year"),data.get("EndDateMonth"),data.get("EndDate"),("2"), logger );
		
		dropDownSelection(ContractDataObjects.origin,ContractDataObjects.origindrpdown , data.get("Origin"),logger);
		click(ContractDataObjects.btnsubmit , logger);
		
		//click(ContractDataObjects.btnsearch , logger);
		click(ContractDataObjects.clickremark , logger);
		verifyElementDisplayed(ContractDataObjects.remarktextfield , "is displayed on the page" , logger );
		//click(ContractDataObjects.clickXicon , logger);
		click(ContractDataObjects.downloadexcel , logger);
		
		
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}}

public void DifferentdateSearchDropdownShortName(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try {
		
	
		selectCalenderMonth(ContractDataObjects.selectdatecalendar , data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("1"), logger );
		//selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate ,data.get("Year"),data.get("EndDateMonth"),data.get("EndDate"),("2"), logger );
		
		dropDownSelection(ContractDataObjects.origin,ContractDataObjects.origindrpdown , data.get("Origin"),logger);
		click(ContractDataObjects.btnsubmit , logger);
		
		//click(ContractDataObjects.btnsearch , logger);
		click(ContractDataObjects.clickremark , logger);
		verifyElementDisplayed(ContractDataObjects.remarktextfield , "is displayed on the page" , logger );
		//click(ContractDataObjects.clickXicon , logger);
		click(ContractDataObjects.downloadexcel , logger);
		
		
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}

public void NegativeDatesearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try {
		
		
		selectCalanderDate(ContractDataObjects.selectdatecalendar ,ContractDataObjects.calendarviewstartdate,data.get("Date"), logger );
		click(ContractDataObjects.startdateclosebutton , logger);
		dropDownSelection(ContractDataObjects.origin,ContractDataObjects.origindrpdown , data.get("Origin"),logger);
		ClickSendText(ContractDataObjects.inputallotmentcode,data.get("AllotmentCode"), logger);
		click(ContractDataObjects.btnsubmit , logger);
		verifyElementDisplayed(ContractDataObjects.datefielderrormessage , "is displayed on the page" , logger );
		verifyElementDisplayed(ContractDataObjects.originerrormessage , "is displayed on the page" , logger );
		verifyElementDisplayed(ContractDataObjects.Allotmentcodeerrormessage , "is displayed on the page" , logger );
		
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}
}