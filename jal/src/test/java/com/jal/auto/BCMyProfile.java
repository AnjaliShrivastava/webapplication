package com.jal.auto;

import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.jal.pages.GeneralObjects;
import com.jal.pages.MyProfileObjects;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BCMyProfile extends TestBase {
	

	
	// ************** SETPASSWORD ******************* //

	public void Relogin(Map<String, String> data, String pwd ,int num, ExtentTest logger){
		//sample
		try {

			SubmitButton(GeneralObjects.LogginButton, logger);
			ClickSendText(GeneralObjects.portalID,data.get("PortalID")+num, logger);
			//Thread.sleep(2000);
			ClickSendText(GeneralObjects.portalPassword,pwd, logger);
			SubmitButton(GeneralObjects.buttonSignin ,logger );
		//	logger.log(LogStatus.PASS, "User Logged in Successfully");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());	
		}
	}




	public void ForgotPassewordNavigation( ExtentTest logger){
		//sample
		try {

			click(MyProfileObjects.forgotPasswordLink, logger);
			verifyElementDisplayed(MyProfileObjects.forgotPasswordTitle, "is displayed on the page", logger);
			//logger.log(LogStatus.PASS, "User Logged in Successfully");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());	
		}
	}

	public void ForgotPassewordDetails(Map<String, String> data, ExtentTest logger){
		//sample
		try {

			ClickSendText(MyProfileObjects.forgotPasswordPortalID,data.get("UserType"), logger);
			ClickSendText(MyProfileObjects.forgotPasswordName,data.get("ShortName"), logger);
			ClickSendText(MyProfileObjects.forgotPasswordEmail,data.get("Email"), logger);
			ClickSendText(MyProfileObjects.forgotPasswordPhone,data.get("Text"), logger);
			logger.log(LogStatus.PASS, "User details entered on Forgot Password Page");
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());	
		}
	}

	public void SaveForgotPassewordDetails(ExtentTest logger){
		//sample
		try {
			click(MyProfileObjects.forgotPasswordSubmit, logger);
			verifyElementDisplayed(MyProfileObjects.forgotPasswordSuccessMessage, " : message is displayed ", logger);
			click(MyProfileObjects.forgotPasswordBackToLoginButton, logger);
			verifyElementDisplayed(GeneralObjects.portalID , " : User redirected back to login Screen", logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());	
		}
	}


	public void ResetForgotPassewordDetails( ExtentTest logger){
		//sample
		try {
			click(MyProfileObjects.forgotPasswordReset, logger);
			verifyElementcontainsText(MyProfileObjects.forgotPasswordPortalID, "" , "Entered value is resetted",logger);
			verifyElementText(MyProfileObjects.forgotPasswordName, "" , "Entered value is resetted", logger);
			verifyElementText(MyProfileObjects.forgotPasswordEmail, "" , "Entered value is resetted", logger);
			verifyElementText(MyProfileObjects.forgotPasswordPhone, "" , "Entered value is resetted", logger);

		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getLocalizedMessage());	
		}
	}

	public void login(Map<String, String> data, ExtentTest logger){
		//sample
	

			SubmitButton(GeneralObjects.LogginButton, logger);
			ClickSendText(GeneralObjects.portalID,data.get("PortalID"), logger);
			//Thread.sleep(2000);
			ClickSendText(GeneralObjects.portalPassword,data.get("ExistingPwd"), logger);
			SubmitButton(GeneralObjects.buttonSignin ,logger );
			long loginFirstInstance = System.currentTimeMillis();
			verifyElementDisplayedlongTime(GeneralObjects.BurgerMenu, "Menu Icon is displaying", logger);
               long endInstance = System.currentTimeMillis();
                long totalTime4 = endInstance - loginFirstInstance;
                  
			logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken in Login to the Application : " + totalTime4 +" MilliSeconds or "+totalTime4/1000 +" in Seconds");
		
	}

	public void FirstTimelogin_SetPassword1(Map<String, String> data,int n , ExtentTest logger) throws InterruptedException {
		
			if (verifyErroDisplayed(GeneralObjects.firstTimeLogintitleDev)) {
				logger.log(LogStatus.INFO, "User is trying to login first time and forced to reset its password");

				ClickSendText(GeneralObjects.passwordDev, data.get("NewPassword"), logger);
				ClickSendText(GeneralObjects.confirmpasswordDev, data.get("ConfirmPassword"), logger);
				SubmitButton(GeneralObjects.btnSUBMITDev, logger);
				verifyElementDisplayed(GeneralObjects.successmessage, "is displayed on the page", logger);
				click(GeneralObjects.returntologin, logger);
				ClickSendText(GeneralObjects.portalID,data.get("PortalID")+n, logger);
				writeInTextFile(data.get("PortalID")+n );
				Thread.sleep(2000);
				ClickSendText(GeneralObjects.portalPassword,data.get("NewPassword"), logger);
				writeInTextFile(data.get("NewPassword"));
				SubmitButton(GeneralObjects.buttonSignin ,logger );
				logger.log(LogStatus.PASS, "User Logged in Successfully");

		} }
	
	
	public void FirstTimelogin_SetPassword(Map<String, String> data , ExtentTest logger) throws InterruptedException {
	
			if (verifyErroDisplayed(GeneralObjects.firstTimeLogintitleDev)) {
				logger.log(LogStatus.INFO, "User is trying to login first time and forced to reset its password");

				ClickSendText(GeneralObjects.passwordDev, data.get("NewPassword"), logger);
				ClickSendText(GeneralObjects.confirmpasswordDev, data.get("ConfirmPassword"), logger);
				SubmitButton(GeneralObjects.btnSUBMITDev, logger);
				verifyElementDisplayed(GeneralObjects.successmessage, "is displayed on the page", logger);
				click(GeneralObjects.returntologin, logger);
				ClickSendText(GeneralObjects.portalID,data.get("PortalID"), logger);
				Thread.sleep(2000);
				ClickSendText(GeneralObjects.portalPassword,data.get("NewPassword"), logger);
				SubmitButton(GeneralObjects.buttonSignin ,logger );
				logger.log(LogStatus.PASS, "User Logged in Successfully");

			}
		



	}

	// ************** MYPROFILE ******************* //

	public void navigateToMyProfile(Map<String, String> data, ExtentTest logger) {
	
			click(MyProfileObjects.welcomebutton, logger);
			click(MyProfileObjects.myprofile, logger);
			//dropDownSelection(MyProfileObjects.myprofile, MyProfileObjects.myprofile,TestBase.Gettextvalue("myprofile"), logger);
			logger.log(LogStatus.PASS, "User redirect to My profile page");
		
	}

	// **************AGENT PROFILE ******************* //
		public void AgentWithoutPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			

				// click(MyProfileObjects.tabmanage, logger);
				click(MyProfileObjects.editicon, logger);
				click(MyProfileObjects.allbutton, logger);
				click(GeneralObjects.cancelButton, logger);
				verifyElementDisplayed(MyProfileObjects.canceltoastmsg, "is displayed on the page", logger);
				Thread.sleep(4000);
				click(MyProfileObjects.btnchangepassword, logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
			    verifyElementDisplayed(MyProfileObjects.emptyPasswordMessage, "is displayed on the page", logger);
			    click(MyProfileObjects.btncancel, logger);
			

			
		}

		public void AgentChangePassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			
				click(MyProfileObjects.editicon, logger);
				click(MyProfileObjects.limitedbutton, logger);
				ClickSendText(MyProfileObjects.limitedtxtfield, data.get("Text"), logger);
				click(GeneralObjects.SaveButton, logger);
				verifyElementDisplayed(MyProfileObjects.successtoastmsg, "is displayed on the page", logger);
				Thread.sleep(4000);
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordSeccessMessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				Thread.sleep(3000);

			
		}

		public void AgentIncorrectPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			
				
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordIncorrectmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				
			

		}

		public void AgentSecondTimePasswordChange(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			
				click(MyProfileObjects.editicon, logger);
				click(MyProfileObjects.limitedbutton, logger);
				ClickSendText(MyProfileObjects.limitedtxtfield, data.get("Text"), logger);
				click(GeneralObjects.SaveButton, logger);

				verifyElementDisplayed(MyProfileObjects.successtoastmsg, "is displayed on the page", logger);
				Thread.sleep(4000);
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				
			

		}

		// ************** ADMIN PROFILE ******************* //
		public void AdminWithoutPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				// click(MyProfileObjects.tabmanage, logger);
				click(MyProfileObjects.btnchangepassword, logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);

				click(MyProfileObjects.btncancel, logger);
				Thread.sleep(3000);
			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void AdminChangePassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordSeccessMessage, "is displayed on the page", logger);
				
				

			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void AdminIncorrectPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordIncorrectmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				Thread.sleep(3000);
			} catch (Exception e) {
				logger.log(LogStatus.PASS, e.getMessage());
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void AdminSecondTimePasswordChange(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}

		// ************** CONSIGNEE PROFILE******************* //
		public void ConsigneeWithoutPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				// click(MyProfileObjects.tabmanage, logger);
				click(MyProfileObjects.btnchangepassword, logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);

				click(MyProfileObjects.btncancel, logger);
				Thread.sleep(3000);
			} catch (Exception e) {
				logger.log(LogStatus.PASS, e.getMessage());
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void ConsigneeChangePassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordSeccessMessage, "is displayed on the page", logger);
				

			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void ConsigneeIncorrectPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				// click(MyProfileObjects.tabmanage, logger);
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordIncorrectmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				
			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void ConsigneeSecondTimePasswordChange(Map<String, String> data, ExtentTest logger)
				throws InterruptedException {
			try {
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				
			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}

		// ************** STAFF PROFILE******************* //
		public void StaffWithoutPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				// click(MyProfileObjects.tabmanage, logger);
				click(MyProfileObjects.btnchangepassword, logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);

				click(MyProfileObjects.btncancel, logger);
				
			} catch (Exception e) {
			
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}

		public void StaffChangePassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordSeccessMessage, "is displayed on the page", logger);
				

			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void StaffIncorrectPassword(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

			
				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);
				click(MyProfileObjects.buttonprofilesubmit, logger);
				verifyElementDisplayed(MyProfileObjects.passwordIncorrectmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
			} catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}
		}

		public void StaffSecondTimePasswordChange(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {

				click(MyProfileObjects.btnchangepassword, logger);
				ClickSendText(MyProfileObjects.newpassword, data.get("NewPassword"), logger);
				ClickSendText(MyProfileObjects.confirmpassword, data.get("ConfirmPassword"), logger);

				click(MyProfileObjects.buttonprofilesubmit, logger);

				verifyElementDisplayed(MyProfileObjects.passwordwarningmessage, "is displayed on the page", logger);
				click(MyProfileObjects.btncancel, logger);
				
			} catch (Exception e) {
				
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}


}
