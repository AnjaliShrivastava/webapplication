package com.jal.auto;


import java.util.Date;
import java.util.Map;

import com.jal.pages.GeneralObjects;
import com.jal.pages.NewBookingObjects;
import com.jal.pages.ShipmentListObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BCShipmentList extends TestBase {

long st , et ;
	public void NavigatetoShipmentList( ExtentTest logger ) {
		
			click(GeneralObjects.BurgerMenu , logger);
			//verifyElementDisplayed(GeneralObjects.ShipmentList , "is displayed on the page" , logger );
		//click("Anjali" , logger);
			click(GeneralObjects.ShipmentList , logger);
			st = System.currentTimeMillis();
			verifyElementDisplayed(ShipmentListObjects.tabShipmentList , "Shipment list screen is displaying" , logger);
			et =System.currentTimeMillis();
			logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken for redirecting to shipment list screen : " + (et-st) + " MilliSeconds or "+(et-st)/1000 +" in Seconds");
		
		
	}
	public void ShipmentListTab(ExtentTest logger ) {
				click(ShipmentListObjects.tabShipmentList , logger);
		
			
		}
	

	public void EyeIcon(ExtentTest logger ) throws InterruptedException {
		
			click(ShipmentListObjects.viewBookingIcon , logger);
			verifyElementSelected(ShipmentListObjects.tabNewBooking ,"is selected and User redirected to New booking page" , logger);
		
			
		}
	

	public void EditIcon(ExtentTest logger ) throws InterruptedException {
		
			click(ShipmentListObjects.editBookingIcon , logger);
			verifyElementSelected(ShipmentListObjects.tabNewBooking ,"is selected and User redirected to New booking page" , logger);
		
			
		}
	
	public void TrackAndTrace(ExtentTest logger ) {
		
			click(ShipmentListObjects.track , logger);
			//verifyElementDisplayed(ShipmentListObjects.headertitleTrack , "User redirected to Track AWB page and Track AWB and Mail title is displayed" , logger);
		
			
		}
	




	public void Search(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		
			
			String AWB = TestBase.getElementText(ShipmentListObjects.AWBtext , logger);
			System.out.println(AWB);
			if (AWB.isEmpty()) {
				Date d = new Date();
				@SuppressWarnings("deprecation")
				int tdat =d.getDate();
				for (int i = 1; i < 3; i++) {
					if(tdat<=28 && tdat>=1  ) {	
						tdat = tdat+i ;
						String serachDate = Integer.toString(tdat);
						selectCalanderDate(ShipmentListObjects.calanderIcon ,ShipmentListObjects.MaincalanderObject ,serachDate, logger );
						AWB = TestBase.getElementText(ShipmentListObjects.AWBtext , logger);
						if(!AWB.isEmpty())
							break;
					}else if(tdat>=28) {
						tdat = tdat-i ;
						String serachDate = Integer.toString(tdat);
						selectCalanderDate(ShipmentListObjects.calanderIcon ,ShipmentListObjects.MaincalanderObject ,serachDate, logger );
						AWB = TestBase.getElementText(ShipmentListObjects.AWBtext , logger);
						if(!AWB.isEmpty())
							break;
					}				
				}				
			}		
			SubmitButton(ShipmentListObjects.btnSearch , logger);		
			ClickSendText(ShipmentListObjects.txtAWBprefix,AWB.substring(0, 3), logger );
			ClickSendText(ShipmentListObjects.txtAWBserial,AWB.substring(4, 12) , logger);
			SubmitButton(GeneralObjects.btnSubmit, logger);
			verifyElementinContainer(ShipmentListObjects.SortedAWBText,AWB.substring(0, 3)+"-"+AWB.substring(4, 12), logger); 

		
	}

	public void SearchClose(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		try {
			SubmitButton(ShipmentListObjects.btnSearch , logger);		
			ClickSendText(ShipmentListObjects.txtAWBprefix,data.get("AWBPrefix"), logger );
			ClickSendText(ShipmentListObjects.txtAWBserial,data.get("AWBSerial") , logger);
			SubmitButton(ShipmentListObjects.closeBtn, logger);
			Thread.sleep(1000);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}


	public void InvalidSearch(Map<String, String> data , ExtentTest logger ) throws InterruptedException {

		SubmitButton(ShipmentListObjects.btnSearch , logger);		
		ClickSendText(ShipmentListObjects.txtAWBprefix,data.get("AWBPrefix"), logger );
		ClickSendText(ShipmentListObjects.txtAWBserial,data.get("AWBSerial") , logger);
		SubmitButton(GeneralObjects.btnSubmit, logger);
		verifyElementDisplayed(ShipmentListObjects.invalidAWBMessage , "is displayed on the page" , logger );

	}

	public void sortByAWB(Map<String, String> data , ExtentTest logger)  {
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("awb"), logger);
			System.out.println(TestBase.Gettextvalue("awb"));
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "AWB" );
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedAWBText, " :is sorted based on AWB" , logger);

		
	}

	public void sortByAWBAss(Map<String, String> data , ExtentTest logger)  {
		try {
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("awb"), logger);
			System.out.println(TestBase.Gettextvalue("awb"));
			Thread.sleep(2000);
			click(ShipmentListObjects.assecding, logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedAWBText, " :is sorted based on AWB in assecending order " , logger);
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("flight"), logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedFlightText, " :is sorted based on Flight in assecending order " , logger);
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("depart"), logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedDepartText, " :is sorted based on Depart in assecending order " , logger);
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("alloccode"), logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedAllocCodeText, " :is sorted based on Alloc code in assecending order " , logger);
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("alocrlsetime"), logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedAllocReleaseText, " :is sorted based on Alloc release time in assecending order " , logger);
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("bookingclse"), logger);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedBookingCloseText2, " :is sorted based on Booking Close Time in assecending order " , logger);
			
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("bup"), logger);
			Thread.sleep(3000);
			verifySortedElementsAssendingOrder(ShipmentListObjects.SortedBUPText, " :is sorted based on Flight in assecending order " , logger);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void sortByFlight(Map<String, String> data , ExtentTest logger) {
		
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "Flight" );
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("flight"), logger);
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedFlightText, " :is sorted based on Flight" , logger);
		
	}

	public void sortByDepart(Map<String, String> data , ExtentTest logger) {
		
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "Depart" );
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("depart"), logger );
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedDepartText, " :is sorted based on Depart" , logger);
		}

	public void sortByAllocCode(Map<String, String> data , ExtentTest logger) {
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("alloccode"), logger );
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "Alloc Code" );
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedAllocCodeText, " :is sorted based on Alloc Code" , logger);
		
	}

	public void sortByAllocReleaseTime(Map<String, String> data , ExtentTest logger) {
		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("alocrlsetime") , logger);
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "Alloc Release Time" );
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedAllocReleaseText, " :is sorted based on Alloc Release Time" , logger);
		}

	public void sortByBookingCloseTime(Map<String, String> data , ExtentTest logger) {
	
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "Booking Close" );
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("bookingclse"), logger);
			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedBookingCloseText2, " :is sorted based on Booking Close Time" ,logger);
		
	}





	public void sortByBUP(Map<String, String> data , ExtentTest logger) {

		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("bup"), logger);
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "BUP");

			verifySortedElementsDecendingOrder(ShipmentListObjects.SortedBUPText, " :is sorted based on BUP" , logger);
		
	}
	public void sortByorigin(Map<String, String> data , ExtentTest logger) {

		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("origin"), logger);
			verifySortedElementsDecendingOrderWithmultipleText(ShipmentListObjects.SortedOriginDes , ": sorted based on Origin", 0 , logger);
		
	}
	public void sortByDestination(Map<String, String> data , ExtentTest logger) {

		
			dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , TestBase.Gettextvalue("destination"), logger);
			//dropDownSelection(ShipmentListObjects.SortByDropdown,ShipmentListObjects.dropdownvalue , "BUP");

			verifySortedElementsDecendingOrderWithmultipleText(ShipmentListObjects.SortedOriginDes , ": sorted based on Destination", 0 , logger);
		} 



	








	public void calanderSearch(Map<String, String> data , ExtentTest logger) throws InterruptedException {		
		
			/*Date d = new Date();
			int tdat =d.getDate()+7;*/
			//String serachDate = Integer.toString(tdat);
			selectCalanderDate(ShipmentListObjects.calanderIcon ,ShipmentListObjects.MaincalanderObject ,data.get("SearchDate"), logger );
			//Thread.sleep(1000);
		
	}

	public void advanceSearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		
			SubmitButton(ShipmentListObjects.btnSearch , logger);
			SubmitButton(ShipmentListObjects.advanchSearchIcon, logger);
			dropDownSelection(ShipmentListObjects.originDropdown,ShipmentListObjects.dropdownvalue , data.get("Origin") ,logger );
			dropDownSelection(ShipmentListObjects.destinationDropdown,ShipmentListObjects.dropdownvalue , data.get("Destination"), logger );

			selectCalanderDate(ShipmentListObjects.searchCalanderIcon ,ShipmentListObjects.searchcalanderObject ,data.get("SearchDate") , logger );
			//ClickSendText(ShipmentListObjects.flightNumberInput,data.get("SerialNo"));
			dropDownSelection(ShipmentListObjects.depTimeDropdown,ShipmentListObjects.dropdownvalue , data.get("DepTime"), logger);
			dropDownSelection(ShipmentListObjects.bookingTypeDropdown,ShipmentListObjects.dropdownvalue , data.get("BookingType"), logger);
			dropDownSelection(ShipmentListObjects.bookingBookingStatus,ShipmentListObjects.dropdownvalue , data.get("Bookingstatus"), logger);
			//click(ShipmentListObjects.departedShipmentCheckbox);
			click(GeneralObjects.btnSubmit , logger);	
			verifyElementinContainer(ShipmentListObjects.SortedAllocCodeText,"CA" ,logger);
		

	}



}







