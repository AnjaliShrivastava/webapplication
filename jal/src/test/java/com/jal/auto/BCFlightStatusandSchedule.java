package com.jal.auto;

import java.util.Map;

import com.jal.pages.FlightStatusandScheduleObjects;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCFlightStatusandSchedule extends TestBase {
	public void NavigatetoFlightstatusandSchedule(ExtentTest logger ) {
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.Menuflightstatusandschedule , "is displayed on the page" , logger );
			click(GeneralObjects.Menuflightstatusandschedule , logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	
	public void FlightStatusToday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
	             ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             String value = GetAttributeValue("//object[1]", "data");	     
	            
	             Thread.sleep(4000);
	             if(value.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) {
	            	 
	            	 logger.log(LogStatus.PASS, " Today's Flight status is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Today's Flight status does not display on the screen");
	            	 
	             }
	             
	           
	             click(FlightStatusandScheduleObjects.btnsearch , logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
	             ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	                
	         
                  if(value.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) {
	            	 
	            	 logger.log(LogStatus.PASS, " Today's Flight status is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Today's Flight status does not display on the screen");
	            	 
	             }
	             
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	public void FlightStatusYesterday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
	             ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             String YESValue1 = GetAttributeValue("//object[1]", "data");	     
	             System.out.println(YESValue1);
                  if((YESValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && YESValue1.contains("dateAttribute=1")	 ) {
	            	 logger.log(LogStatus.PASS, " Yesterday's Flight status is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Yesterday's Flight status does not display on the screen");
	            	 
	             }
	             click(FlightStatusandScheduleObjects.btnsearch , logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
	             ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             if((YESValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && YESValue1.contains("dateAttribute=1")	 ) {
	            	 logger.log(LogStatus.PASS, " Yesterday's Flight status is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Yesterday's Flight status does not display on the screen");
	            	 
	             }
	             
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void FlightStatusTomorrow(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

            dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
            dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
            ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
            dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

            click(FlightStatusandScheduleObjects.btnsubmit , logger);
            Thread.sleep(4000);
            String TOMValue1 = GetAttributeValue("//object[1]", "data");	     
            System.out.println(TOMValue1);
             if((TOMValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && TOMValue1.contains("dateAttribute=2")	 ) {
           	 logger.log(LogStatus.PASS, " Tomorrow's Flight status is displayed on the screen");	            	 
            }else {
           	 logger.log(LogStatus.FAIL, " Tomorrow's Flight status does not display on the screen");
           	 
            }
            click(FlightStatusandScheduleObjects.btnsearch , logger);
            dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
            dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
            ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
            dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

            click(FlightStatusandScheduleObjects.btnsubmit , logger);
            Thread.sleep(4000);
            if((TOMValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && TOMValue1.contains("dateAttribute=2")	 ) {
              	 logger.log(LogStatus.PASS, " Tomorrow's Flight status is displayed on the screen");	            	 
               }else {
              	 logger.log(LogStatus.FAIL, " Tomorrow's Flight status does not display on the screen");
              	 
               }
            
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
	}
	public void FlightStatusNegativescenario(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

            dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
            dropDownSelection(FlightStatusandScheduleObjects.headerairline,FlightStatusandScheduleObjects.drpdownselectAirlines , data.get("Airline"),logger);
            ClickSendText(FlightStatusandScheduleObjects.inputflightnumber,data.get("FlightNumber"), logger);
            dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

            click(FlightStatusandScheduleObjects.btnsubmit , logger);
            verifyElementDisplayed(FlightStatusandScheduleObjects.statusflightfielderrormessage , "is displayed on the page" , logger );
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
		}
	
	
	
	public void FlightScheduleToday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             
	             String TODValue1 = GetAttributeValue("//object[1]", "data");	     
	             System.out.println(TODValue1);
	              if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG="))){
	            	 logger.log(LogStatus.PASS, " Today's Flight schedule is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Today's Flight schedule does not display on the screen");
	            	 
	             }
	             
	             
	             click(FlightStatusandScheduleObjects.btnsearch , logger);

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG="))){
	            	 logger.log(LogStatus.PASS, " Today's Flight schedule is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Today's Flight schedule does not display on the screen");
	            	 
	             }
	             
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void FlightScheduleYesterday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             String TODValue1 = GetAttributeValue("//object[1]", "data");	     
	             System.out.println(TODValue1);
	              if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=1"))){
	            	 logger.log(LogStatus.PASS, " Yesterday's Flight schedule is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Yesterday's Flight schedule does not display on the screen");
	            	 
	             }
	             click(FlightStatusandScheduleObjects.btnsearch , logger);

	             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
	             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
	
	             click(FlightStatusandScheduleObjects.btnsubmit , logger);
	             Thread.sleep(4000);
	             if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=1"))){
	            	 logger.log(LogStatus.PASS, " Yesterday's Flight schedule is displayed on the screen");	            	 
	             }else {
	            	 logger.log(LogStatus.FAIL, " Yesterday's Flight schedule does not display on the screen");
	            	 
	             }
	             
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
		}
		
		public void FlightScheduleTomorrow(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try{

		             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
		
		             click(FlightStatusandScheduleObjects.btnsubmit , logger);
		             Thread.sleep(4000);
		             String TODValue1 = GetAttributeValue("//object[1]", "data");	     
		             System.out.println(TODValue1);
		              if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=2"))){
		            	 logger.log(LogStatus.PASS, " Tomorrow's Flight schedule is displayed on the screen");	            	 
		             }else {
		            	 logger.log(LogStatus.FAIL, " Tomorrow's Flight schedule does not display on the screen");
		            	 
		             }
		             click(FlightStatusandScheduleObjects.btnsearch , logger);

		             dropDownSelection(FlightStatusandScheduleObjects.headerflight,FlightStatusandScheduleObjects.drpdownselectflight , data.get("Flight"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.departureairport,FlightStatusandScheduleObjects.drpdowndepartureairport , data.get("DepartureAirport"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.arrivalairport,FlightStatusandScheduleObjects.drpdownarrivalairport , data.get("ArrivalAirport"),logger);
		             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);
		
		             click(FlightStatusandScheduleObjects.btnsubmit , logger);
		             Thread.sleep(4000);
		             if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=2"))){
		            	 logger.log(LogStatus.PASS, " Tomorrow's Flight schedule is displayed on the screen");	            	 
		             }else {
		            	 logger.log(LogStatus.FAIL, " Tomorrow's Flight schedule does not display on the screen");
		            	 
		             }
		             
			}catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}
	
		}
	

//******************* Dashboard ***************//

public void DashboardFlightStatusToday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

             dropDownSelection(FlightStatusandScheduleObjects.Dashboardairline,FlightStatusandScheduleObjects.DashboarddrpdownselectAirlines , data.get("Airline"),logger);
            
             ClickSendText(FlightStatusandScheduleObjects.Dashboardinputflightnumber,data.get("FlightNumber"), logger);
             dropDownSelection(FlightStatusandScheduleObjects.Dashboardstatusdeparturedate,FlightStatusandScheduleObjects.Dashboardstatusdrpdowndeparturedate , data.get("Departuredate"),logger);

             click(FlightStatusandScheduleObjects.Dashboardstatusbtnsearch , logger);
             elementAvailbility(FlightStatusandScheduleObjects.Dashboardpopupmessage, FlightStatusandScheduleObjects.Yesbutton, logger);
             Thread.sleep(4000);
             click(FlightStatusandScheduleObjects.btnsearch , logger);
             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

             click(FlightStatusandScheduleObjects.btnsubmit , logger);
             Thread.sleep(4000);
             String value = GetAttributeValue("//object[1]", "data");	     
	         if(value.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) {
            	 
            	 logger.log(LogStatus.PASS, " Today's Flight status is displayed on the screen");	            	 
             }else {
            	 logger.log(LogStatus.FAIL, " Today's Flight status does not display on the screen");
            	 
             }
            
             
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}

public void DashboardFlightStatusYesterday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

		 dropDownSelection(FlightStatusandScheduleObjects.Dashboardairline,FlightStatusandScheduleObjects.DashboarddrpdownselectAirlines , data.get("Airline"),logger);
         
         ClickSendText(FlightStatusandScheduleObjects.Dashboardinputflightnumber,data.get("FlightNumber"), logger);
         dropDownSelection(FlightStatusandScheduleObjects.Dashboardstatusdeparturedate,FlightStatusandScheduleObjects.Dashboardstatusdrpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.Dashboardstatusbtnsearch , logger);
         elementAvailbility(FlightStatusandScheduleObjects.Dashboardpopupmessage, FlightStatusandScheduleObjects.Yesbutton, logger);
         Thread.sleep(4000);
         click(FlightStatusandScheduleObjects.btnsearch , logger);
         dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.btnsubmit , logger);
         Thread.sleep(4000);
         String YESValue1 = GetAttributeValue("//object[1]", "data");	     
         System.out.println(YESValue1);
          if((YESValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && YESValue1.contains("dateAttribute=1")	 ) {
        	 logger.log(LogStatus.PASS, " Yesterday's Flight status is displayed on the screen");	            	 
         }else {
        	 logger.log(LogStatus.FAIL, " Yesterday's Flight status does not display on the screen");
        	 
         }
        
             
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}
public void DashboardFlightStatusTomorrow(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

         dropDownSelection(FlightStatusandScheduleObjects.Dashboardairline,FlightStatusandScheduleObjects.DashboarddrpdownselectAirlines , data.get("Airline"),logger);
         
         ClickSendText(FlightStatusandScheduleObjects.Dashboardinputflightnumber,data.get("FlightNumber"), logger);
         dropDownSelection(FlightStatusandScheduleObjects.Dashboardstatusdeparturedate,FlightStatusandScheduleObjects.Dashboardstatusdrpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.Dashboardstatusbtnsearch, logger);
         elementAvailbility(FlightStatusandScheduleObjects.Dashboardpopupmessage, FlightStatusandScheduleObjects.Yesbutton, logger);
         Thread.sleep(4000);
         click(FlightStatusandScheduleObjects.btnsearch , logger);
         dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.btnsubmit , logger);
         Thread.sleep(4000);
         String TOMValue1 = GetAttributeValue("//object[1]", "data");	     
         System.out.println(TOMValue1);
          if((TOMValue1.contains("airlineCode="+data.get("Airline")+"&flightSerNo="+data.get("FlightNumber")) ) && TOMValue1.contains("dateAttribute=2")	 ) {
        	 logger.log(LogStatus.PASS, " Tomorrow's Flight status is displayed on the screen");	            	 
         }else {
        	 logger.log(LogStatus.FAIL, " Tomorrow's Flight status does not display on the screen");
        	 
         }
        
        
}catch (Exception e) {
	logger.log(LogStatus.FAIL, e.getMessage());
}
}
public void DashboardFlightStatusNegativescenario(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

		dropDownSelection(FlightStatusandScheduleObjects.Dashboardairline,FlightStatusandScheduleObjects.DashboarddrpdownselectAirlines , data.get("Airline"),logger);
        
        ClickSendText(FlightStatusandScheduleObjects.Dashboardinputflightnumber,data.get("FlightNumber"), logger);
        dropDownSelection(FlightStatusandScheduleObjects.Dashboardstatusdeparturedate,FlightStatusandScheduleObjects.Dashboardstatusdrpdowndeparturedate , data.get("Departuredate"),logger);

        click(FlightStatusandScheduleObjects.Dashboardstatusbtnsearch, logger);
        elementAvailbility(FlightStatusandScheduleObjects.Dashboardpopupmessage, FlightStatusandScheduleObjects.Yesbutton, logger);
        
        verifyElementDisplayed(FlightStatusandScheduleObjects.Dashboardstatusflightfielderrormessage , "is displayed on the page" , logger );
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
	}


public void DashboardFlightScheduleToday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

             dropDownSelection(FlightStatusandScheduleObjects.Dashboarddepartureairport,FlightStatusandScheduleObjects.Dashboarddrpdowndepartureairport , data.get("DepartureAirport"),logger);
             dropDownSelection(FlightStatusandScheduleObjects.Dashboardarrivalairport,FlightStatusandScheduleObjects.Dashboarddrpdownarrivalairport , data.get("ArrivalAirport"),logger);
             dropDownSelection(FlightStatusandScheduleObjects.Dasboarddeparturedate,FlightStatusandScheduleObjects.Dashboarddrpdowndeparturedate , data.get("Departuredate"),logger);

             click(FlightStatusandScheduleObjects.Dashboardbtnsearch , logger);
             Thread.sleep(4000);
             click(FlightStatusandScheduleObjects.btnsearch , logger);
             dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

             click(FlightStatusandScheduleObjects.btnsubmit , logger);
             Thread.sleep(4000);
             
             String TODValue1 = GetAttributeValue("//object[1]", "data");	     
             System.out.println(TODValue1);
              if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG="))){
            	 logger.log(LogStatus.PASS, " Today's Flight schedule is displayed on the screen");	            	 
             }else {
            	 logger.log(LogStatus.FAIL, " Today's Flight schedule does not display on the screen");
            	 
             }
          
             
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}
public void DashboardFlightScheduleYesterday(Map<String, String> data, ExtentTest logger) throws InterruptedException {
	try{

		 dropDownSelection(FlightStatusandScheduleObjects.Dashboarddepartureairport,FlightStatusandScheduleObjects.Dashboarddrpdowndepartureairport , data.get("DepartureAirport"),logger);
         dropDownSelection(FlightStatusandScheduleObjects.Dashboardarrivalairport,FlightStatusandScheduleObjects.Dashboarddrpdownarrivalairport , data.get("ArrivalAirport"),logger);
         dropDownSelection(FlightStatusandScheduleObjects.Dasboarddeparturedate,FlightStatusandScheduleObjects.Dashboarddrpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.Dashboardbtnsearch , logger);
         Thread.sleep(4000);
         click(FlightStatusandScheduleObjects.btnsearch , logger);
         dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

         click(FlightStatusandScheduleObjects.btnsubmit , logger);
         Thread.sleep(4000);
         String TODValue1 = GetAttributeValue("//object[1]", "data");	     
         System.out.println(TODValue1);
          if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=1"))){
        	 logger.log(LogStatus.PASS, " Yesterday's Flight schedule is displayed on the screen");	            	 
         }else {
        	 logger.log(LogStatus.FAIL, " Yesterday's Flight schedule does not display on the screen");
         }
         
             
	}catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage());
	}
	}
	
	public void DashboardFlightScheduleTomorrow(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

			 dropDownSelection(FlightStatusandScheduleObjects.Dashboarddepartureairport,FlightStatusandScheduleObjects.Dashboarddrpdowndepartureairport , data.get("DepartureAirport"),logger);
	         dropDownSelection(FlightStatusandScheduleObjects.Dashboardarrivalairport,FlightStatusandScheduleObjects.Dashboarddrpdownarrivalairport , data.get("ArrivalAirport"),logger);
	         dropDownSelection(FlightStatusandScheduleObjects.Dasboarddeparturedate,FlightStatusandScheduleObjects.Dashboarddrpdowndeparturedate , data.get("Departuredate"),logger);

	         click(FlightStatusandScheduleObjects.Dashboardbtnsearch , logger);
	         Thread.sleep(4000);
	         click(FlightStatusandScheduleObjects.btnsearch , logger);
	         dropDownSelection(FlightStatusandScheduleObjects.departuredate,FlightStatusandScheduleObjects.drpdowndeparturedate , data.get("Departuredate"),logger);

	         click(FlightStatusandScheduleObjects.btnsubmit , logger);
	         Thread.sleep(4000);
             String TODValue1 = GetAttributeValue("//object[1]", "data");	     
             System.out.println(TODValue1);
              if((TODValue1.contains("DPORT="+data.get("DepartureAirport")+"&APORT="+data.get("ArrivalAirport")+"&DATEFLG=2"))){
            	 logger.log(LogStatus.PASS, " Tomorrow's Flight schedule is displayed on the screen");	            	 
             }else {
            	 logger.log(LogStatus.FAIL, " Tomorrow's Flight schedule does not display on the screen");
            	 
             }
	        
	             
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void DashboardScheduleNegativeScenario(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try{

			 dropDownSelection(FlightStatusandScheduleObjects.Dashboarddepartureairport,FlightStatusandScheduleObjects.Dashboarddrpdowndepartureairport , data.get("DepartureAirport"),logger);
	         dropDownSelection(FlightStatusandScheduleObjects.Dashboardarrivalairport,FlightStatusandScheduleObjects.Dashboarddrpdownarrivalairport , data.get("ArrivalAirport"),logger);
	         dropDownSelection(FlightStatusandScheduleObjects.Dasboarddeparturedate,FlightStatusandScheduleObjects.Dashboarddrpdowndeparturedate , data.get("Departuredate"),logger);

	         click(FlightStatusandScheduleObjects.Dashboardbtnsearch , logger);
	         verifyElementDisplayed(FlightStatusandScheduleObjects.Dashboarddepartureairporterrormessage , "is displayed on the page" , logger );
	         verifyElementDisplayed(FlightStatusandScheduleObjects.Dashboardarrivalairporterrormessage , "is displayed on the page" , logger );
	        
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	}

