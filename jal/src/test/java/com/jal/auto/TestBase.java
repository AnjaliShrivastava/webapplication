package com.jal.auto;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Keys;


import com.jal.pages.GeneralObjects;


import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ISuite;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

//import jxl.Workbook;

public class TestBase {
	protected String usedDeviceName;
	protected String testname;
	public static Map<String, String> data = null;
	protected Throwable exception = null;
	public static RemoteWebDriver driverinstance;
	public static DesiredCapabilities dc;
	public int const_waitlow = 20;
	protected int const_waitmed = 60;
	protected int const_waithigh = 60;
	public static ExtentReports extent;
	public String testCaseName ;	
	public static ExtentTest logger ;
	public static  ThreadLocal <WebDriver> driver ;
	public WebDriver instances ;
	public static ThreadLocal<ExtentTest> testReport ;



	// ****************** WEBDRIVER Methods *************************** //

	public   void LaunchBrowser(Map<String, String> data) throws MalformedURLException, InterruptedException {
		try {
			driver = new ThreadLocal<WebDriver>();

			// switch (getProperty("browser.name").toUpperCase()) {
			switch (data.get("Browser").toUpperCase()) {
			case("CHROME"):
				//System.setProperty("webdriver.chrome.driver", "/Drivers/chromedriver.exe");
				//driver = new ChromeDriver();
				// For Japenese
				if(data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--lang=ja");
					driverinstance = new ChromeDriver(options);
					driver.set(driverinstance);
					dc = new DesiredCapabilities();
				}else {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					driverinstance = new ChromeDriver();
					driver.set(driverinstance);
				}

			break;

			case("FIREFOX"):
				System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");

			if(data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				FirefoxProfile profile = new FirefoxProfile();
				firefoxOptions.setCapability("marionette", true);
				profile.setPreference("intl.accept_languages","ja");
				firefoxOptions.setProfile(profile);
				driverinstance = new FirefoxDriver(firefoxOptions);			
			}else {	
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.setCapability("marionette", true);		
				driverinstance = new FirefoxDriver(firefoxOptions);	
			}
			break;

			case("EDGE"):
				System.setProperty("webdriver.edge.driver", "./Drivers/MicrosoftWebDriver.exe");

			driverinstance = new EdgeDriver();
			driverinstance.get(getProperty("URL"));
			/*
			 * dc = new DesiredCapabilities();
			 * dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			 * dc.setCapability(InternetExplorerDriver.
			 * INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			 * dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			 * dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			 */
			break;

			// case("ANDROIDCHROME"):
			// DesiredCapabilities capability = DesiredCapabilities.android();
			// capability.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			// capability.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			// capability.setCapability(MobileCapabilityType.DEVICE_NAME, "VIVO");
			// capability.setCapability("appPackage", "com.android.chrome");
			// capability.setCapability("appActivity",
			// "com.google.android.apps.chrome.Main");
			// driver = new RemoteWebDriver(new URL("http://localhost:4723/wd/hub"),
			// capability);
			// break;
			}
		}catch (Exception e) {

		}


	}


	
	
	
	public   void LaunchBrowserParallely(Map<String, String> data) throws MalformedURLException, InterruptedException {
		try {
			driver = new ThreadLocal<WebDriver>();

			// switch (getProperty("browser.name").toUpperCase()) {
			switch (data.get("Browser").toUpperCase()) {
			case("CHROME"):
				//System.setProperty("webdriver.chrome.driver", "/Drivers/chromedriver.exe");
				//driver = new ChromeDriver();
				// For Japenese
				if(data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--lang=ja");
					driverinstance = new ChromeDriver(options);
					driver.set(driverinstance);
					
				}else {
					System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				/*	driverinstance = new ChromeDriver();
					driver.set(driverinstance);*/
					URL url = new URL ("http://172.25.79.71:4444/wd/hub");
					dc = new DesiredCapabilities();
					dc.setBrowserName("Chrome");
					dc.setPlatform(Platform.WINDOWS);
					driverinstance = new RemoteWebDriver(url,dc);
					driver.set(driverinstance);
					System.out.println("This is grid exceution");
				}

			} }catch (Exception e) {
				System.out.println(e.getMessage()) ;
			}
	}

	
	
	
	
	
	
	
	
	@BeforeSuite
	public void setUp() {

		System.out.println(getClass().getName().substring(14));
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
		String strDate = formatter.format(date);
		extent = new ExtentReports("./reports/report" + strDate + ".html", false);

		// extent.loadConfig(new File("/jal/ConfigFile.xml"));

	}






	@BeforeMethod
	public  void startReport(Method method  ){

		try {
			testname = method.getName();			
			getTestData();			

			if(data.get("Execute").equalsIgnoreCase("yes")){
				LaunchBrowser(data);
			}

			driver.get().manage().window().maximize();
			driver.get().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			//driver.manage().deleteAllCookies();
			//driver.get("WWW.facebook.com");
			driver.get().navigate().to(getProperty("URL"));
			driver.get().manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

			//testReport = new ThreadLocal<ExtentTest>();

			/*testReport.set(logger);
			testReport.get();*/
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}


	}


	@AfterMethod
	public void tearDown(Method method) {
		try {
			if(data.get("Execute").equalsIgnoreCase("yes")){

				driver.get().close();	
				extent.endTest(logger);
				extent.flush();
				//extent.close();
			}} catch (WebDriverException e) {

			} catch (Exception e) {

			}
	}

	@AfterTest
	public void tearDown2() {


	}

	public long takeSnap() {
		/*Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyyhh:mm:ss");*/
		//String strDate = formatter.format(date);

		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L;
		try {
			FileUtils.copyFile(((TakesScreenshot) driver.get()).getScreenshotAs(OutputType.FILE),
					new File("./reports/images/" + number + ".jpg"));
		} catch (WebDriverException e) {

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return number;
	}

	public List<String> GetResponse(String urlvalue, String jsonxpath) throws Exception {
		String urlPath = getProperty("apiurl") + urlvalue;
		String headervalue = getProperty("apitoken");
		System.out.println(urlPath);
		String jsonData;
		List<String> values = null;
		URL url = new URL(urlPath);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("accept", "application/json");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", headervalue);

		try {
			System.out.println(con.getResponseCode());
			if (con.getResponseCode() == 200) {

				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
				jsonData = "";

				if ((jsonData = reader.readLine()) != null) {
					System.out.println(jsonData);
				}

				// Get the specific values from the json
				DocumentContext jsonContext = JsonPath.parse(jsonData);
				values = jsonContext.read(jsonxpath);
				values = new ArrayList<String>(new HashSet<String>(values));
				// values = new ArrayList<String>(values);
				System.out.println("values size = " + values.size());
			} else {
				// client.report("API Json details not extracted", false);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return values;
	}
	public static  String getJSONPath(Map<String, String> data) {
		String JSONFilePath = "" ;
		if(data.get("JapeneseVersion").equalsIgnoreCase("Yes")) {
			JSONFilePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepathJapense");
		}else {
			JSONFilePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepathEnglish");
		}
		return JSONFilePath;

	}



	public static String Gettextvalue(String jsonheader) {
		String JSONValue ="";

		try {	
			String UTF8Str = "" ;
			String s;
			//String  filePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepath");

			File file = new File(getJSONPath(data));
			BufferedReader in = new BufferedReader(new FileReader(file));


			while( (s = in.readLine()) != null) {

				if(new String(s.getBytes(),"UTF-8") != null)
					UTF8Str = UTF8Str +
					(new String(s.getBytes(),"UTF-8"));
			}
			JSONParser japaneseParser = new JSONParser();
			JSONObject obj = (JSONObject)japaneseParser.parse(UTF8Str);
			JSONObject jObject = new JSONObject(obj);
			JSONValue = (String) jObject.get(jsonheader); 

			in.close();
		}catch (IOException e) {
			System.out.println(e.getMessage());

		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return JSONValue;		



	}
	/*public static String Gettextvalue(String jsonheader) {
		String filePath = getProperty("user.dir") + "\\"+getProperty("jsonfilepath");
		String jsonvalue = null;
		// Get the specific values from the json language file
		JSONParser parser = new JSONParser();

		try {
			File fileDir = new File(filePath);
			BufferedReader in = new BufferedReader(
					   new InputStreamReader(
			                      new FileInputStream(fileDir), "UTF8"));
			Object obj = parser.parse(in);



			JSONObject jsonObject =  (JSONObject) obj;

			jsonvalue = (String) jsonObject.get(jsonheader);
			if (jsonvalue.isEmpty()) {
				System.out.println("json value not retrieved");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		System.out.println(jsonvalue);

		return jsonvalue;

	}
	 */


	public void getTestData() throws IOException {
		int z;
		data = new HashMap<String, String>();
		Row row;
		FileInputStream fileInputStream = new FileInputStream(getProperty("data.spreadsheet.name"));
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		Sheet dataSheet = workbook.getSheet(getTestName());

		for (z=1;z<dataSheet.getLastRowNum();z++) {
			row = dataSheet.getRow(z);
			if (row.getCell(0).getStringCellValue().equalsIgnoreCase(testname)) {
				break;
			}
		}

		for (int i = 0; i < dataSheet.getRow(0).getLastCellNum(); i++) {
			row = dataSheet.getRow(0);
			String key = row.getCell(i).getStringCellValue();
			System.out.println(key);
			row = dataSheet.getRow(z);
			String value = row.getCell(i).getStringCellValue();
			System.out.println(value);
			data.put(key, value);
		}
		workbook.close();
	}
	/*public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException{

        //Create an object of File class to open xlsx file

        File file =    new File(filePath+"\\"+fileName);

        //Create an object of FileInputStream class to read excel file

        FileInputStream inputStream = new FileInputStream(file);

        Workbook guru99Workbook = null;

        //Find the file extension by splitting  file name in substring and getting only extension name

        String fileExtensionName = fileName.substring(fileName.indexOf("."));

        //Check condition if the file is xlsx file

        if(fileExtensionName.equals(".xlsx")){

        //If it is xlsx file then create object of XSSFWorkbook class

        guru99Workbook = new XSSFWorkbook(inputStream);

        }

        //Check condition if the file is xls file

        else if(fileExtensionName.equals(".xls")){

            //If it is xls file then create object of XSSFWorkbook class


        	HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
        }    

    //Read excel sheet by sheet name    

    Sheet sheet = guru99Workbook.getSheet(sheetName);

    //Get the current count of rows in excel file

    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();

    //Get the first row from the sheet

    Row row = sheet.getRow(0);

    //Create a new row and append it at last of sheet

    Row newRow = sheet.createRow(rowCount+1);

    //Create a loop over the cell of newly created Row

    for(int j = 0; j < row.getLastCellNum(); j++){

        //Fill data in row

        Cell cell = newRow.createCell(j);

        cell.setCellValue(dataToWrite[j]);

    }

    //Close input stream

    inputStream.close();

    //Create an object of FileOutputStream class to create write data in excel file

    FileOutputStream outputStream = new FileOutputStream(file);

    //write data in the excel file

    guru99Workbook.write(outputStream);

    //close output stream

    outputStream.close();

    }










	 */















	public static String getProperty(String property) {
		if (System.getProperty(property) != null) {
			return System.getProperty(property);
		}
		File setupPropFile = new File("setup.properties");
		if (setupPropFile.exists()) {
			Properties prop = new Properties();
			FileReader reader;
			try {
				reader = new FileReader(setupPropFile);
				prop.load(reader);
				reader.close();
				return prop.getProperty(property);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getTestName() {
		String[] klassNameSplit = this.getClass().getName().split("\\.");
		System.out.println(klassNameSplit[klassNameSplit.length - 1]);
		return klassNameSplit[klassNameSplit.length - 1];

	}

	public void onFinish(ISuite suite) {
		System.out.println("Finishing");
	}

	public void onStart(ISuite suite) {
		System.out.println("Starting");
	}

	public void ClickSendText(String Objname, String Textval, ExtentTest Log) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				driver.get().findElement(By.xpath(Objname)).click();
				driver.get().findElement(By.xpath(Objname)).clear();
				driver.get().findElement(By.xpath(Objname)).sendKeys(Textval);
				
				//Log.log(LogStatus.PASS, Textval + " entered in the text field ");
				//takeSnap();

			} else {
				Log.log(LogStatus.FAIL, Objname + " not dipslayed on page");
				System.out.println(Objname + "is not displayed");
				//takeSnap();

			}
		} catch (NoSuchElementException e) {

			Log.log(LogStatus.FAIL, "Unable to enter the " + Textval);
			//takeSnap();
		}
	}

	public boolean elementAvailbility(String Objname, String object, ExtentTest logger) {
		try {
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				logger.log(LogStatus.INFO, driver.get().findElement(By.xpath(Objname)).getText() + " is displayed");
				driver.get().findElement(By.xpath(object)).click();
				takeSnap();

			}
			return true;
		} catch (WebDriverException e) {
			logger.log(LogStatus.INFO, " Password Alert is displayed");

		}
		return false;
	}

	public boolean verifyElementDisplayedlongTime(String Objname, String message, ExtentTest logger) {
		//Thread.sleep(2000);
		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 300);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			takeSnap();
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText() + "-" + message));
			//logger.log(LogStatus.PASS,driver.get().findElement(By.xpath(Objname)).getText() + "  is dipslayed on page");
			return true;
		} else {
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			System.out.println(Objname + "is not displayed");
			//takeSnap();
			return false;
		}
	}


	public boolean verifyElementDisplayed(String Objname, String message, ExtentTest logger) {
		//Thread.sleep(2000);
		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 60);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			takeSnap();
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText() + "-" + message));
			//logger.log(LogStatus.PASS,driver.get().findElement(By.xpath(Objname)).getText() + "  is dipslayed on page");
			return true;
		} else {
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			System.out.println(Objname + "is not displayed");
			//takeSnap();
			return false;
		}
	}


	public boolean verifyErroDisplayed(String Objname) {

		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText()));
			takeSnap();
			return true;
		} else {
			System.out.println(Objname + "is not displayed");
			takeSnap();
			return false;
		}
	}

	public void verifyElementText(String Objname, String Text, String message , ExtentTest logger) {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));		
		if(driver.get().findElement(By.xpath(Objname)).isDisplayed()==true) {
			if (driver.get().findElement(By.xpath(Objname)).getText().equals(Text)) {
				System.out.println(message);
				//logger.log(LogStatus.PASS, message);
				//takeSnap();
			}} else {
				logger.log(LogStatus.FAIL, "Unsuccessful Completion");
				System.out.println("Unsuccessful Completion");
				takeSnap();
			}
		
		
		
		
	}

	public boolean verifyElementcontainsText(String Objname, String Text, String message , ExtentTest logger) {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).getText().contains(Text)) {
			logger.log(LogStatus.PASS, Text +message);
			return true;

		} else {
			System.out.println("Unsuccessful Completion");
			logger.log(LogStatus.FAIL, "Unsuccessful Completion");
			return false;
		}
	}

	public boolean verifyElementinContainerContainsText(String Objname, String strText, ExtentTest logger) {
		try {
			Thread.sleep(1000);

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				logger.log(LogStatus.PASS, Objname + "is displayed");

				List<WebElement> totalObj = driver.get().findElements(By.xpath(Objname));
				for (int i=0;i <totalObj.size(); i++) {
					if (totalObj.get(i).getText().contains(strText)) {
						System.out.println(strText + " is dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.PASS, strText + " is dispalyed inside container");
					} else {
						System.out.println(strText + " not dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.FAIL, strText + " is not dispalyed inside container");
					}
				}
				return true;

			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}
		return false;

	}













	public boolean verifyElementinContainer(String Objname, String strText, ExtentTest logger) {
		try {
			Thread.sleep(1000);

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				logger.log(LogStatus.PASS, Objname + "is displayed");

				List<WebElement> totalObj = driver.get().findElements(By.xpath(Objname));
				for (int i=0;i <totalObj.size(); i++) {
					if (totalObj.get(i).getText().equalsIgnoreCase(strText)) {
						System.out.println(strText + " is dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.PASS, strText + " is dispalyed inside container");
					} else {
						System.out.println(strText + " not dispalyed inside container");
						takeSnap();
						logger.log(LogStatus.FAIL, strText + " is not dispalyed inside container");
					}
				}
				return true;

			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}
		return false;

	}
	public boolean verifyElementSelected(String Objname, String message, ExtentTest logger)
			throws InterruptedException {

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

		if (driver.get().findElement(By.xpath(Objname)).isEnabled()) {
			System.out.println((driver.get().findElement(By.xpath(Objname)).getText() + "-" + message));
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)).getText() + "-" + message);
			takeSnap();
			return true;
		} else {
			System.out.println(Objname + "is not displayed");
			takeSnap();
			logger.log(LogStatus.FAIL, Objname + "is not displayed");
			return false;
		}
	}

	public String GetAttributeValue(String Objname , String attObject) {

		String abc = driver.get().findElement(By.xpath(Objname)).getAttribute(attObject);

		return abc;
	}
	public void ClickSendText1(String Objname, String Textval, ExtentTest logger) {

		try {
			Thread.sleep(2000);

			// WebDriverWait myWaitVar = new WebDriverWait(driver.get(),const_waitmed);
			// myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));

			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
				driver.get().findElement(By.xpath(Objname)).click();
				// driver.get().findElement(By.xpath(Objname)).clear();
				driver.get().findElement(By.xpath(Objname)).sendKeys(Textval);
				takeSnap();
				logger.log(LogStatus.PASS, Textval + " successfully entered in text field ");

			} else {
				logger.log(LogStatus.FAIL, driver.get().findElement(By.xpath(Objname)).getText() + " is not displayed ");

			}
		} catch (InterruptedException e) {
			logger.log(LogStatus.FAIL, e.getMessage());
			takeSnap();
		}

	}

	public boolean click(String Objname, ExtentTest logger) {
		try {

			/*WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 120);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
			*/
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				//WebDriverWait myWaitVar1 = new WebDriverWait(driver.get(), 120);
				//myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));

				driver.get().findElement(By.xpath(Objname)).click();

				//takeSnap();
				//logger.log(LogStatus.PASS, " Button clicked Successfully");
				return true;

			} else {
				System.out.println("This is else block");
				logger.log(LogStatus.FAIL, driver.get().findElement(By.xpath(Objname)).getText() + " Button is not clicked ");
				//takeSnap();
				return false;
			}

		}catch(Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
		return true;
	}

	void SelectDropDown(String Objname, String objectValue , ExtentTest logger ) throws InterruptedException {

		try {

			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
			logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)) + " is displayed ");
			takeSnap();
			if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {

				driver.get().findElement(By.xpath(Objname)).click();
				logger.log(LogStatus.PASS, driver.get().findElement(By.xpath(Objname)) + " is clicked ");
				Thread.sleep(1000);
				List<WebElement> drodown = driver.get().findElements(By.xpath(objectValue));
				Random r = new Random();

				int index = r.nextInt(drodown.size());
				WebElement value = driver.get().findElement(By.xpath(objectValue + "[" + index + "]"));	
				((JavascriptExecutor) driver).executeScript("arguments[0].click()", value);			
				logger.log(LogStatus.PASS, value + " value is selected ");
				takeSnap();
			}
		} catch (NoSuchElementException e) {
			takeSnap();
			logger.log(LogStatus.FAIL,  e.getLocalizedMessage());
		} catch (Exception e) {
			// reportStep("The element: "+objectValue+" can't be selected :", "Fail");
		}
	}


	public boolean dropDownSelection(String Objname, String objectValue, String dropDownValue, ExtentTest logger)
	{

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			/*takeSnap();
			Thread.sleep(3000);*/
			driver.get().findElement(By.xpath(Objname)).click();
			//logger.log(LogStatus.PASS, "Dropdown is clicked ");

			//			Thread.sleep(3000);
			List<WebElement> dropdown = driver.get().findElements(By.xpath(objectValue));
			for (WebElement drpdownOptions : dropdown) {

				if (drpdownOptions.getText().contains(dropDownValue)) {
					drpdownOptions.click();

					//logger.log(LogStatus.PASS, dropDownValue + " is selected ");
					//takeSnap();
					break;
				}
			}

			return true;
		} else {

			logger.log(LogStatus.FAIL, dropDownValue + " is not selected ");
			return false;

		}
	}

	public void SubmitButton(String Objname, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), 120);
			System.out.println(driver.get());
			WebElement el = myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));
			System.out.println(el);
			((JavascriptExecutor) driver.get()).executeScript("arguments[0].click()", el);
			System.out.println(driver.get());
			//((JavascriptExecutor) driver).executeScript("arguments[0].click()", el);
			//logger.log(LogStatus.PASS, Objname + " is clicked");
			//takeSnap();
			// takeSnap();
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
			//takeSnap();
		}

	}

	public boolean elementNotDisplayed(String Objname, ExtentTest logger) {
		try {
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Objname)));
			takeSnap();
			logger.log(LogStatus.PASS, Objname + " is not displayed");
			return true;

		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			takeSnap();
		}
		return false;

	}
	public void scrollPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,200)");
	}

	public void selectCalanderDate(String Objname, String calobj, String DateData, ExtentTest logger)
			throws InterruptedException {

		boolean dateSelected = false;

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		Thread.sleep(4000);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			driver.get().findElement(By.xpath(Objname)).click();
			takeSnap();
			logger.log(LogStatus.PASS, "Calander is clicked");
			Thread.sleep(4000);
			WebElement dateWidget = driver.get().findElement(By.xpath(calobj));
			List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
			for (WebElement TetData : rows) {

				List<WebElement> columns = TetData.findElements(By.tagName("td"));

				// comparing the text of cell with today's date and clicking it.
				for (WebElement cell : columns) {
					System.out.println(cell.getText());
					if (cell.getText().equals(DateData)) {
						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,250)");
						Thread.sleep(2000);

						cell.click();
						takeSnap();
						logger.log(LogStatus.PASS, DateData + "  is selected");

						dateSelected = true;
						break;
					} else {

					}
				}
				if (dateSelected == true)
					break;
			}
		} else {
			logger.log(LogStatus.FAIL, "Date is not selected");
			System.out.println("Calander option is not visble");
		}

	}

	public void pageRefresh(String Url) {

		driver.get().get(getProperty("URL"));

	}

	public void verifySortedElementsAssendingOrder(String objectName, String message, ExtentTest logger) {
		try {
			Thread.sleep(4000);

			if ((driver.get().findElement(By.xpath(objectName)).isDisplayed())) {
				logger.log(LogStatus.PASS, objectName + " is Displayed");

				List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

				List<String> textValues = new ArrayList<String>();

				for (int i = 0; i < elements.size(); i++) {

					textValues.add(elements.get(i).getText());
					System.out.println(elements.get(i).getText());
				}
				Collections.sort(textValues);



				List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
				for (int j = 0; j < textValues.size(); j++) {
					if (newelemnets.get(j).getText().equals(textValues.get(j))) {
						System.out.println(textValues.get(j) + message);
						takeSnap();
						logger.log(LogStatus.PASS, textValues.get(j) + message);

					} else {
						takeSnap();
						System.out.println(textValues.get(j) + " is not sorted");
						logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					}

				}
			} else {
				logger.log(LogStatus.FAIL, objectName +" is not Displayed");
			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}

	public void verifySortedElementsDecendingOrder(String objectName, String message, ExtentTest logger) {
		try {
			Thread.sleep(4000);
			/*
			 * WebDriverWait myWaitVar = new WebDriverWait(driver.get(),const_waitmed);
			 * myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
			 * objectName)));
			 */
			if ((driver.get().findElement(By.xpath(objectName)).isDisplayed())) {
				logger.log(LogStatus.PASS, objectName + " is Displayed");

				List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

				List<String> textValues = new ArrayList<String>();

				for (int i = 0; i < elements.size(); i++) {

					textValues.add(elements.get(i).getText());
					System.out.println(elements.get(i).getText());
				}
				Collections.sort(textValues);

				Collections.reverse(textValues);

				List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
				for (int j = 0; j < textValues.size(); j++) {
					if (newelemnets.get(j).getText().equals(textValues.get(j))) {
						System.out.println(textValues.get(j) + message);
						takeSnap();
						logger.log(LogStatus.PASS, textValues.get(j) + message);

					} else {
						takeSnap();
						System.out.println(textValues.get(j) + " is not sorted");
						logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					}

				}
			} else {
				logger.log(LogStatus.FAIL, objectName +" is not Displayed");
			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}




	public void verifySortedElementsDecendingOrderWithmultipleText(String objectName, String message, int indexValue , ExtentTest logger) {
		try {
			Thread.sleep(2000);
			WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
			myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objectName)));

			List<WebElement> elements = driver.get().findElements(By.xpath(objectName));

			List<String> textValues = new ArrayList<String>();

			for (int i = 0; i < elements.size(); i++) {


				textValues.add(elements.get(i).getText());
				System.out.println(textValues);
			}
			//String[] arr = elements.get().getText().split(" ");
			Collections.sort(textValues);

			Collections.reverse(textValues);

			List<WebElement> newelemnets = driver.get().findElements(By.xpath(objectName));
			for (int j = 0; j < textValues.size(); j++) {

				String[] sortedarr = newelemnets.get(j).getText().split(" ");
				if (sortedarr[indexValue].equals(textValues.get(j))) {
					System.out.println(textValues.get(j) + message);
					logger.log(LogStatus.PASS, sortedarr[indexValue] + message);
					takeSnap();
				} else {
					logger.log(LogStatus.FAIL, textValues.get(j) + " is not sorted");
					System.out.println(textValues.get(j) + " is not sorted");
					takeSnap();
				}

			}
		} catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
			System.out.println(e.getMessage());
		}
	}


	public static   String getElementText(String object , ExtentTest logger ) throws InterruptedException {	

		String text ="" ;
		List<WebElement> elements  = driver.get().findElements(By.xpath(object));
		if(!elements.isEmpty()) {
			Random r = new Random();
			int index = r.nextInt(elements.size());
			text = elements.get(index).getText();	
			System.out.println(text +"has been retrived");
			return text;		

		} else {
			logger.log(LogStatus.INFO, text + " is not been retrived");
			return null ;
		}
	}

	public static  String[] arrayElementText(String object , ExtentTest logger ) throws InterruptedException {	

		String [] text = {} ;
		List<WebElement> elements  = driver.get().findElements(By.xpath(object));
		if(!elements.isEmpty()) {
			int index ;
			Random r = new Random();
			if(elements.size()>0) {
				index = r.nextInt(elements.size());
			}else {
				index =0 ;
			}
			Thread.sleep(1000);
			String str = elements.get(index).getText();	
			text = str.split("\n");
			System.out.println(text);

			logger.log(LogStatus.INFO, text + " is been retrived");
			return text;		

		} else {
			logger.log(LogStatus.INFO, text + " is not been retrived");
			System.out.println(text);
			return text ;

		}
	}



	public void writeInTextFile(String PortID ) {
		FileOutputStream fop = null;
		File file;
		try {

			file = new File("D:\\newfile.txt");
			fop = new FileOutputStream(file);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			// get the content in bytes
			byte[] contentInBytes = PortID.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void selectCalenderMonth(String Objname, String DateYear, String DateMonth, String DateData, String index, ExtentTest logger)
			throws InterruptedException { 
		boolean dateSelected = false;

		WebDriverWait myWaitVar = new WebDriverWait(driver.get(), const_waitmed);
		Thread.sleep(4000);
		myWaitVar.until(ExpectedConditions.elementToBeClickable(By.xpath(Objname)));
		if (driver.get().findElement(By.xpath(Objname)).isDisplayed()) {
			driver.get().findElement(By.xpath(Objname)).click();
			takeSnap();
			logger.log(LogStatus.PASS, "Calander is clicked");
			Thread.sleep(4000);

			WebElement dateWidgetMonth = driver.get().findElement(By.xpath(("(" + GeneralObjects.Calendarmonthdrpdown) + ")[" + index + "]"));
			dateWidgetMonth.click();
			dateWidgetMonth.sendKeys(DateMonth);
			dateWidgetMonth.sendKeys(Keys.ENTER);

			WebElement dateWidgetYear = driver.get().findElement(By.xpath(("(" + GeneralObjects.Calendaryeardrpdown) + ")[" + index + "]"));
			dateWidgetYear.click();
			dateWidgetYear.sendKeys(DateYear);
			dateWidgetYear.sendKeys(Keys.ENTER);

			WebElement dateWidget = driver.get().findElement(By.xpath(("(" + GeneralObjects.calendardateview) + ")[" + index + "]"));
			List<WebElement> rows = dateWidget.findElements(By.tagName("tr"));
			for (WebElement TetData : rows) {

				List<WebElement> columns = TetData.findElements(By.tagName("td"));

				// comparing the text of cell with today's date and clicking it.
				for (WebElement cell : columns) {
					System.out.println(cell.getText());
					if (cell.getText().equals(DateData)) {
						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,250)");
						Thread.sleep(2000);

						cell.click();
						takeSnap();
						logger.log(LogStatus.PASS, DateData + "  is selected");

						dateSelected = true;
						break;
					} else {

					}
				}
				if (dateSelected == true)
					break;
			}
		}
	}

	public void selectValueUsingKey(String xpth, String txt ,ExtentTest logger) throws InterruptedException{

		driver.get().findElement(By.xpath(xpth)).sendKeys(txt);
		Thread.sleep(2000);
		driver.get().findElement(By.xpath(xpth)).sendKeys(Keys.ARROW_DOWN);
		driver.get().findElement(By.xpath(xpth)).sendKeys(Keys.ENTER);	

	}


}







