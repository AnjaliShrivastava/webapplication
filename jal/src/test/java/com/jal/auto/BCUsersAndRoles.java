package com.jal.auto;
import java.util.Arrays;
import java.util.Map;

import com.jal.pages.GeneralObjects;
import com.jal.pages.usersandrolesObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCUsersAndRoles extends TestBase {

	public void navigateToUsersAndRoles(ExtentTest logger) {
		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuUsersAndRoles , "is displayed on the page" , logger );
			click(GeneralObjects.MenuUsersAndRoles , logger );
			//verifyElementDisplayed(usersandrolesObjects.headertitle , "Redirected to UserAndRole Screen", logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	public void navigateToCreateRoles(ExtentTest logger) {

		click(usersandrolesObjects.mainTabRoles , logger);
		click(usersandrolesObjects.tabCreateRole, logger);
	}	

	public void navigateToExistingRoles(ExtentTest logger) {

		click(usersandrolesObjects.mainTabRoles ,logger);
		//click(usersandrolesObjects.tabExistingRoles, logger);
	}


	public void CreateRoleValidation(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		try{
			ClickSendText(usersandrolesObjects.inputRoleName,data.get("RoleName"), logger);

			ClickSendText(usersandrolesObjects.inputDescription,data.get("Description"), logger);
			verifyElementDisplayed(usersandrolesObjects.labelPermission , "is displayed on the page", logger);
			click(GeneralObjects.createButton, logger);
			verifyElementDisplayed(usersandrolesObjects.errorMessageExistingRole , "is displayed on the page", logger);
		}catch(Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}

	public void CreateRolewithDefaultPermission(Map<String, String> data , int num, ExtentTest logger) throws InterruptedException {
		ClickSendText(usersandrolesObjects.inputRoleName,data.get("RoleName") + num, logger);
		ClickSendText(usersandrolesObjects.inputDescription,data.get("Description"), logger);
		verifyElementDisplayed(usersandrolesObjects.labelPermission , "is displayed on the page", logger);
		Thread.sleep(1000);
		click(GeneralObjects.createButton, logger);
		verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page",logger);
		Thread.sleep(2000);
		verifyElementDisplayed(usersandrolesObjects.labelRoleName , "is displayed on the page", logger);
		verifyElementDisplayed(usersandrolesObjects.labelDescription , "is displayed on the page", logger);
		verifyElementDisplayed(usersandrolesObjects.labelSearchRole , "is displayed on the page", logger);

		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("RoleName")+num, logger);

		click(usersandrolesObjects.seachRoleIcon, logger);
		Thread.sleep(2000);
		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("RoleName")+num , logger);
		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("Description"), logger);

	}


	public void CreateRolewithspecifPermission(Map<String, String> data , int num, ExtentTest logger) throws InterruptedException {
		ClickSendText(usersandrolesObjects.inputRoleName,data.get("RoleName") + num, logger);
		ClickSendText(usersandrolesObjects.inputDescription,data.get("Description"), logger);
		verifyElementDisplayed(usersandrolesObjects.labelPermission , "is displayed on the page", logger);
		click(usersandrolesObjects.permission, logger);
		Thread.sleep(1000);
		click(GeneralObjects.createButton, logger);
		verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page",logger);
		Thread.sleep(1000);

		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("RoleName")+num, logger);

		click(usersandrolesObjects.seachRoleIcon, logger);
		Thread.sleep(2000);
		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("RoleName")+num , logger);

	}







	public void searchRole(Map<String, String> data , int num , ExtentTest logger ) throws InterruptedException {
		verifyElementDisplayed(usersandrolesObjects.labelRoleName , "is displayed on the page" , logger );
		verifyElementDisplayed(usersandrolesObjects.labelDescription , "is displayed on the page" , logger);
		verifyElementDisplayed(usersandrolesObjects.labelSearchRole , "is displayed on the page", logger);
		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("RoleName")+num , logger);
		Thread.sleep(1000);
		click(usersandrolesObjects.seachRoleIcon , logger);

		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("EditRoleName")+num, logger);
		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("EditDescription"), logger);
	}
	public void searchRolewithExistingRole(Map<String, String> data , int num , ExtentTest logger ) throws InterruptedException {
		verifyElementDisplayed(usersandrolesObjects.labelRoleName , "is displayed on the page" , logger );
		verifyElementDisplayed(usersandrolesObjects.labelDescription , "is displayed on the page" , logger);
		verifyElementDisplayed(usersandrolesObjects.labelSearchRole , "is displayed on the page", logger);
		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("RoleName")+num , logger);
		Thread.sleep(1000);
		click(usersandrolesObjects.seachRoleIcon , logger);

		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("RoleName")+num, logger);
		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("Description"), logger);
	}



	public void ResetValues(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		ClickSendText(usersandrolesObjects.inputRoleName,data.get("RoleName"), logger);
		ClickSendText(usersandrolesObjects.inputDescription,data.get("Description"), logger);
		Thread.sleep(2000);
		click(GeneralObjects.resetButton ,logger);
		Thread.sleep(1000);
		verifyElementText(usersandrolesObjects.inputRoleName, "" , "Entered value is resetted", logger);
		verifyElementText(usersandrolesObjects.inputDescription, "" , "Entered value is resetted", logger);

	}

	public void CancelRole(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		ClickSendText(usersandrolesObjects.inputRoleName,data.get("RoleName"), logger);
		ClickSendText(usersandrolesObjects.inputDescription,data.get("Description"), logger);
		Thread.sleep(2000);
		click(GeneralObjects.cancelButton , logger);	
		verifyElementSelected(usersandrolesObjects.tabExistingRoles ," is selected and Create Role page has been closed now", logger);

	}

	public void searchRole(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		verifyElementDisplayed(usersandrolesObjects.labelRoleName , "is displayed on the page" , logger);
		verifyElementDisplayed(usersandrolesObjects.labelDescription , "is displayed on the page" , logger);
		verifyElementDisplayed(usersandrolesObjects.labelSearchRole , "is displayed on the page" , logger);
		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("RoleName"), logger);
		Thread.sleep(1000);
		click(usersandrolesObjects.seachRoleIcon, logger);
		Thread.sleep(2000);
		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("RoleName"),logger );
		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("Description"), logger);
	}
	public void editRoleSection(ExtentTest logger) throws InterruptedException {
		click(usersandrolesObjects.roleEditIcon , logger);
	}


	public void DeleteRole(ExtentTest logger) throws InterruptedException {
		click(usersandrolesObjects.roledeletIcon , logger);
		click(usersandrolesObjects.deleteYesButton, logger);
		verifyElementDisplayed(usersandrolesObjects.DeletesuccessMessage , "is displayed on the page", logger);
		verifyElementDisplayed(usersandrolesObjects.errorMessageNoRole1st , "is displayed on the page", logger);
		verifyElementDisplayed(usersandrolesObjects.errorMessageNoRole2nd , "is displayed on the page",logger);

	}

	public void DeleteRoleValidation(ExtentTest logger) throws InterruptedException {

		try{
			click(usersandrolesObjects.roledeletIcon, logger);

			click(usersandrolesObjects.deleteYesButton, logger);
			verifyElementDisplayed(usersandrolesObjects.errorMsgRoleWithUser , "is displayed on the page",logger);
		}catch(Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	public void EditRoleValidation(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try{
			ClickSendText(usersandrolesObjects.inputEditRoleName,data.get("RoleName"), logger);

			click(GeneralObjects.SaveButton, logger);
			verifyElementDisplayed(usersandrolesObjects.errorMsgEditRole , "is displayed on the page", logger);
		}catch(Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}



	public void SaveRole(Map<String, String> data , int num, ExtentTest logger) throws InterruptedException {
		click(GeneralObjects.SaveButton, logger);
		verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);
		Thread.sleep(2000);
		ClickSendText(usersandrolesObjects.inputSearchRoleType,data.get("EditRoleName")+num , logger);

		click(usersandrolesObjects.seachRoleIcon, logger);
		Thread.sleep(2000);
		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("EditRoleName")+num, logger);


		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("EditDescription"), logger);


	}	

	public void ResetRole(Map<String, String> data ,int num , ExtentTest logger) throws InterruptedException {
		click(GeneralObjects.resetButton, logger);
		Thread.sleep(1000);
		verifyElementinContainer(usersandrolesObjects.roleNameContainer,data.get("RoleName")+num, logger);		
		verifyElementinContainer(usersandrolesObjects.roleDescriptionContainer,data.get("Description"), logger);


	}
	public void CancelRole(ExtentTest logger) throws InterruptedException {
		try {
			click(usersandrolesObjects.rolecloseIcon, logger);
			Thread.sleep(1000);
			elementNotDisplayed(usersandrolesObjects.inputEditRoleName, logger);		
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void editRole(Map<String, String> data , int num , ExtentTest logger) throws InterruptedException {
		ClickSendText(usersandrolesObjects.inputEditRoleName,data.get("EditRoleName")+num , logger);
		ClickSendText(usersandrolesObjects.inputEditDescription,data.get("EditDescription") , logger);

	} 

	// **********   Create New USers  **************************************


	public void CreateAgentUser(Map<String, String> data ,int num , ExtentTest logger) throws InterruptedException {
		try {
			//click(GeneralObjects.MenuUsersAndRoles , logger );
			//verifyElementDisplayed(usersandrolesObjects.headertitle , "Redirected to UserAndRole Screen", logger);
			//click(usersandrolesObjects.mainTabUsers , logger);
			click(usersandrolesObjects.tabCreateUser, logger);
			dropDownSelection(usersandrolesObjects.userTypefield,usersandrolesObjects.dropdownvalue , data.get("UserType"),logger);
			ClickSendText(usersandrolesObjects.TextField, data.get("ShortName"), logger);
			click(usersandrolesObjects.buttoncheck , logger);

			ClickSendText(usersandrolesObjects.portalId, data.get("PortalID")+num, logger);
			SelectDropDown(usersandrolesObjects.roledropdownvalue, usersandrolesObjects.dropdownvalue  ,logger);
			//dropDownSelection(usersandrolesObjects.roledropdownvalue,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);
			//click(usersandrolesObjects.clickprooflistlimited , logger);
			ClickSendText(usersandrolesObjects.prooflisttextfield, data.get("LimitedValues"), logger);
			click(usersandrolesObjects.agentcreatebutton , logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			logger.log(LogStatus.INFO, Details);
			//click(usersandrolesObjects.buttoncopymessage , logger);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}


	public void CreateConsigneeUser(Map<String, String> data, int num , ExtentTest logger) throws InterruptedException {
		try {
			click(usersandrolesObjects.tabCreateUser, logger);
			dropDownSelection(usersandrolesObjects.userTypefield,usersandrolesObjects.dropdownvalue , data.get("UserType"),logger);
			ClickSendText(usersandrolesObjects.TextField, data.get("ShortName"), logger);
			click(usersandrolesObjects.buttoncheck , logger);

			ClickSendText(usersandrolesObjects.portalId, data.get("PortalID")+num, logger);
			SelectDropDown(usersandrolesObjects.roledropdownvalue, usersandrolesObjects.dropdownvalue  ,logger);
			//dropDownSelection(usersandrolesObjects.portalIdconsigneerole,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);
			click(usersandrolesObjects.consigneecreatebutton , logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);
			//click(usersandrolesObjects.buttoncopymessage , logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			logger.log(LogStatus.INFO, Details);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}



	public void CreateAdminUser(Map<String, String> data ,int num, ExtentTest logger) throws InterruptedException {
		try {

			click(usersandrolesObjects.tabCreateUser, logger);
			dropDownSelection(usersandrolesObjects.userTypefield,usersandrolesObjects.dropdownvalue , data.get("UserType"),logger);
			ClickSendText(usersandrolesObjects.employeeId, data.get("EmployeeId")+num, logger);
			ClickSendText(usersandrolesObjects.firstname, data.get("FirstName"), logger);
			ClickSendText(usersandrolesObjects.lastname, data.get("LastName"), logger);
			ClickSendText(usersandrolesObjects.department, data.get("Department"), logger);
			SelectDropDown(usersandrolesObjects.rolesdropdown, usersandrolesObjects.dropdownvalue  ,logger);
			//dropDownSelection(usersandrolesObjects.rolesdropdown,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);
			ClickSendText(usersandrolesObjects.telephone, data.get("Telephone"), logger);

			//ClickSendText(usersandrolesObjects.portalIdofExistinguser, data.get("PortalIdOfExistingUser"), logger);
			click(usersandrolesObjects.consigneecreatebutton , logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);
			//click(usersandrolesObjects.buttoncopymessage , logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			logger.log(LogStatus.INFO, Details);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}


	public void CreateStaffUser(Map<String, String> data, int num , ExtentTest logger) throws InterruptedException {
		try {
			click(usersandrolesObjects.tabCreateUser, logger);
			dropDownSelection(usersandrolesObjects.userTypefield,usersandrolesObjects.dropdownvalue , data.get("UserType"),logger);
			ClickSendText(usersandrolesObjects.employeeId, data.get("EmployeeId")+num, logger);
			ClickSendText(usersandrolesObjects.firstname, data.get("FirstName"), logger);
			ClickSendText(usersandrolesObjects.lastname, data.get("LastName"), logger);
			ClickSendText(usersandrolesObjects.department, data.get("Department"), logger);
			SelectDropDown(usersandrolesObjects.rolesdropdown, usersandrolesObjects.dropdownvalue  ,logger);
			//dropDownSelection(usersandrolesObjects.rolesdropdown,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);
			ClickSendText(usersandrolesObjects.telephone, data.get("Telephone"), logger);
			//ClickSendText(usersandrolesObjects.portalIdofExistinguser, data.get("PortalIdOfExistingUser"), logger);
			click(usersandrolesObjects.staffCreatebutton , logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);
			//click(usersandrolesObjects.buttoncopymessage , logger);
			verifyElementDisplayed(usersandrolesObjects.texttoastmessage , "is displayed on the page", logger);
			String Details = TestBase.getElementText(usersandrolesObjects.IDPasswoRDTEXT , logger);
			logger.log(LogStatus.INFO, Details);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}}  

	//********************************* EXISTING USERS ****************************//

	public void navigateToExistingUsers(ExtentTest logger) {


		try {
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuUsersAndRoles , "is displayed on the page" , logger );
			click(GeneralObjects.MenuUsersAndRoles , logger );
			//verifyElementDisplayed(usersandrolesObjects.headertitle , "Redirected to UserAndRole Screen", logger);
			//	click(usersandrolesObjects.tabExistingUsers, logger);
		}catch(Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	} 

	public void ValidateSortUsersbyPortalID(Map<String, String> data , ExtentTest logger) {
		try{
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("portalid"), logger);
			System.out.println(TestBase.Gettextvalue("portalid"));
			verifySortedElementsAssendingOrder(usersandrolesObjects.sortedportalIDtext, ":is sorted based on portalid" , logger);
			//verifySortedElementsDecendingOrder(usersandrolesObjects.sortedportalIDtext, " :is sorted based on portalid" , logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	
	public void ValidateSortUsersDesc(Map<String, String> data , ExtentTest logger) {
		try{
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("portalid"), logger);
			click(usersandrolesObjects.downSortArrow , logger);
			verifySortedElementsDecendingOrder(usersandrolesObjects.sortedportalIDtext, " :is sorted based on portalid in Descending" , logger);
			
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("usertype"), logger);
			verifySortedElementsDecendingOrder(usersandrolesObjects.sortedusertypetext, " :is sorted based on User Type in Descending" , logger);
		
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("firstname"), logger);
			verifySortedElementsDecendingOrder(usersandrolesObjects.sortedfirstnametext, " :is sorted based on First Name in Descending" , logger);
		
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("lastname"), logger);
			verifySortedElementsDecendingOrder(usersandrolesObjects.sortedlastnametext, ":is sorted based on lastname in Descending" , logger);
		
		
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}
	

	public void ValidateSortUsersbyUsername(Map<String, String> data , ExtentTest logger) {
		try{
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("usertype"), logger);
			System.out.println(TestBase.Gettextvalue("usertype"));
			verifySortedElementsAssendingOrder(usersandrolesObjects.sortedusertypetext, ":is sorted based on usertype" , logger);
			//verifySortedElementsDecendingOrder(usersandrolesObjects.sortedusertypetext, " :is sorted based on usertype", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortUsersbyFirstname(Map<String, String> data , ExtentTest logger) {
		try{
			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("firstname"), logger);
			System.out.println(TestBase.Gettextvalue("firstname"));
			verifySortedElementsAssendingOrder(usersandrolesObjects.sortedfirstnametext, ":is sorted based on firstname" , logger);
			//verifySortedElementsDecendingOrder(usersandrolesObjects.sortedfirstnametext, " :is sorted based on firstname",logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void ValidateSortUsersbyLastname(Map<String, String> data , ExtentTest logger) {
		try{

			dropDownSelection(usersandrolesObjects.usersortBydropdown,usersandrolesObjects.dropdownvalue , TestBase.Gettextvalue("lastname"), logger);
			System.out.println(TestBase.Gettextvalue("lastname"));
			verifySortedElementsAssendingOrder(usersandrolesObjects.sortedlastnametext, ":is sorted based on lastname" , logger);
			//verifySortedElementsDecendingOrder(usersandrolesObjects.sortedlastnametext, " :is sorted based on lastname", logger);
		}catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void searchUsersActive(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {

			Thread.sleep(1000);
			String [] text = TestBase.arrayElementText(usersandrolesObjects.UserCardActive, logger);
			Thread.sleep(1000);

			if(text[1].equalsIgnoreCase("Administrator")){
				text[1] ="Admin" ;

			}
			click(usersandrolesObjects.searchusericon, logger);
			ClickSendText(usersandrolesObjects.searchuserportalID, text[0], logger);
			dropDownSelection(usersandrolesObjects.searchusertypedropdown,usersandrolesObjects.dropdownvalue , text[1],logger);
			dropDownSelection(usersandrolesObjects.searchstatusdropdown,usersandrolesObjects.dropdownvalue , "Active",logger);
			Thread.sleep(1000);
			SubmitButton(usersandrolesObjects.SubmitButton, logger);
			verifyElementinContainer(usersandrolesObjects.sortedportalIDtext,text[0], logger);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void searchUsersInactive(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {

			Thread.sleep(1000);
			String [] text = TestBase.arrayElementText(usersandrolesObjects.UserCardInActive, logger);
			Thread.sleep(2000);
			if(text[1].equalsIgnoreCase("Administrator")){
				text[1] ="Admin" ;

			}
			click(usersandrolesObjects.searchusericon, logger);
			ClickSendText(usersandrolesObjects.searchuserportalID, text[0], logger);
			dropDownSelection(usersandrolesObjects.searchusertypedropdown,usersandrolesObjects.dropdownvalue , text[1],logger);
			dropDownSelection(usersandrolesObjects.searchstatusdropdown,usersandrolesObjects.dropdownvalue , "Inactive",logger);
			SubmitButton(usersandrolesObjects.SubmitButton, logger);
			verifyElementinContainer(usersandrolesObjects.sortedportalIDtext,text[0], logger);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void searchUsersName(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {

			Thread.sleep(1000);
			String [] text = TestBase.arrayElementText(usersandrolesObjects.UserCardActive, logger);
			Thread.sleep(2000);
			System.out.println(Arrays.toString(text));
			click(usersandrolesObjects.searchusericon, logger);
			ClickSendText(usersandrolesObjects.searchuserfirstname, text[2], logger);
			if( text.length > 3) {
			ClickSendText(usersandrolesObjects.searchuserlastname, text[3], logger);
			}
			SubmitButton(usersandrolesObjects.SubmitButton, logger);
			Thread.sleep(1000);
			verifyElementinContainerContainsText(usersandrolesObjects.sortedfirstnametext,text[2], logger);
			if( text.length >3) {
				verifyElementinContainerContainsText(usersandrolesObjects.sortedlastnametext,text[3], logger);
		}
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}


	public void searchUsersLocked(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {
			click(usersandrolesObjects.editUserIcon, logger);
			scrollPage();
			dropDownSelection(usersandrolesObjects.editPortalStatus,usersandrolesObjects.dropdownvalue , "Locked", logger);
			Thread.sleep(2000);
			click(GeneralObjects.SaveButton,logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page",logger);
			Thread.sleep(1000);
			String [] text = TestBase.arrayElementText(usersandrolesObjects.UserCardLockedUser, logger);
			Thread.sleep(1000);
			if(text[1].equalsIgnoreCase("Administrator")){
				text[1] ="Admin" ;    		
			}

			click(usersandrolesObjects.searchusericon, logger);
			Thread.sleep(1000);
			ClickSendText(usersandrolesObjects.searchuserportalID, text[0], logger);
			dropDownSelection(usersandrolesObjects.searchusertypedropdown,usersandrolesObjects.dropdownvalue , text[1],logger);
			dropDownSelection(usersandrolesObjects.searchstatusdropdown,usersandrolesObjects.dropdownvalue , "Locked",logger);
			SubmitButton(usersandrolesObjects.SubmitButton, logger);
			verifyElementinContainer(usersandrolesObjects.sortedportalIDtext,text[0], logger);
		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}













	//click(GeneralObjects.userserachsubmit,logger);
	//verifyElementinContainer(usersandrolesObjects.portalIdcontainer,usersandrolesObjects.dropdownvalue.TestBase.Gettextvalue("PortalID"), logger);
	//verifyElementinContainer(usersandrolesObjects.usertypecontainer,data.get("UserType"), logger);
	//verifyElementinContainer(usersandrolesObjects.firstnamecontainer,data.get("Status"), logger);
	//verifyElementinContainer(usersandrolesObjects.firstnamecontainer,data.get("FirstName"), logger);
	//verifyElementinContainer(usersandrolesObjects.lastnamecontainer,data.get("LastName"), logger);

	/*click(usersandrolesObjects.userediticon,logger);
			dropDownSelection(usersandrolesObjects.edituserportalstatusdropdown,usersandrolesObjects.dropdownvalue , data.get("Status"),logger);
			dropDownSelection(usersandrolesObjects.edituserroledropdown,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);

			//click(usersandrolesObjects.editprooflistAll , logger);
			click(usersandrolesObjects.editprooflistlimitedbutton , logger);
			ClickSendText(usersandrolesObjects.editprooflistlimitedtext, data.get("LimitedValue"), logger);

			click(GeneralObjects.SaveButton , logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page", logger);*/


	public void searchCreatedUsers(Map<String, String> data , int num, ExtentTest logger) throws InterruptedException {	
		try {
			click(usersandrolesObjects.searchusericon, logger);
			ClickSendText(usersandrolesObjects.searchuserportalID, data.get("PortalID")+num, logger);
			click(usersandrolesObjects.SubmitButton,logger);
			verifyElementinContainer(usersandrolesObjects.portalIdcontainer,data.get("RoleName")+num, logger);
		}catch(Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());	
		}}


	public void searchUsersResetUsers(Map<String, String> data , ExtentTest logger) throws InterruptedException {
		try {
			click(usersandrolesObjects.searchusericon, logger);
			ClickSendText(usersandrolesObjects.searchuserportalID, data.get("PortalID"), logger);
			dropDownSelection(usersandrolesObjects.searchusertypedropdown,usersandrolesObjects.dropdownvalue , data.get("UserType"),logger);
			dropDownSelection(usersandrolesObjects.searchstatusdropdown,usersandrolesObjects.dropdownvalue , data.get("Status"),logger);
			ClickSendText(usersandrolesObjects.searchuserfirstname, data.get("FirstName"), logger);
			ClickSendText(usersandrolesObjects.searchuserlastname, data.get("LastName"), logger);

			click(usersandrolesObjects.searchbuttonsubmit , logger);
			click(usersandrolesObjects.userediticon , logger);

			dropDownSelection(usersandrolesObjects.edituserportalstatusdropdown,usersandrolesObjects.dropdownvalue , data.get("Status"),logger);
			dropDownSelection(usersandrolesObjects.edituserroledropdown,usersandrolesObjects.dropdownvalue , data.get("Role"),logger);
			//click(usersandrolesObjects.editprooflistAll , logger);
			click(usersandrolesObjects.editprooflistlimitedbutton , logger);
			ClickSendText(usersandrolesObjects.editprooflistlimitedtext, data.get("LimitedValue"), logger);
			click(GeneralObjects.resetButton , logger);

		}
		catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}
	}

	public void editUser(Map<String, String> data , ExtentTest logger) throws InterruptedException {	
		try {
			click(usersandrolesObjects.editUserIcon, logger);
			scrollPage();
			SelectDropDown(usersandrolesObjects.editPortalStatus, usersandrolesObjects.dropdownvalue  ,logger);
			SelectDropDown(usersandrolesObjects.editAgentRole, usersandrolesObjects.dropdownvalue  ,logger);
			click(GeneralObjects.SaveButton,logger);
			verifyElementDisplayed(usersandrolesObjects.successMessage , "is displayed on the page",logger);
		}catch(Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());	
		}}

	public void deleteUserData(Map<String, String> data , ExtentTest logger) throws InterruptedException {	
		try {
			click(usersandrolesObjects.deletUserIcon, logger);
			click(GeneralObjects.YesButton,logger);
			verifyElementDisplayed(usersandrolesObjects.deletUserMessage , "is displayed on the page",logger);
		}catch(Exception e) {

			logger.log(LogStatus.FAIL, e.getMessage());	
		}}





}
