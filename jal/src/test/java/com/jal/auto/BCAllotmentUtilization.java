package com.jal.auto;

import java.util.Map;

import com.jal.pages.AllotmentUtilizationObjects;
import com.jal.pages.GeneralObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCAllotmentUtilization extends TestBase {

	public void NavigatetoAllotmentUtilization(ExtentTest logger ) {
		try{
			click(GeneralObjects.BurgerMenu , logger);
			verifyElementDisplayed(GeneralObjects.MenuAllotmentUtilization , "is displayed on the page" , logger );
			click(GeneralObjects.MenuAllotmentUtilization , logger);
		}catch (Exception e) {
			logger.log(LogStatus.ERROR, e.getMessage());
		}
	}
	
		public void DifferentDatesearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
				try {
					
					click(AllotmentUtilizationObjects.btnSearch , logger);
					selectCalenderMonth(AllotmentUtilizationObjects.Searchstartdate , data.get("Year"),data.get("StartDateMonth"),data.get("StartDate"),("1"), logger );
					selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate ,data.get("Year"),data.get("EndDateMonth"),data.get("EndDate"),("2"), logger );
					
					//dropDownSelection(AllotmentUtilizationObjects.Calendarheader,AllotmentUtilizationObjects.Calendarmonthdrpdown , data.get("EndDateMonth"),logger);
					//ClickSendText(AllotmentUtilizationObjects.InputTextField,data.get("Text"), logger);
					click(AllotmentUtilizationObjects.buttonsubmit , logger);
					
					verifyElementcontainsText("//div[@class='col-10']",data.get("StartDate")+" "+data.get("StartDateMonth").substring(0, 3)+" "+data.get("Year")+" until "+data.get("EndDate")+" "+data.get("EndDateMonth").substring(0, 3)+" "+data.get("Year"), " Search results are displaying " , logger);
				
					click(AllotmentUtilizationObjects.clickPDF , logger);
					
				}catch (Exception e) {
					logger.log(LogStatus.FAIL, e.getMessage());
				}

			}
		public void CurrentDatesearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {
				
				click(AllotmentUtilizationObjects.btnSearch , logger);
				//selectCalenderMonth(AllotmentUtilizationObjects.Searchstartdate ,data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("1"), logger );
				//selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate , data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("2"), logger );
				click(AllotmentUtilizationObjects.buttonsubmit , logger);
				click(AllotmentUtilizationObjects.clickPDF , logger);
			}catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}
		public void NegativeDatesearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {
			try {
				
				click(AllotmentUtilizationObjects.btnSearch , logger);
				//ClickSendText(AllotmentUtilizationObjects.Searchstartdate,data.get("Date"), logger);
				//selectCalenderMonth(AllotmentUtilizationObjects.Searchstartdate ,data.get("Date"),("1"), logger );
				selectCalanderDate(AllotmentUtilizationObjects.Searchstartdate ,AllotmentUtilizationObjects.calendarviewstartdate,data.get("StartDate"), logger );
				click(AllotmentUtilizationObjects.startdateclosebutton , logger);
				//ClickSendText(AllotmentUtilizationObjects.Searchenddate,data.get("Date"), logger);
				selectCalanderDate(AllotmentUtilizationObjects.Searchenddate ,AllotmentUtilizationObjects.calendarviewEnddate,data.get("EndDate"), logger );
				click(AllotmentUtilizationObjects.enddateclosebutton , logger);
				//selectCalenderMonth(AllotmentUtilizationObjects.Searchenddate ,data.get("Year"),data.get("StartDateMonth"),data.get("Date"),("2"), logger );
				ClickSendText(AllotmentUtilizationObjects.flightnumbersuffix,data.get("FlightNo."), logger);
				click(AllotmentUtilizationObjects.daysofweeks , logger);
				dropDownSelection(AllotmentUtilizationObjects.originfield,AllotmentUtilizationObjects.origindrpdown , data.get("Origin"),logger);
				click(AllotmentUtilizationObjects.buttonsubmit , logger);
				verifyElementDisplayed(AllotmentUtilizationObjects.Fromdateerrormessage , "is displayed on the page" , logger );
				verifyElementDisplayed(AllotmentUtilizationObjects.Todateerrormessage , "is displayed on the page" , logger );
				verifyElementDisplayed(AllotmentUtilizationObjects.flightnumbererrormessage , "is displayed on the page" , logger );
				verifyElementDisplayed(AllotmentUtilizationObjects.daysofweekserrormessage , "is displayed on the page" , logger );
				verifyElementDisplayed(AllotmentUtilizationObjects.originerrormessage , "is displayed on the page" , logger );
			}catch (Exception e) {
				logger.log(LogStatus.FAIL, e.getMessage());
			}

		}
}