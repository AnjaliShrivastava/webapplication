package com.jal.auto;
import java.util.Date;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import com.jal.pages.AgentNotificationsObjects;
import com.jal.pages.MyProfileObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class BCAgentNotifications extends TestBase {
	WebDriver driver;

	public BCAgentNotifications() {
		
	}

	Date d = new Date();

	@SuppressWarnings("deprecation")
	int todayDate = d.getDate();
	String tDate = Integer.toString(todayDate);

	public void navigateToMyProfile(Map<String, String> data, ExtentTest logger) throws InterruptedException{
		
			click(MyProfileObjects.welcomebutton, logger);
			click(MyProfileObjects.myprofile, logger);
			//dropDownSelection(MyProfileObjects.myprofile, MyProfileObjects.myprofile,TestBase.Gettextvalue("myprofile"), logger);
			logger.log(LogStatus.PASS, "User redirect to My profile page");
		
	}

	public void NotificationsTab(ExtentTest logger) {
		
			click(AgentNotificationsObjects.tabnotification, logger);
		 
	}
	public void NavigateNotificationUsingBell(ExtentTest logger) throws InterruptedException {
		
			click(AgentNotificationsObjects.notificationBell, logger);
			verifyElementSelected(AgentNotificationsObjects.tabnotification ,"is selected and User redirected to Notification page" , logger);
		
	}

	public void SearchClose(Map<String, String> data, ExtentTest logger) throws InterruptedException {
		try {
			click(AgentNotificationsObjects.btnsearchicon, logger);
			ClickSendText(AgentNotificationsObjects.txtawbprefix, data.get("AWBPrefix"), logger);
			ClickSendText(AgentNotificationsObjects.txtawbsuffix, data.get("AWBSerial"), logger);
			ClickSendText(AgentNotificationsObjects.shortnamevalues, data.get("ShortName"), logger);
			selectCalanderDate(AgentNotificationsObjects.dateField, AgentNotificationsObjects.startdatecalendarObjects,data.get("SearchDate"), logger);
			SubmitButton(AgentNotificationsObjects.btnsubmit, logger);
			Thread.sleep(1000);
			verifyElementinContainer(AgentNotificationsObjects.sortedAWBText,data.get("AWBPrefix")+"-"+data.get("AWBSerial"), logger);
			verifyElementinContainer(AgentNotificationsObjects.sortedShortname,data.get("ShortName"), logger);
			//verifyElementinContainerContainsText(AgentNotificationsObjects.sortedTimetext, data.get("SearchDate"), logger);
			
		} catch (Exception e) {
			logger.log(LogStatus.FAIL, e.getMessage());
		}

	}

	public void InvalidSearch(Map<String, String> data, ExtentTest logger) throws InterruptedException {

		click(AgentNotificationsObjects.btnsearchicon, logger);
		ClickSendText(AgentNotificationsObjects.txtawbprefix, data.get("AWBPrefix"), logger);
		ClickSendText(AgentNotificationsObjects.txtawbsuffix, data.get("AWBSerial"), logger);
		ClickSendText(AgentNotificationsObjects.shortnamevalues, data.get("ShortName"), logger);
		//selectCalanderDate(AgentNotificationsObjects.dateField, AgentNotificationsObjects.startdatecalendarObjects,tDate, logger);
		SubmitButton(AgentNotificationsObjects.btnsubmit, logger);
		verifyElementDisplayed(AgentNotificationsObjects.invalidAWBmessage, "is displayed on the page", logger);
		verifyElementDisplayed(AgentNotificationsObjects.invalidShortNamemessage, "is displayed on the page", logger);
	}

	public void sortByAWB(Map<String, String> data, ExtentTest logger) {
		
			dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"AWB", logger);
			System.out.println(TestBase.Gettextvalue("awb"));
		    // dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue,AgentNotificationsObjects.dropdownvalues , "AWB" );
			verifySortedElementsDecendingOrder(AgentNotificationsObjects.sortedAWBText, " :is sorted based on AWB",logger);

		
	}

	public void sortByShortName(Map<String, String> data, ExtentTest logger) {
		
			dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"Short Name", logger);
			System.out.println(TestBase.Gettextvalue("ShortName"));
			//click(AgentNotificationsObjects.ascendingvalues, logger);
			//verifySortedElementsAssendingOrder(AgentNotificationsObjects.sortedShortname, " :is sorted based on ShortName ",logger);
			verifySortedElementsDecendingOrder(AgentNotificationsObjects.sortedShortname," :is sorted based on ShortName", logger);

		
	}

	public void sortByDateTime(Map<String, String> data, ExtentTest logger) {
		
			dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"Date Time", logger);
			verifySortedElementsDecendingOrder(AgentNotificationsObjects.sortedDatetext," :is sorted based on Date", logger);
			/*dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,TestBase.Gettextvalue("Time"), logger);
			verifySortedElementsDecendingOrder(AgentNotificationsObjects.sortedTimetext," :is sorted based on Time", logger);*/
			
		
	}

public void AscendingsortByAWB(Map<String, String> data, ExtentTest logger) {
	try {
		dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"AWB", logger);
		Thread.sleep(3000);
		click(AgentNotificationsObjects.ascendingvalues, logger);
		Thread.sleep(2000);
		verifySortedElementsAssendingOrder(AgentNotificationsObjects.sortedAWBText, " :is sorted based on AWB ",logger);

	} catch (InterruptedException e) {
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}

public void AscendingsortByShortName(Map<String, String> data, ExtentTest logger) {
	try {
		dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"Short Name", logger);
		Thread.sleep(2000);
		click(AgentNotificationsObjects.ascendingvalues, logger);
		Thread.sleep(2000);
		verifySortedElementsAssendingOrder(AgentNotificationsObjects.sortedShortname, " :is sorted based on ShortName ",logger);
		
	} catch (InterruptedException e) {
		System.out.println(e.getMessage());
		logger.log(LogStatus.FAIL, e.getMessage());
	}
}

public void AscendingsortByDateTime(Map<String, String> data, ExtentTest logger) {
	try {
		dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,"Date Time", logger);
		Thread.sleep(2000);
		click(AgentNotificationsObjects.ascendingvalues, logger);
		Thread.sleep(2000);
		verifySortedElementsAssendingOrder(AgentNotificationsObjects.sortedDatetext, " :is sorted based on Date ",logger);
		/*dropDownSelection(AgentNotificationsObjects.sortbydrpdownvalue, AgentNotificationsObjects.dropdownvalues,TestBase.Gettextvalue("Time"), logger);
		click(AgentNotificationsObjects.ascendingvalues, logger);
		verifySortedElementsAssendingOrder(AgentNotificationsObjects.sortedTimetext, " :is sorted based on Time ",logger);*/
		
	} catch (InterruptedException e) {
		System.out.println(e.getMessage());
	}
}
}