package com.jal.auto;

import java.util.Map;

import org.apache.commons.lang3.time.StopWatch;

import com.jal.pages.GeneralObjects;
import com.jal.pages.NewBookingObjects;
import com.jal.pages.ShipmentListObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCNewBooking extends TestBase {
	long startTime ; long endTime ; long startTime1 , step2t;
	long b1 ,ts ;
	StopWatch pageLoad ;
	
	public void newBookingNavigation(ExtentTest logger ) {
		
			click(ShipmentListObjects.tabNewBooking , logger);
		b1 = System.currentTimeMillis(); 
			
		}
	
	
	public void EnterFlightDetails(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		verifyElementDisplayed(NewBookingObjects.originSelectField , "User redirected to create booking Screen" , logger);
		ts = System.currentTimeMillis(); 
		logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken for redirecting to create booking screen " + (ts-b1) + " MilliSeconds or "+(ts-b1)/1000 +" in Seconds");
		
			selectValueUsingKey(NewBookingObjects.originSelectField, data.get("Origin") ,logger);
			selectValueUsingKey(NewBookingObjects.DestinationselectField, data.get("Des") ,logger);
		
		
		
			
		}
	
	
	
	public void EnterShipmentDetailsforLooseBulk(Map<String, String> data , ExtentTest logger ) {
		
			click(NewBookingObjects.radioLooseBulk , logger);
			ClickSendText(NewBookingObjects.weightField,data.get("Wgt"), logger);
			ClickSendText(NewBookingObjects.dryIceField,data.get("Wgt"), logger);
			ClickSendText(NewBookingObjects.VolumeInput,data.get("Volume"), logger);
			ClickSendText(NewBookingObjects.commodityField,data.get("Commodity"), logger);
			click(NewBookingObjects.DryICE , logger);
			ClickSendText(NewBookingObjects.ICEPieces,data.get("Pcs"), logger);
			ClickSendText(NewBookingObjects.ICEWGT,data.get("icewgt"), logger);
			click(NewBookingObjects.PLUSIcon , logger);
			click(NewBookingObjects.DoneBtn , logger);
		
			
		}
	
	
	public void EnterShipmentDetailsforULDBUP(Map<String, String> data , ExtentTest logger ) {
		
			ClickSendText(NewBookingObjects.LD3,data.get("Pallet"), logger);
			ClickSendText(NewBookingObjects.LD2,data.get("Pallet"), logger);
			ClickSendText(NewBookingObjects.ULDPallet,data.get("Pallet"), logger);
			ClickSendText(NewBookingObjects.weightField,data.get("Wgt"), logger);
			ClickSendText(NewBookingObjects.dryIceField,data.get("Wgt"), logger);		
			ClickSendText(NewBookingObjects.commodityField,data.get("Commodity"), logger);
			/*scrollPage();
			ClickSendText(NewBookingObjects.SHCField,data.get("SHC"), logger);
			click(NewBookingObjects.SHCPlusIcon , logger);*/
		
		
			
		}
	
	
	
	public void SearchFlight(Map<String, String> data , ExtentTest logger ) {
		
			SubmitButton(NewBookingObjects.searchButton ,logger );
			step2t = System.currentTimeMillis();
			
			 /*pageLoad = new StopWatch();
			 pageLoad.start();*/
		
	}
	
	
	public void SelectFlight(Map<String, String> data , ExtentTest logger ) {
		
		verifyElementDisplayed("//div[@class='booking-step-2 col-xl-12']" , "API call completed and Select button is displayed" , logger);
		//pageLoad.stop();
		long step3t = System.currentTimeMillis();
		/*long pageLoadTime_ms = pageLoad.getTime();
        long pageLoadTime_Seconds = pageLoadTime_ms / 1000;
*/
        //System.out.println("step1timing " +totalTime );
       // logger.log(LogStatus.INFO, "Loading time to display Flight Details "  + pageLoadTime_ms + " milliseconds" + " or " + pageLoadTime_Seconds + " in Seconds" );
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Total time to display Flights (Step1-2) : "  + (step3t-step2t) + " milliseconds" + " or " + (step3t-step2t)/1000 + " in Seconds" );
        
        
		SubmitButton(NewBookingObjects.SelectButton , logger);
		startTime = System.currentTimeMillis(); 
		
		
		
		
			
		}
	
	
	public void cancelCreatedBooking(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		
			click(NewBookingObjects.CancelBookingButton , logger);
			verifyElementDisplayed(NewBookingObjects.cancelBookingMessage , "Booking cancellation message is displaying" , logger);
			click(NewBookingObjects.cancelYesButton , logger);
		
			
		}
	
	
	public void cancelBookingDetails( ExtentTest logger ) throws InterruptedException {
		
			
			String Details = TestBase.getElementText(NewBookingObjects.FlightDetailsSection , logger);
			verifyElementText(NewBookingObjects.BookingStatus,"CN", "Booking has been cancelled and Booking status changed to CN " , logger);
			
			logger.log(LogStatus.INFO,"(Browser Session " + driver.get().hashCode()+" ) Booking Details : "+ Details);
		
			
		}
	
	
	public void searchAWB(Map<String, String> data , ExtentTest logger ) throws InterruptedException {

		SubmitButton(ShipmentListObjects.btnSearch , logger);		
		ClickSendText(ShipmentListObjects.txtAWBprefix,data.get("AWBPrefix"), logger );
		ClickSendText(ShipmentListObjects.txtAWBserial,data.get("AWBSerial") , logger);
		SubmitButton(GeneralObjects.btnSubmit, logger);

	}
	
	
	
	public void enterNewShipperDetails(Map<String, String> data , ExtentTest logger ) {
		verifyElementDisplayed(NewBookingObjects.radioNewShipper , "Shipment Radio button displayed" , logger);
		endTime = System.currentTimeMillis(); 
		
        long totalTime = endTime - startTime;        
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken between selecting flight and getting shipment details section (Step2-3) : " +totalTime + " Milliseconds or " +totalTime/1000+" in Seconds");
        startTime = System.currentTimeMillis();
          click(NewBookingObjects.radioNewShipper , logger);
			ClickSendText(NewBookingObjects.newShipperName,data.get("name"), logger);
			ClickSendText(NewBookingObjects.newShipperPhNo,data.get("phone"), logger);
			ClickSendText(NewBookingObjects.newShipperAddress,data.get("address"), logger);
		
			
			
			
		}
	public void enterNewConsigneeDetails(Map<String, String> data , ExtentTest logger ) {
		
		endTime = System.currentTimeMillis();		    
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken in capturing Shipper Details : " +(endTime-startTime) + " Milliseconds or "+(endTime-startTime)/1000 +" in Seconds");
        startTime = System.currentTimeMillis();
        click(NewBookingObjects.consignneAccordian , logger);
		click(NewBookingObjects.newConsigneeRadioButton , logger);
		ClickSendText(NewBookingObjects.newConsigneeName,data.get("name"), logger);
		ClickSendText(NewBookingObjects.newConsigneePhNo,data.get("phone"), logger);
		ClickSendText(NewBookingObjects.newConsigneeAddress,data.get("address"), logger);
		
		
	}
	
	public void enterShipmentDetails(Map<String, String> data , ExtentTest logger ) {
		
		endTime = System.currentTimeMillis();		    
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken in capturing Cosignee Details : " +(endTime-startTime) + " Milliseconds or " +(endTime-startTime)/1000+" in Seconds");
       
        startTime = System.currentTimeMillis();	
        click(NewBookingObjects.shipmentAccordian , logger);
	
		ClickSendText(NewBookingObjects.pieces,data.get("Pcs"), logger);
		dropDownSelection(NewBookingObjects.ChargesDropDown,ShipmentListObjects.dropdownvalue , data.get("charges"), logger);
		ClickSendText(NewBookingObjects.natureOfGoods,data.get("NOG"), logger);
		ClickSendText(NewBookingObjects.valueOfCarriage,data.get("VOC"), logger);
		ClickSendText(NewBookingObjects.remarks,data.get("remark"), logger);
		dropDownSelection(NewBookingObjects.deliveryAtDropdown,ShipmentListObjects.dropdownvalue , data.get("DeliveryAt"), logger);
		ClickSendText(NewBookingObjects.deliveryRemarks,data.get("remark"), logger);
	
	
		
	}
	
	public void existingShipperDetails(Map<String, String> data , ExtentTest logger ) {
		
			click(NewBookingObjects.radioUseExistingShipper , logger);
			ClickSendText(NewBookingObjects.ShortNameShipper,data.get("ShortName"), logger);
			click(NewBookingObjects.searchButton , logger);
		
		
			
		}
	
	
	
	
	public void existingConsigneeDetails(Map<String, String> data , ExtentTest logger ) {
		
			click(NewBookingObjects.consignneAccordian , logger);
			//click(NewBookingObjects.radioExConsignee , logger);
			ClickSendText(NewBookingObjects.ShortName,data.get("ShortName"), logger);
			click(NewBookingObjects.consigneeSerachButton , logger);
		
		
			
		}
	
	

	
	
	
	
	
	
	public void continueBooking(Map<String, String> data , ExtentTest logger ) {
		endTime = System.currentTimeMillis();		    
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken in capturing Shipment Details : " +(endTime-startTime) + " Milliseconds or " +(endTime-startTime)/1000+" in Seconds");
       
			click(NewBookingObjects.continueButton , logger);
         startTime1 = System.currentTimeMillis(); 
		
		
		
			
		}
	
	
	public void paymentINFO(Map<String, String> data , ExtentTest logger ) throws InterruptedException {
		
		verifyElementDisplayed(NewBookingObjects.TAndC , "Shipment Radio button displayed" , logger);
		long endTime1 = System.currentTimeMillis(); 
        long totalTime = endTime1 - startTime1;        
        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken for getting payment Info (Step3-4) : " +totalTime + " Milliseconds or "+totalTime/1000 +" in Seconds");
		
			click(NewBookingObjects.TAndC , logger);
			click(NewBookingObjects.createBookingButton , logger);
			verifyElementDisplayed(NewBookingObjects.BookinCreationMessage , "Booking creation confirmation message is displaying" , logger);
			verifyElementDisplayed(NewBookingObjects.bookingConfirmationYes , "Booking confirmation yes button is displayed" , logger);
			click(NewBookingObjects.bookingConfirmationYes , logger);
			long st = System.currentTimeMillis(); 
			verifyElementDisplayed(NewBookingObjects.leverOpt , "Booking page" , logger);
			
			//SubmitButton(NewBookingObjects.leverOpt , logger);
			long et = System.currentTimeMillis(); 
	        long tt = et - st;        
	        logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Time taken for booking creation (Step4-5)  : " +tt + " Milliseconds or "+tt/1000  +" in Seconds");
			//click(NewBookingObjects.YesStatus , logger);
			
		
			
		}
	
	
	public void BookingDoneVerification( ExtentTest logger ) throws InterruptedException {
		
			/*verifyElementDisplayed(NewBookingObjects.editBookingButton , "Edit Booking Button is displaying" , logger);
			verifyElementDisplayed(NewBookingObjects.printLabelButton , "Print Label button is displayed" , logger);
			verifyElementDisplayed(NewBookingObjects.CancelBookingButton , "Cancel Booking button is displayed" , logger);
			verifyElementDisplayed(NewBookingObjects.editBookingButton , "Cancel Booking button is displayed" , logger);*/
			verifyElementText(NewBookingObjects.BookingStatus,"KK", "Booking has been created and Booking status is KK" , logger);
		
			
		}
	
	
	
	
	public void getBookingDetails( ExtentTest logger ) throws InterruptedException {
		
			
			String BookingDetails = TestBase.getElementText(NewBookingObjects.FlightDetailsSection , logger);
			
			logger.log(LogStatus.INFO, "(Browser Session " + driver.get().hashCode()+" ) Booking Details : "+ BookingDetails);
			
		}
	
	
	
	
}
		
	
	
