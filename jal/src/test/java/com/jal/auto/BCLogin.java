package com.jal.auto;

import org.openqa.selenium.WebDriver;
import com.jal.pages.GeneralObjects;
import com.jal.pages.MyProfileObjects;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BCLogin extends TestBase {
	
	 
	
	
	
	public void login(ExtentTest logger){
		//sample
	
		
		SubmitButton(GeneralObjects.LogginButton, logger);
		ClickSendText(GeneralObjects.portalID,getProperty("Username"), logger);
		
		ClickSendText(GeneralObjects.portalPassword,getProperty("Password"), logger);
		SubmitButton(GeneralObjects.buttonSignin ,logger );
		
		/*if(verifyErroDisplayed(GeneralObjects.loginerror)) {
		
		elementAvailbility(GeneralObjects.loginerror ,GeneralObjects.signinErrorPage , logger );
		ClickSendText(GeneralObjects.portalID,getProperty("Username"), logger);
		Thread.sleep(2000);
		ClickSendText(GeneralObjects.portalPassword,getProperty("Password"), logger);
		SubmitButton(GeneralObjects.buttonSignin, logger);
		}*/
	
		
		/*try {
			if(verifyErroDisplayed(GeneralObjects.oAuthError)) {
			logger.log(LogStatus.INFO, "Auth Error is displaying");
				pageRefresh(getProperty("URL"));
				ClickSendText(GeneralObjects.portalID,getProperty("Username"), logger);
				Thread.sleep(2000);
				ClickSendText(GeneralObjects.portalPassword, data.get("ConfirmPassword"), logger);
				SubmitButton(GeneralObjects.buttonSignin ,logger );
			}else {
				System.err.println("No Auth Error");
			}*/
			
		/*}catch(Exception e) {
			logger.log(LogStatus.INFO, "No OAuth error Available");
		}*/
	}
	
public void logout(ExtentTest logger){
	try {
		click(GeneralObjects.Welcome, logger);
		click(MyProfileObjects.logout, logger);
		//dropDownSelection(MyProfileObjects.logout, MyProfileObjects.logout,TestBase.Gettextvalue("logout"), logger);
		logger.log(LogStatus.PASS, "User logged out successfully");
	} catch (Exception e) {
		logger.log(LogStatus.FAIL, e.getMessage(), "User is unable logout from application");

	}
	
}

}


